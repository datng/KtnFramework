# Supported Operating systems
## Linux
The scripts have been tested on the following distributions:
- OpenSUSE Leap/Tumbleweed
- Ubuntu 18.04

Pull requests for other distributions are welcome.
## Windows
The setup script is written in Powershell, so the prefered version of Windows would be Windows 10.

# Dependencies
These dependencies will be installed using the setup scripts
- Assimp for importing 3D assets (BSD-3-Clause). (TODO: use gltf format)
- GLFW for window creation and handling keyboard/mouse input (Zlib).
- glm for GL math (MIT).
- libepoxy for loading OpenGL functions (MIT).
- libogg and libvorbis for loading ogg audio files (BSD-3-Clause).
- PortAudio for audio I/O operations (MIT).
- stb for loading images (Public Domain/MIT).
- Vulkan SDK (Apache License, Version 2.0).

# Development
## Development environment setup
### Linux
```
./setup.sh
```
### Windows 10
Before running the script, you need to install these tools in their default folders, otherwise adjust the executables' paths in the script accordingly.
- [7-Zip](https://www.7-zip.org/download.html)
- [Build Tools for Visual Studio 2017](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2017)
- [CMake](https://cmake.org/download/)
- [git](https://git-scm.com/downloads)

Then, in Powershell:
```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process
```
```
./setup.ps1
```
Note that the script uses the Build Tools for Visual Studio 2017. If a later version is installed, make sure to change the msbuild alias in this line to its appropriate path:
```
Set-Alias msbuild 'C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\MSBuild.exe'
```
## Compilation
### Linux
```
./ci-scripts/build.sh
```

### Windows 10
In Powershell:
```
mkdir build
cd build
cmake -A x64 -T host=x64 ..
msbuild /m KtnFramework.sln
```

### With Qt Creator (Cross-platform)
- Open CMakeLists.txt with Qt Creator
- Build -> Build All or `Ctrl + Shift + B`

## Test
### Building tests
Googletest is used for testing. It is downloaded to the `external` directory with the setup script.

Enable building tests by passing -DBUILD_UNIT_TEST to CMake.

### Running tests
In Qt Creator: Tools -> Tests -> Run All Tests.
Alternatively, on Linux:
```
./ci-scripts/run-test.sh
```
To see what needs more testing, generate test coverage with the [script](ci-scripts/report-test-coverage.sh) (only on Linux):
```
./ci-scripts/report-test-coverage.sh
```