# remove the external directory, if needed
param(
    [switch]$force = $false,

    # header-only libraries
    [bool]$glm = $true,
    [bool]$stb = $true,

    # dynamically linked libraries
    [bool]$assimp = $true,
    [bool]$glfw = $true,
    [bool]$libepoxy = $true,
    [bool]$libogg = $true,
    [bool]$libvorbis = $true,
    [bool]$portaudio = $true,
    [bool]$vulkan = $true,

    # statically linked libraries
    [bool]$gtest = $true
)

if ($glm -or $stb) {
    $header_only_libs = $true
}

if ($assimp -or $glfw -or $libepoxy -or $libogg -or $libvorbis -or $portaudio -or $vulkan) {
    $dynamic_libs = $true
}

if ($gtest) {
    $static_libs = $true
}

$nproc = Get-WmiObject -Class Win32_Processor | Select-Object -Expand NumberOfLogicalProcessors | Out-String
$nproc = $nproc -as [int]

# check build tools
MSBuild -version; if (!$?) {
    Write-Output "The directory for MSBuild.exe from the Visual Studio Build Tools is not in PATH."; exit 1
}

7z *>$null; if (!$?) {
    Write-Output "The directory for 7z.exe is not in PATH."; exit 1
}

if ($force) {
    Write-Output "Removing existing external libraries folder..."
    Remove-Item external -Recurse -Force
}

# create external directory if it's not already there
if ([IO.Directory]::Exists((Join-Path(Get-Location) 'external'))) {
    Write-Output "Working with existing external libraries folder. To force setup with new folder, use the -force switch.";
}
else {
    mkdir external
}

Set-Location external
Write-Output "Downloading external libraries..."



if ($header_only_libs) {
    if ($glm) {
        if (Test-Path glm) {
            Set-Location glm
            Write-Output "Updating glm..."
            git pull
            if (!$?) {
                Write-Output "Could not update glm."; exit 1
            }
            Set-Location ..
        }
        else {
            Write-Output "Downloading glm..."
            git clone https://github.com/g-truc/glm.git
            if (!$?) {
                Write-Output "Could not download glm."; exit 1
            }
        }
    }
    if ($stb) {
        if (Test-Path stb) {
            Set-Location stb
            Write-Output "Updating stb..."
            git pull; if (!$?) {
                Write-Output "Could not update stb."; exit 1
            }
            Set-Location ..
        }
        else {
            Write-Output "Downloading stb..."
            git clone https://github.com/nothings/stb.git; if (!$?) {
                Write-Output "Could not download stb."; exit 1
            }
        }
    }
}



if ($dynamic_libs) {
    if ($assimp) {
        if (Test-Path assimp) {
            Set-Location assimp
            Write-Output "Updating assimp..."
            git pull; if (!$?) {
                Write-Output "Could not update assimp."; exit 1
            }
            Set-Location ..
        }
        else {
            Write-Output "Downloading assimp..."
            git clone https://github.com/assimp/assimp.git; if (!$?) {
                Write-Output "Could not download assimp."; exit 1
            }
        }
        Set-Location assimp
        if (!(Test-Path build)) {
            mkdir build
        }
        Set-Location build
        cmake -DASSIMP_BUILD_ASSIMP_TOOLS=OFF -DASSIMP_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=Release -A x64 -T host=x64 ..
        msbuild Assimp.sln /m /p:Configuration=Release
        Set-Location ../..
    }
    if ($glfw) {
        if (Test-Path glfw) {
            Set-Location glfw
            Write-Output "Updating glfw..."
            git pull; if (!$?) {
                Write-Output "Could not update glfw."; exit 1
            }
            Set-Location ..
        }
        else {
            Write-Output "Downloading glfw..."
            git clone https://github.com/glfw/glfw.git; if (!$?) {
                Write-Output "Could not download glfw."; exit 1
            }
        }
        Set-Location glfw
        if (!(Test-Path build)) {
            mkdir build
        }
        Set-Location build
        cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release -DGLFW_BUILD_DOCS=OFF -DGLFW_BUILD_EXAMPLES=OFF -DGLFW_BUILD_TESTS=OFF -A x64 -T host=x64 ..
        msbuild GLFW.sln /m /p:Configuration=Release
        Set-Location ../..
    }
    if ($libepoxy) {
        $libepoxy_version = "1.5.3"
        $libepoxy_extracted_name = "libepoxy-shared-x64"
        $libepoxy_archive = "$libepoxy_extracted_name.zip"
        $libepoxy_download_target = "$PWD/$libepoxy_archive"
        $libepoxy_url = "https://github.com/anholt/libepoxy/releases/download/$libepoxy_version/$libepoxy_archive"
        if (Test-Path libepoxy) {
            Remove-Item libepoxy -Recurse -Force
        }
        if (Test-Path $libepoxy_extracted_name) {
            Remove-Item $libepoxy_extracted_name -Recurse -Force
        }
        if (Test-Path $libepoxy_archive) {
            Remove-Item $libepoxy_archive
        }
        Write-Output "Downloading libepoxy..."
        (New-Object System.Net.WebClient).DownloadFile($libepoxy_url, $libepoxy_download_target); if (!$?) {
            Write-Output "Could not download libepoxy."; exit 1
        }
        7z x $libepoxy_archive; if (!$?) {
            Write-Output "Could not extract $libepoxy_archive."; exit 1
        }
        Remove-Item $libepoxy_archive
        Rename-Item $libepoxy_extracted_name libepoxy
    }
    if ($libogg) {
        if (Test-Path ogg) {
            Set-Location ogg
            Write-Output "Updating libogg..."
            git pull; if (!$?) {
                Write-Output "Could not update libogg."; exit 1
            }
            Set-Location ..
        }
        else {
            Write-Output "Downloading libogg..."
            git clone https://github.com/xiph/ogg.git; if (!$?) {
                Write-Output "Could not download libogg."; exit 1
            }
        }
        Set-Location ogg
        if (!(Test-Path build)) {
            mkdir build
        }
        Set-Location build
        cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release -A x64 -T host=x64 ..
        msbuild libogg.sln /m /p:Configuration=Release
        Set-Location ../..
    }
    if ($libvorbis) {
        if (Test-Path vorbis) {
            Set-Location vorbis
            Write-Output "Updating libvorbis..."
            git pull; if (!$?) {
                Write-Output "Could not update libvorbis."; exit 1
            }
            Set-Location ..
        }
        else {
            Write-Output "Downloading libvorbis..."
            git clone https://github.com/xiph/vorbis.git; if (!$?) {
                Write-Output "Could not download libvorbis."; exit 1
            }
        }
        Set-Location vorbis
        if (!(Test-Path build)) {
            mkdir build
        }
        Set-Location build
        cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release -DOGG_INCLUDE_DIRS="$PWD/../../ogg/include" -DOGG_LIBRARIES="$PWD/../../ogg/build/Release/ogg.lib" -A x64 -T host=x64 ..
        msbuild vorbis.sln /m /p:Configuration=Release
        Set-Location ../..
    }
    if ($portaudio) {
        $portaudio_version = "v190600_20161030"
        $portaudio_name = "pa_stable_" + $portaudio_version
        $portaudio_archive = $portaudio_name + ".tgz"
        $portaudio_url = "http://www.portaudio.com/archives/$portaudio_archive"
        if (Test-Path portaudio) {
            Remove-Item portaudio -Recurse -Force
        }
        if (Test-Path $portaudio_archive) {
            Remove-Item $portaudio_archive
        }
        if (Test-Path "$portaudio_name.tar") {
            Remove-Item "$portaudio_name.tar"
        }
        Write-Output "Downloading portaudio..."
        (New-Object System.Net.WebClient).DownloadFile($portaudio_url, "$PWD\$portaudio_archive")
        7z x $portaudio_archive; if (!$?) {
            Write-Output "Could not extract $portaudio_archive."; exit 1
        }
        7z x "$portaudio_name.tar"; if (!$?) {
            Write-Output "Could not extract $portaudio_name.tar."; exit 1
        }
        
        Remove-Item $portaudio_archive
        Remove-Item "$portaudio_name.tar"

        Set-Location portaudio
        if (!(Test-Path build)) {
            mkdir build
        }
        Set-Location build
        cmake -DCMAKE_BUILD_TYPE=Release -A x64 -T host=x64 ..
        msbuild portaudio.sln /m /p:Configuration=Release
        Set-Location ../..
    }
    if ($vulkan) {
        $vulkan_version = "1.1.114.0"
        $vulkan_url = "https://sdk.lunarg.com/sdk/download/$vulkan_version/windows/VulkanSDK-$vulkan_version-Installer.exe"
        $vulkan_exec = "VulkanSDK-$vulkan_version-Installer.exe"
        $vulkan_download_target = "$PWD/$vulkan_exec"
        if (Test-Path $vulkan_exec) {
            Remove-Item $vulkan_exec
        }
        Write-Output "Downloading VulkanSDK..."
        [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
        (New-Object System.Net.WebClient).DownloadFile($vulkan_url, $vulkan_download_target); if (!$?) {
            Write-Output "Could not download VulkanSDK."; exit 1
        }
        Set-Alias vulkan_install $vulkan_download_target
        vulkan_install /S
    }
}



if ($static_libs) {
    if ($gtest) {
        $googletest_archive = "googletest.zip"
        $googletest_download_target = "$PWD/$googletest_archive"
        $googletest_url = "https://github.com/google/googletest/archive/master.zip"
        
        if (Test-Path googletest) {
            Remove-Item googletest -Recurse -Force
        }
        if (Test-Path $$googletest_archive) {
            Remove-Item $$googletest_archive
        }
        Write-Output "Downloading googletest..."
        (New-Object System.Net.WebClient).DownloadFile($googletest_url, $googletest_download_target); if (!$?) {
            Write-Output "Could not download googletest."; exit 1
        }
        7z x $googletest_download_target; if (!$?) {
            Write-Output "Could not extract $googletest_archive."; exit 1
        }
        Remove-Item $googletest_archive
        Rename-Item googletest-master googletest
    }
}

Set-Location ..
