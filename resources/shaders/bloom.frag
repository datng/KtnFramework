#version 330 core
out vec4 FragColor;
  
in vec2 TexCoords;

uniform sampler2D inputImage;
uniform sampler2D blurredImage;
uniform sampler2D heavilyBlurredImage;
uniform float exposure;
uniform float bigBlurIntensity;
uniform float smallBlurIntensity;

void main()
{             
    const float gamma = 2.2;
    vec3 hdrColor = texture(inputImage, TexCoords).rgb;      
    //vec3 bloomColor = vec3(0.0);
    vec3 blurColor = texture(blurredImage, TexCoords).rgb;
    vec3 heavyBlurColor = texture(heavilyBlurredImage, TexCoords).rgb;
    hdrColor += smallBlurIntensity * blurColor + bigBlurIntensity * heavyBlurColor; // additive blending
    // tone mapping
    //vec3 result = vec3(1.0) - exp(-hdrColor * exposure);
    // also gamma correct while we're at it       
    //result = pow(result, vec3(1.0 / gamma));
    FragColor = vec4(hdrColor, 1.0);
    
} 
