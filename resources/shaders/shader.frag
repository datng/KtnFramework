#version 330 core
layout(location = 0) out vec4 FragColor;
layout(location = 1) out vec4 BrightColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec4 FragPosInLightSpace;

//for the z buffer
float near = 0.1;
float far = 100.0;

struct Material{
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
};

struct Cutoff{
    float inner;
    float outer;
};

struct Falloff{
    float linearterm;
    float quadraticterm;
};

struct Light{
    int type;
    float intensity;
    vec3 position;
    vec3 direction;
    vec3 color;

    //for falloff
    Falloff falloff;

    //for lighting cutoff
    Cutoff cutoff;
};

uniform float pseudorandomnumber;
uniform vec3 viewpos;

uniform Material material;
uniform Light light;

uniform sampler2D shadowmap;

float PCFShadow(vec4 FragPosInLightSpace){
     float shadow = 0.0;
    float bias = 0.001;
    // perform perspective divide
    vec3 projCoords = FragPosInLightSpace.xyz / FragPosInLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    
    //soften shadow using Percentage closer filtering
    float blurScaleFactor = 1.0;
    float currentDepth = projCoords.z;
    for(int i = -1; i <= 1; ++i){
        for(int j = -1; j <= 1; ++j){
            float pcf = texture(shadowmap, projCoords.xy + vec2(i, j) * blurScaleFactor / textureSize(shadowmap,0)).r;
            float shadowdistance = currentDepth - pcf;
            shadow += (currentDepth - bias > pcf  && currentDepth < 1.0) ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;
    //if(shadow < 0.5) shadow = 0;
    //use hard shadow and soften in screenspace
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    //float closestDepth = texture(shadowmap, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    //float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
    //shadow = (currentDepth - bias > closestDepth && currentDepth < 1.0) ? 1.0 : 0.0;
    
    

     return shadow;
}

float ChebyshevUpperBound(vec2 moments, float distanceToLight){
    //inequality valid if distanceToLight > moments.x;
    float p = 0.0;
    if(distanceToLight <= moments.x) p = 1.0;
    //compute variance
    float variance = moments.y - moments.x * moments.x;
    float minVariance = 0.0001;
    variance = max(variance, minVariance);
    //compute probabilistic upper bound
    float d = distanceToLight - moments.x;
    float p_max = variance / (variance + d * d);
    return max(p, p_max);
}

float VarianceShadowMap(vec4 FragPosInLightSpace){
    // perform perspective divide
    vec3 projCoords = FragPosInLightSpace.xyz / FragPosInLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    float currentDepth = projCoords.z;
    
    vec2 moments = vec2(0.0,0.0);
    for(int i = -2; i <= 2; ++i){
        for(int j = -2; j <= 2; ++j){
            float temp = texture(shadowmap, projCoords.xy + vec2(i,j)/textureSize(shadowmap,0)).r;
            moments.x += temp/25;
            moments.y += temp*temp/25;
        }
    }
    //moments.x = texture(shadowmap, projCoords.xy).r;
    float dx = dFdx(moments.x);
    float dy = dFdy(moments.x);
    //moments.y = moments.x * moments.x + 0.25*(dx*dx+dy*dy);
    float shadow = ChebyshevUpperBound(moments, currentDepth);
    return 1.0 - shadow;
}

vec3 screen(vec3 a, vec3 b){
    return vec3(1.0 - (1.0-a)*(1.0-b));
}

void main(){
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);

    //light falloff
    float distance = length(light.position - FragPos);
    float attenuation = 1.0 / (1.0 + light.falloff.linearterm * distance + light.falloff.quadraticterm * distance * distance);

    //ambient
    vec3 ambient = vec3(0.0);//light.color * 0.02 * vec3(texture(material.diffuse, TexCoords));

    //diffuse
    float diff = max(dot(norm,lightDir), 0.0);
    vec3 diffuse = light.color * diff * vec3(texture(material.diffuse, TexCoords));

    //specular shader
    vec3 viewDir = normalize(viewpos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir),0.0), material.shininess);
    vec3 specular = light.color * spec * vec3(texture(material.specular, TexCoords));

    //z-based fog
    //float z = gl_FragCoord.z * 2.0 - 1.0;
    //z = 2.0 * near * far / (far + near - z * (far - near));
    //z = 0.0;
    vec3 result;
    //light cutoff
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.cutoff.inner - light.cutoff.outer;
    float intensity = clamp((theta - light.cutoff.outer) / epsilon, 0.0, 1.0);
    diffuse *= intensity;
    specular *= intensity;
    
    //pcf shadow
    float shadow = PCFShadow(FragPosInLightSpace);
    
    //variance shadow map
    //float shadow = VarianceShadowMap(FragPosInLightSpace);
    
    //render normal
    //result = vec3(cross(Normal, viewDir));
    
    //render with shadow
    result = ambient + light.intensity * clamp((1.0 - shadow) * (diffuse + specular), 0, 1) * attenuation;
    
    //render shadow only
    //result = vec3(1.0 - shadow);
    
    //add z fog
    //result = screen(result, vec3(z/100.0));
    
    //gamma correction
//     vec3 gamma = vec3 (1.0/2.2);
//     result = pow(result, gamma);
    
    //dithering noise
    vec3 noise = vec3(sin(pseudorandomnumber * gl_FragCoord.x * gl_FragCoord.y))/255;
    result = result + noise;

    FragColor = vec4(result,1.0);
    BrightColor = clamp(pow(FragColor, vec4(8.0)), 0.0, 1.0);
}
