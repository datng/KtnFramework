﻿#include "scene_streetlamp.hpp"

#include <iostream>
#include <mutex>
#include <thread>

using glm::vec3;
using std::cout;
using std::endl;
using std::lock_guard;
using std::mutex;
using std::chrono::milliseconds;

Scene_StreetLamp::Scene_StreetLamp() {
    AddCamera(ktn::Graphics::ktnCamera());
    // be careful at this step,
    // the name was changed when adding the camera to the scene camera vector,
    // and has to be called with the trailing _XXXX, where X is a digit
    SetCurrentCamera(Cameras[0].Name.ToStdString());

    CurrentCamera()->SetType(ktn::Graphics::CameraType::FIRSTPERSON);
    CurrentCamera()->SetMode(ktn::Graphics::CameraMode::FREELOOK);
    CurrentCamera()->SetPosition(-3.0f, 0.0f, 0.0f);
    CurrentCamera()->SetTarget(0.0f, 0.0f, 0.0f);
    CurrentCamera()->UpdateRightAndTop();
    CurrentCamera()->UpdateViewMatrix();

    ktn::Graphics::ktnLight light("light");
    light.Intensity = 1.2f;
    light.SetScale(vec3(0.2f));
    light.SetPosition(1.2f, 1.0f, 2.0f, true);
    light.SetDirection(CurrentCamera()->Front());
    light.Cutoff.SetInnerAngle(25.0f);
    light.Cutoff.SetOuterAngle(35.0f);
    light.SetColor(1.0f, 1.0f, 1.0f);
    light.Falloff.LinearTerm = 0.0f;
    light.Falloff.QuadraticTerm = 0.05f;
    light.Name.AppendSuffix();
    AddLight(light);

    Lights[0].SetPosition(CurrentCamera()->Position() + CurrentCamera()->Front() * 0.1f, true);
    Lights[0].SetDirection(CurrentCamera()->Front());
    m_InputMapper.Initialize();
}

void Scene_StreetLamp::ProcessInput() {
    auto currentFrameTime = static_cast<float>(glfwGetTime());
    m_DeltaTime = currentFrameTime - m_LastFrameTime;
    m_LastFrameTime = currentFrameTime;
    float cameraSpeed = 2.0f * m_DeltaTime;

    if (glfwGetKey(m_Window, (m_InputMapper.Map[ktn::KTNKEY_ESCAPE])) == GLFW_PRESS) {
        glfwSetWindowShouldClose(m_Window, 1);
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[ktn::KTNKEY_W]) == GLFW_PRESS) {
        CurrentCamera()->MoveForward(cameraSpeed);
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[ktn::KTNKEY_A]) == GLFW_PRESS) {
        CurrentCamera()->MoveLeft(cameraSpeed);
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[ktn::KTNKEY_S]) == GLFW_PRESS) {
        CurrentCamera()->MoveBackward(cameraSpeed);
    };

    if (glfwGetKey(m_Window, m_InputMapper.Map[ktn::KTNKEY_D]) == GLFW_PRESS) {
        CurrentCamera()->MoveRight(cameraSpeed);
    };

    if (glfwGetKey(m_Window, m_InputMapper.Map[ktn::KTNKEY_UP]) == GLFW_PRESS) {
        CurrentCamera()->TurnUp(1);
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[ktn::KTNKEY_DOWN]) == GLFW_PRESS) {
        CurrentCamera()->TurnDown(1);
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[ktn::KTNKEY_LEFT]) == GLFW_PRESS) {
        CurrentCamera()->TurnLeft(1);
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[ktn::KTNKEY_RIGHT]) == GLFW_PRESS) {
        CurrentCamera()->TurnRight(1);
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
    }

    CurrentCamera()->UpdateViewMatrix();

    if (m_LeftMouseIsClicking) {
        Lights[0].SetPosition(CurrentCamera()->Position() + CurrentCamera()->Front() * 0.1f, true);
        Lights[0].SetDirection(CurrentCamera()->Front());
    }
}

void Scene_StreetLamp::callback_FrameBufferSizeChange(GLFWwindow *window, int width, int height) {
    cout << "Windows size changed to " << width << "x" << height;
    if (window != nullptr) {
        CONSIDER
    }
}

void Scene_StreetLamp::callback_MouseClick(GLFWwindow *window, int button, int action, int mods) {
    // for the time being the modification does nothing, but can be added to do stuff later.
    if (mods != 0) {
    }

    if ((window != nullptr) && GLFW_MOUSE_BUTTON_LEFT == button) {
        if (action == GLFW_PRESS) {
            m_LeftMouseIsClicking = true;
        }
        if (action == GLFW_RELEASE) {
            m_LeftMouseIsClicking = false;
        }
    }
}

void Scene_StreetLamp::callback_MouseMove(GLFWwindow *window, double xpos, double ypos) {
    if (window != nullptr) {
        if (m_FirstMouse) {
            m_LastX = xpos;
            m_LastY = ypos;
            m_FirstMouse = false;
        }
        mutex m;
        lock_guard<mutex> guard(m);
        CurrentCamera()->TurnRight(float(xpos - m_LastX));
        CurrentCamera()->TurnDown(float(ypos - m_LastY));
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
        m_LastX = xpos;
        m_LastY = ypos;
    }
}
