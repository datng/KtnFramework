#ifndef SCENE_STREETLAMP_HPP
#define SCENE_STREETLAMP_HPP
#include <ktnEngine/ktnApplication>

class Scene_StreetLamp : public ktn::ktnScene {
public:
    Scene_StreetLamp();
    void ProcessInput() final;
    void callback_MouseMove(GLFWwindow *window, double xpos, double ypos) final;
    void callback_FrameBufferSizeChange(GLFWwindow *window, int width, int height) final;
    void callback_MouseClick(GLFWwindow *window, int button, int action, int mods) final;
};
#endif // SCENE_STREETLAMP_HPP
