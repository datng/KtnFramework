﻿#include "scene_streetlamp.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include <thread>

using namespace glm;
using namespace ktn::Graphics;

int main() {
    Scene_StreetLamp scene;

    auto *au1 = new ktn::audio::ktnAudio();
    auto *au2 = new ktn::audio::ktnAudio();
    auto *au3 = new ktn::audio::ktnAudio();
    std::thread t1([au1] { au1->Play("0288.ogg"); });
    std::thread t2([au2] { au2->Play("0902.ogg"); });
    std::thread t3([au3] { au3->Play("1142.ogg"); });

    ktn::ktnGame game(Backend::OPENGL, "Test Game");

    mat4 ProjectionMatrix = perspective(radians(45.0f), static_cast<float>(game.Extent2D.width) / game.Extent2D.height, 0.1f, 100.0f);

    // This is particularly leaking memory.
    // If we create normal variables here they will go out of scope after the if
    // We keep it for the time being, as the vulkan backend needs to have the shader and model classes changed as well.
    if (game.GraphicsBackend == Backend::OPENGL) {
        // build and compile shader program
        ktnShaderProgram *cubeshader = new ktnShaderProgram("resources/shaders/shader.vert", "resources/shaders/shader.frag");
        cubeshader->Pass = ShaderPass::COLOR;
        ktnShaderProgram *lightObjectShader = new ktnShaderProgram("resources/shaders/shader.vert", "resources/shaders/lightshader.frag");
        lightObjectShader->Pass = ShaderPass::COLOR;

        ktnShaderProgram *blurshader = new ktnShaderProgram("resources/shaders/default.vert", "resources/shaders/blur.frag");
        blurshader->Pass = ShaderPass::POSTPROCESS;

        ktnShaderProgram *bloomshader = new ktnShaderProgram("resources/shaders/default.vert", "resources/shaders/bloom.frag");

        ktnShaderProgram *shadowshader = new ktnShaderProgram("resources/shaders/shadow.vert", "resources/shaders/shadow.frag");
        shadowshader->Pass = ShaderPass::SHADOW;

        ktnShaderProgram *fullscreenrectangleshader = new ktnShaderProgram("resources/shaders/default.vert", "resources/shaders/screen.frag");

        cubeshader->Use();
        cubeshader->SetUniformMat4("ProjectionMatrix", ProjectionMatrix);

        game.Renderer->SetShadowShader(shadowshader);
        game.Renderer->SetColorShader(cubeshader);
        game.Renderer->SetBlurShader(blurshader);
        game.Renderer->SetFullscreenRectangleShader(fullscreenrectangleshader);
        game.Renderer->SetBloomShader(bloomshader);

        ktnModel invertedbox = ktnModel("resources/models/invertedbox.obj");
        invertedbox.Name = "invertedbox";
        ktnModel monkey = ktnModel("resources/models/monkey.obj");
        monkey.SetPosition(0.0f, -0.10f, 0.55f);
        monkey.SetRotation(1.0f, 0.0f, 0.0f, radians(-30.0f), true);
        monkey.Name = "monkey";
        ktnModel trafficlight = ktnModel("resources/models/trafficlight.obj");
        trafficlight.Name = "trafficlight";
        ktnModel cube = ktnModel("resources/models/cube.obj");
        cube.Name = "cube";

        scene.AddModel(cube);
        scene.AddModel(invertedbox);
        scene.AddModel(monkey);
        scene.AddModel(trafficlight);
        // set up frame buffer
        uint ShadowMapSize = 1024;

        ktnFrameBuffer *shadowbuffer = new ktnFrameBuffer(FrameBufferType::DEPTH, vk::Extent2D(ShadowMapSize, ShadowMapSize));

        // add the buffers to the scene
        scene.ShadowBuffer_Directional.push_back(shadowbuffer);
    }
    game.SetActiveScene(&scene);

    // render loop
    game.Run();

    t1.join();
    t2.join();
    t3.join();
    // some functions to deallocate all memories taken by the assets
    return 0;
}
