/*****************************
Copyright 2019 Tien Dat Nguyen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*****************************/

#ifndef KTNSIGNALSLOT_TEST_HPP
#define KTNSIGNALSLOT_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/ktnCore>

#include <string>

using namespace ktn::SignalSlot;

TEST(ktnSignal, constructors_destructor) {
    const ktnSignalSlot::ktnSignal<int> signal1;
    EXPECT_EQ(StaticManager.Signals.size(), 1);
    const ktnSignalSlot::ktnSignal<> *signal2 = new ktnSignalSlot::ktnSignal<>;
    EXPECT_EQ(StaticManager.Signals.size(), 2);
    delete signal2;
    EXPECT_EQ(StaticManager.Signals.size(), 1);
}

typedef struct {
    unsigned int nRecordedCalls = 0;
    float currentFloat = 0.0f;
    long currentLong = 0;
    std::string currentString = "";
    void Reset() {
        nRecordedCalls = 0;
        currentFloat = 0.0f;
        currentLong = 0;
        currentString = "";
        assert(nRecordedCalls == 0);
        assert(currentFloat == 0.0f);
        assert(currentLong == 0);
        assert(currentString == "");
    }
} placeboValueHolder;

static placeboValueHolder STATIC_PLACEBO;

// free slots: funcions with matching signals of arbitrary return types

bool freeslot1() {
    ++STATIC_PLACEBO.nRecordedCalls;
    return true;
}

void freeslot2(float eFloat) {
    ++STATIC_PLACEBO.nRecordedCalls;
    STATIC_PLACEBO.currentFloat = eFloat;
}

int freeslot3(long eLong, std::string eString) {
    ++STATIC_PLACEBO.nRecordedCalls;
    STATIC_PLACEBO.currentLong = eLong;
    STATIC_PLACEBO.currentString = eString;
    return 0;
}

TEST(ktnSignal, connections_disconnections) {
    EXPECT_EQ(StaticManager.Signals.size(), 0);
    class Emitter : public ktnSignalSlot {
    public:
        ktnSignal<> signal1;
        ktnSignal<float> signal2;
        ktnSignal<long, std::string> signal3;
    } *emitter = nullptr;

    EXPECT_EQ(StaticManager.Receivers.size(), 0);
    EXPECT_EQ(StaticManager.Signals.size(), 0);

    emitter = new Emitter();
    emitter->signal1.Connect(&freeslot1);
    emitter->signal2.Connect(&freeslot2);
    emitter->signal3.Connect(&freeslot3);

    EXPECT_EQ(StaticManager.Receivers.size(), 1); // any ktnSignalSlot is a potential receiver
    EXPECT_EQ(StaticManager.Signals.size(), 3);

    // test connections with free functions
    emitter->signal1.Connect(&freeslot1);
    emitter->signal2.Connect(&freeslot2);
    emitter->signal3.Connect(&freeslot3);

    EXPECT_EQ(STATIC_PLACEBO.currentFloat, 0.0f);
    EXPECT_EQ(STATIC_PLACEBO.currentLong, 0);
    EXPECT_EQ(STATIC_PLACEBO.currentString, "");

    emitter->signal1();
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 1);
    emitter->signal2(0.1f);
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 2);
    EXPECT_EQ(STATIC_PLACEBO.currentFloat, 0.1f);
    emitter->signal3(2, "3.4");
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 3);
    EXPECT_EQ(STATIC_PLACEBO.currentLong, 2);
    EXPECT_EQ(STATIC_PLACEBO.currentString, "3.4");

    // test connections with member functions
    class Receiver : public ktnSignalSlot {
    public:
        // these slots are functions with matching signals of arbitrary return types
        float slot1() { //
            ++STATIC_PLACEBO.nRecordedCalls;
            return 0.0f;
        }

        std::string slot2(float eFloat) {
            ++STATIC_PLACEBO.nRecordedCalls;
            STATIC_PLACEBO.currentFloat = eFloat;
            return "OK";
        }

        size_t slot3(long eLong, std::string eString) {
            ++STATIC_PLACEBO.nRecordedCalls;
            STATIC_PLACEBO.currentLong = eLong;
            STATIC_PLACEBO.currentString = eString;
            return 11111111111111;
        }
    };
    Receiver *receiver = nullptr;

    STATIC_PLACEBO.Reset();
    emitter->signal1.Connect(receiver, &Receiver::slot1); // receiver is nullptr, no effect.
    emitter->signal2.Connect(receiver, &Receiver::slot2); // receiver is nullptr, no effect.
    emitter->signal3.Connect(receiver, &Receiver::slot3); // receiver is nullptr, no effect.
    emitter->signal1();
    emitter->signal2(0.1f);
    emitter->signal3(2, "3.4");

    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 3); // only the free functions were called

    STATIC_PLACEBO.Reset();
    receiver = new Receiver();
    emitter->signal1.Connect(receiver, &Receiver::slot1);
    emitter->signal2.Connect(receiver, &Receiver::slot2);
    emitter->signal3.Connect(receiver, &Receiver::slot3);
    emitter->signal1();
    emitter->signal2(0.1f);
    emitter->signal3(2, "3.4");

    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 6); // both free and member functions are called.

    // test duplicate connections
    STATIC_PLACEBO.Reset();
    for (int i = 0; i < 100; ++i) {
        emitter->signal1.Connect(&freeslot1);
        emitter->signal2.Connect(&freeslot2);
        emitter->signal3.Connect(&freeslot3);
        emitter->signal1.Connect(receiver, &Receiver::slot1);
        emitter->signal2.Connect(receiver, &Receiver::slot2);
        emitter->signal3.Connect(receiver, &Receiver::slot3);
    }

    emitter->signal1();
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 2); // both free and member functions are called once each.
    emitter->signal2(5.6f);
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 4); // both free and member functions are called once each.
    EXPECT_EQ(STATIC_PLACEBO.currentFloat, 5.6f);
    emitter->signal3(7, "8.9");
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 6); // both free and member functions are called once each.
    EXPECT_EQ(STATIC_PLACEBO.currentLong, 7);
    EXPECT_EQ(STATIC_PLACEBO.currentString, "8.9");

    // test disconnections
    STATIC_PLACEBO.Reset();
    emitter->signal1.Disconnect(&freeslot1);
    emitter->signal2.Disconnect(&freeslot2);
    emitter->signal3.Disconnect(&freeslot3);
    emitter->signal1();
    emitter->signal2(0.1f);
    emitter->signal3(2, "3.4");
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 3);

    STATIC_PLACEBO.Reset();
    emitter->signal1.Connect(&freeslot1);
    emitter->signal2.Connect(&freeslot2);
    emitter->signal3.Connect(&freeslot3);
    emitter->signal1.Disconnect(receiver, &Receiver::slot1);
    emitter->signal2.Disconnect(receiver, &Receiver::slot2);
    emitter->signal3.Disconnect(receiver, &Receiver::slot3);
    emitter->signal1();
    emitter->signal2(0.1f);
    emitter->signal3(2, "3.4");
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 3);
    emitter->signal1.Disconnect(&freeslot1);
    emitter->signal2.Disconnect(&freeslot2);
    emitter->signal3.Disconnect(&freeslot3);
    // at this point all slots are disconnected
    STATIC_PLACEBO.Reset();
    emitter->signal1();
    emitter->signal2(0.1f);
    emitter->signal3(2, "3.4");
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 0);

    // test duplicate disconnections
    for (int i = 0; i < 100; ++i) {
        emitter->signal1.Disconnect(&freeslot1);
        emitter->signal2.Disconnect(&freeslot2);
        emitter->signal3.Disconnect(&freeslot3);
        emitter->signal1.Disconnect(receiver, &Receiver::slot1);
        emitter->signal2.Disconnect(receiver, &Receiver::slot2);
        emitter->signal3.Disconnect(receiver, &Receiver::slot3);
    }

    // test connections and disconnections on deleted receivers
    STATIC_PLACEBO.Reset();
    delete receiver;
    emitter->signal1.Connect(receiver, &Receiver::slot1);
    emitter->signal2.Connect(receiver, &Receiver::slot2);
    emitter->signal3.Connect(receiver, &Receiver::slot3);
    emitter->signal1();
    emitter->signal2(0.1f);
    emitter->signal3(2, "3.4");
    EXPECT_EQ(STATIC_PLACEBO.nRecordedCalls, 0);
    emitter->signal1.Disconnect(receiver, &Receiver::slot1);
    emitter->signal2.Disconnect(receiver, &Receiver::slot2);
    emitter->signal3.Disconnect(receiver, &Receiver::slot3);

    EXPECT_EQ(StaticManager.Signals.size(), 3);
    delete emitter;
    EXPECT_EQ(StaticManager.Signals.size(), 0);
    EXPECT_EQ(StaticManager.Receivers.size(), 0);
}

#endif // KTNSIGNALSLOT_TEST_HPP
