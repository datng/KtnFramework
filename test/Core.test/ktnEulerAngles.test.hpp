#ifndef KTNEULERANGLES_TEST_HPP
#define KTNEULERANGLES_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Math/ktnEulerAngles.hpp>

using namespace ktn::Math;

TEST(ktnEulerAngles, constructors_destructor) {
    const ktnEulerAngles anglesDefault;
    EXPECT_EQ(anglesDefault.Pitch, 0);
    EXPECT_EQ(anglesDefault.Roll, 0);
    EXPECT_EQ(anglesDefault.Yaw, 0);
    const ktnEulerAngles anglesInDegrees(ktn::AngleUnit::degree, 1.2f, 3.4f, 5.6f);
    EXPECT_EQ(anglesInDegrees.Pitch, 1.2f);
    EXPECT_EQ(anglesInDegrees.Roll, 3.4f);
    EXPECT_EQ(anglesInDegrees.Yaw, 5.6f);
    const ktnEulerAngles anglesInRadians(ktn::AngleUnit::radian, 1.2f, 3.4f, 5.6f);
    EXPECT_EQ(DegreeToRadian(anglesInRadians.Pitch), 1.2f);
    EXPECT_EQ(DegreeToRadian(anglesInRadians.Roll), 3.4f);
    EXPECT_EQ(DegreeToRadian(anglesInRadians.Yaw), 5.6f);
}

TEST(ktnEulerAngles, operators) {
    const ktnEulerAngles anglesDefault;
    ktnEulerAngles angles(ktn::AngleUnit::degree, 1.2f, 3.4f, 5.6f);
    EXPECT_EQ(anglesDefault == angles, false);
    EXPECT_EQ(anglesDefault != angles, true);
    angles = ktnEulerAngles(ktn::AngleUnit::degree, 0, 0, 0);
    EXPECT_EQ(anglesDefault == angles, true);
    EXPECT_EQ(anglesDefault != angles, false);
}

#endif // KTNEULERANGLES_TEST_HPP
