﻿#ifndef KTNOBJECT_TEST_HPP
#define KTNOBJECT_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/ktnCore>

using namespace ktn::Core;

TEST(ktnObject, constructors_destructor) {
    ktnObject obj_default;
    EXPECT_EQ(obj_default.Name.ToStdString() == "ktnObject", true);
    ktnObject obj_noDefault("non-generic name");
    EXPECT_EQ(obj_noDefault.Name.ToStdString() == "nongenericname", true);
}

TEST(ktnObject, operators) {
    ktnObject obj_default;
    EXPECT_EQ(obj_default == ktnObject(), true);
    ktnObject obj_noDefault("non_generic name");
    EXPECT_EQ(obj_noDefault != ktnObject(), true);
    obj_noDefault = obj_default;
    EXPECT_EQ(obj_noDefault == ktnObject(), true);
}
#endif // KTNOBJECT_TEST_HPP
