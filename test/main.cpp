﻿#include "Application.test.hpp"
#include "Core.test.hpp"
#include "Graphics.test.hpp"
#include "Math.test.hpp"

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
