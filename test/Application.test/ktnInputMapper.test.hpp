#ifndef KTNINPUTMAPPER_TEST_HPP
#define KTNINPUTMAPPER_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/ktnApplication>

using namespace ktn;

TEST(ktnInputMapper, default_initializer_should_map_the_right_keys) {
    // the base MapKeyboardInput should work here too, so there will be no separate test.
    ktnInputMapper mapperDefault;
    mapperDefault.Initialize();
    EXPECT_EQ(mapperDefault.Map[KTNKEY_A], KTNKEY_A);
    EXPECT_EQ(mapperDefault.Map[KTNKEY_Z], KTNKEY_Z);

    EXPECT_EQ(mapperDefault.Map[KTNKEY_0], KTNKEY_0);
    EXPECT_EQ(mapperDefault.Map[KTNKEY_9], KTNKEY_9);

    EXPECT_EQ(mapperDefault.Map[KTNKEY_ESCAPE], KTNKEY_ESCAPE);
}
#endif // KTNINPUTMAPPER_TEST_HPP
