#ifndef KTNQUATERNION_TEST_HPP
#define KTNQUATERNION_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/Math/ktnQuaternion.hpp>

using namespace ktn::Math;

TEST(ktnQuaternion_Component, constructors_destructor) {
    auto real = ktnQuaternion::ComponentReal(3.4f);
    EXPECT_EQ(real.Basis, ktnQuaternion::ComponentBasis::Real);
    EXPECT_THAT(real.Scale, ::testing::FloatEq(3.4f));
    ktnQuaternion::ComponentReal realCopy(real);
    EXPECT_EQ(realCopy.Basis, ktnQuaternion::ComponentBasis::Real);
    EXPECT_THAT(realCopy.Scale, ::testing::FloatEq(3.4f));

    auto i = ktnQuaternion::ComponentI(5.6f);
    EXPECT_EQ(i.Basis, ktnQuaternion::ComponentBasis::I);
    EXPECT_THAT(i.Scale, ::testing::FloatEq(5.6f));
    ktnQuaternion::ComponentI iCopy(i);
    EXPECT_EQ(iCopy.Basis, ktnQuaternion::ComponentBasis::I);
    EXPECT_THAT(iCopy.Scale, ::testing::FloatEq(5.6f));

    auto j = ktnQuaternion::ComponentJ(7.8f);
    EXPECT_EQ(j.Basis, ktnQuaternion::ComponentBasis::J);
    EXPECT_THAT(j.Scale, ::testing::FloatEq(7.8f));
    ktnQuaternion::ComponentJ jCopy(j);
    EXPECT_EQ(jCopy.Basis, ktnQuaternion::ComponentBasis::J);
    EXPECT_THAT(jCopy.Scale, ::testing::FloatEq(7.8f));

    auto k = ktnQuaternion::ComponentK(9.0f);
    EXPECT_EQ(k.Basis, ktnQuaternion::ComponentBasis::K);
    EXPECT_THAT(k.Scale, ::testing::FloatEq(9.0f));
    ktnQuaternion::ComponentK kCopy(k);
    EXPECT_EQ(kCopy.Basis, ktnQuaternion::ComponentBasis::K);
    EXPECT_THAT(kCopy.Scale, ::testing::FloatEq(9.0f));
}

TEST(ktnQuaternion_Component, operators) {
    ktnQuaternion::ComponentReal real(1.2f);
    ktnQuaternion::ComponentI i(3.4f);
    ktnQuaternion::ComponentJ j(5.6f);
    ktnQuaternion::ComponentK k(7.8f);
    real = real * 4.0;
    i = i * 3.0;
    j = j * 2.0;
    k = k * 0.5;
    EXPECT_THAT(real.Scale, ::testing::FloatEq(4.8f));
    EXPECT_THAT(i.Scale, ::testing::FloatEq(10.2f));
    EXPECT_THAT(j.Scale, ::testing::FloatEq(11.2f));
    EXPECT_THAT(k.Scale, ::testing::FloatEq(3.9f));

    real = real + 4.0;
    i = i + ktnQuaternion::BasisI * 3.0;
    j = j + ktnQuaternion::BasisJ * 2.0;
    k = k + ktnQuaternion::BasisK * 0.5;
    EXPECT_THAT(real.Scale, ::testing::FloatEq(8.8f));
    EXPECT_THAT(i.Scale, ::testing::FloatEq(13.2f));
    EXPECT_THAT(j.Scale, ::testing::FloatEq(13.2f));
    EXPECT_THAT(k.Scale, ::testing::FloatEq(4.4f));
}

TEST(ktnQuaternion, constructors_destructor) {
    ktnQuaternion qDefault;
    EXPECT_THAT(qDefault.W.Scale, ::testing::FloatEq(0.0f));
    EXPECT_THAT(qDefault.X.Scale, ::testing::FloatEq(0.0f));
    EXPECT_THAT(qDefault.Y.Scale, ::testing::FloatEq(0.0f));
    EXPECT_THAT(qDefault.Z.Scale, ::testing::FloatEq(0.0f));

    qDefault.Set(1.0, 2.0, 3.0, 4.0);
    ktnQuaternion qCopy(qDefault);
    EXPECT_THAT(qCopy.W.Scale, ::testing::FloatEq(1.0f));
    EXPECT_THAT(qCopy.X.Scale, ::testing::FloatEq(2.0f));
    EXPECT_THAT(qCopy.Y.Scale, ::testing::FloatEq(3.0f));
    EXPECT_THAT(qCopy.Z.Scale, ::testing::FloatEq(4.0f));

    ktnQuaternion qComponent(
        ktnQuaternion::ComponentReal(1.1f), ktnQuaternion::ComponentI(2.2f), ktnQuaternion::ComponentJ(3.3f), ktnQuaternion::ComponentK(4.4f));
    EXPECT_THAT(qComponent.W.Scale, ::testing::FloatEq(1.1f));
    EXPECT_THAT(qComponent.X.Scale, ::testing::FloatEq(2.2f));
    EXPECT_THAT(qComponent.Y.Scale, ::testing::FloatEq(3.3f));
    EXPECT_THAT(qComponent.Z.Scale, ::testing::FloatEq(4.4f));

    ktnQuaternion qFloat(5.5f, 6.6f, 7.7f, 8.8f);
    EXPECT_THAT(qFloat.W.Scale, ::testing::FloatEq(5.5f));
    EXPECT_THAT(qFloat.X.Scale, ::testing::FloatEq(6.6f));
    EXPECT_THAT(qFloat.Y.Scale, ::testing::FloatEq(7.7f));
    EXPECT_THAT(qFloat.Z.Scale, ::testing::FloatEq(8.8f));
}

TEST(ktnQuaternion, operators) {
    ktnQuaternion q;
    q = ktnQuaternion(1.0f, 2.0f, 3.0f, 4.0f);
    EXPECT_THAT(q.W.Scale, ::testing::FloatEq(1.0f));
    EXPECT_THAT(q.X.Scale, ::testing::FloatEq(2.0f));
    EXPECT_THAT(q.Y.Scale, ::testing::FloatEq(3.0f));
    EXPECT_THAT(q.Z.Scale, ::testing::FloatEq(4.0f));

    q = ktnQuaternion();
    q = q + ktnQuaternion(1.0f, 0.0f, 0.0f, 0.0f);

    q = q * ktnQuaternion::BasisI;
    EXPECT_EQ(q.Equals(ktnQuaternion(0.0f, 1.0f, 0.0f, 0.0f)), true);

    q = q * ktnQuaternion::BasisJ;
    EXPECT_EQ(q.Equals(ktnQuaternion(0.0f, 0.0f, 0.0f, 1.0f)), true);

    q = q * ktnQuaternion::BasisK;
    EXPECT_EQ(q.Equals(ktnQuaternion(-1.0f, 0.0f, 0.0f, 0.0f)), true);

    q = q * 2 * ktnQuaternion::BasisReal;
    EXPECT_EQ(q.Equals(ktnQuaternion(-2.0f, 0.0f, 0.0f, 0.0f)), true);
}

TEST(ktnQuaternion, others) {
    ktnQuaternion q(4.0f, 3.0f, 2.0f, 1.0f);
    EXPECT_EQ(q.Equals(ktnQuaternion(4.0f, 3.0f, 2.0f, 1.0f)), true);
    auto qConj = q.Conjugated();
    EXPECT_EQ(qConj.Equals(ktnQuaternion(4.0f, -3.0f, -2.0f, -1.0f)), true);
    q = ktnQuaternion(0.0f, 3.0f, 4.0f, 0.0);
    EXPECT_THAT(q.Norm(), ::testing::FloatEq(5.0f));
}

TEST(ktnQuaternion, utilities) {
    ktnQuaternion q(1.0f, 0.0f, 0.0f, 0.0f);
    q = 2 * q;
    EXPECT_EQ(q.Equals(ktnQuaternion(2.0f, 0.0f, 0.0f, 0.0f)), true);
    q = ktnQuaternion::BasisReal * q;
    EXPECT_EQ(q.Equals(ktnQuaternion(2.0f, 0.0f, 0.0f, 0.0f)), true);
    q = ktnQuaternion::BasisI * q;
    EXPECT_EQ(q.Equals(ktnQuaternion(0.0f, 2.0f, 0.0f, 0.0f)), true);
    q = ktnQuaternion::BasisJ * q;
    EXPECT_EQ(q.Equals(ktnQuaternion(0.0f, 0.0f, 0.0f, -2.0f)), true);
    q = ktnQuaternion::BasisK * q;
    EXPECT_EQ(q.Equals(ktnQuaternion(2.0f, 0.0f, 0.0f, 0.0f)), true);

    q = ktnQuaternion(0.0f, 3.0, 4.0f, 0.0f);
    auto n = Normalize(q);
    EXPECT_EQ(n.Status, ktn::ExecutionStatus::OK);
    EXPECT_THAT(n.Quaternion.Norm(), ::testing::FloatEq(1.0f));
    WIP; // the direction should also stay the same, but there is no function to calculate the angle between the vector and its normalized form

    q = q * 0;
    n = Normalize(q);
    EXPECT_EQ(n.Status, ktn::ExecutionStatus::FAILURE);
    EXPECT_THAT(n.Quaternion.Norm(), ::testing::FloatEq(0.0f));
}
#endif
