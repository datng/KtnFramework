#ifndef KTN3DELEMENT_TEST_HPP
#define KTN3DELEMENT_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ktnEngine/ktnGraphics>

#include <glm/gtc/matrix_transform.hpp>

using namespace ktn::Graphics;

TEST(ktn3DElement, constructors_destructor) {
    ktn3DElement element_default;
    EXPECT_EQ(element_default.Name.ToStdString(), "ktn3DElement");
    EXPECT_EQ(element_default.ModelMatrix(), IDENTITY_MATRIX_4);
    EXPECT_EQ(element_default.Position(), glm::vec3(0, 0, 0));
    EXPECT_EQ(element_default.Rotation(), glm::vec4(0, 0, 1, 0));
    EXPECT_EQ(element_default.Scale(), glm::vec3(1, 1, 1));

    ktn3DElement element_noDefault("non-generic name");
    EXPECT_EQ(element_noDefault.Name.ToStdString(), "nongenericname");
}

TEST(ktn3DElement, operators) {
    // different ktnObjects
    const ktn3DElement element_default("name1");
    ktn3DElement element2("name2");
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
    element2 = element_default;
    EXPECT_EQ(element_default == element2, true);
    EXPECT_EQ(element_default != element2, false);

    // different positions
    element2.SetPosition(0, 1, 2);
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
    element2.SetPosition(0, 0, 0);
    EXPECT_EQ(element_default == element2, true);
    EXPECT_EQ(element_default != element2, false);

    // different rotations
    element2.SetRotation(3, 4, 5, 6);
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
    element2.SetRotation(0, 0, 1, 0);
    EXPECT_EQ(element_default == element2, true);
    EXPECT_EQ(element_default != element2, false);

    // different scales
    element2.SetScale(7, 8, 9);
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
    element2.SetScale(1, 1, 1);
    EXPECT_EQ(element_default == element2, true);
    EXPECT_EQ(element_default != element2, false);

    // different model matrices
    element2.SetScale(7, 8, 9, true);
    EXPECT_EQ(element_default == element2, false);
    EXPECT_EQ(element_default != element2, true);
    element2.SetScale(1, 1, 1, true);
    EXPECT_EQ(element_default == element2, true);
    EXPECT_EQ(element_default != element2, false);
}

TEST(ktn3DElement, getters_setters) {
    ktn3DElement element;
    element.SetPosition(0, 1, 2);
    EXPECT_EQ(element.Position(), glm::vec3(0, 1, 2));
    element.SetRotation(3, 4, 5, 6);
    EXPECT_EQ(element.Rotation(), glm::vec4(3, 4, 5, 6));
    element.SetScale(7, 8, 9);
    EXPECT_EQ(element.Scale(), glm::vec3(7, 8, 9));
    EXPECT_EQ(element.ModelMatrix(), IDENTITY_MATRIX_4);

    glm::mat4 matrix;
    matrix = glm::translate(IDENTITY_MATRIX_4, element.Position());
    matrix = glm::rotate(matrix, element.Rotation().w, glm::vec3(element.Rotation().x, element.Rotation().y, element.Rotation().z));
    matrix = glm::scale(matrix, element.Scale());
    element.SetScale(7, 8, 9, true);
    EXPECT_EQ(element.ModelMatrix(), matrix);

    element.SetPosition(glm::vec3(9, 8, 7));
    EXPECT_EQ(element.Position(), glm::vec3(9, 8, 7));
    element.SetRotation(glm::vec3(6, 5, 4), 3);
    EXPECT_EQ(element.Rotation(), glm::vec4(6, 5, 4, 3));
    element.SetRotation(glm::vec4(3, 4, 5, 6));
    EXPECT_EQ(element.Rotation(), glm::vec4(3, 4, 5, 6));
    element.SetScale(glm::vec3(2, 1, 0));
    EXPECT_EQ(element.Scale(), glm::vec3(2, 1, 0));
}

#endif // KTN3DELEMENT_TEST_HPP
