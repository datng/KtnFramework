./ci-scripts/run-test.sh || {
    exit 1
}

cd build/test
gcovr -r ../.. --exclude ../../example/ --exclude ../../external/ --exclude ../../test/ || {
    exit 1
}
cd ..
