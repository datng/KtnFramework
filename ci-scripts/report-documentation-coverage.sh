DOCS_DIR=documentation

if [ ! -d $DOCS_DIR ]; then
    echo "Could not find directory \"$DOCS_DIR\". Generating..."
    ./ci-scripts/generate-documentation.sh || {
        exit 1;
    }
fi

cd $DOCS_DIR

python3 -m coverxygen --xml-dir xml --src-dir .. --output coverxygenated.info --kind all || {
    echo "Could not generate documentation coverage info file."
    exit 1;
}

# TODO add check if genhtml exists
genhtml coverxygenated.info -o ./coverage
genhtml coverxygenated.info -o ./coverage > /dev/null 2>&1 || {
    echo "Could not generate html files from documentation coverage info file."
    exit 1;
}

# TODO add check if lcov exists
lcov --summary coverxygenated.info || {
    exit 1;
}

cd ..
