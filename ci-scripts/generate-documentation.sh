# TODO: parse input argument and set the output folder,
# as well as options to generate protected/private members.
# This can be done with sed to replace the corresponding entries in the Doxyfile.

command -v doxygen > /dev/null 2>&1 || {
    echo >&2 "Doxygen is required to generate documentation, but not installed."
    exit 2;
}

DOXYFILE=Doxyfile

doxygen $DOXYFILE > /dev/null 2>&1 || {
    echo >&2 "$DOXYFILE could not be found."
    exit 2;
}

echo "Documentation generated. Check documentation/html/index.html"
