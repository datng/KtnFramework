# parse arguments
for i in "$@"; do
    case $i in
    --build-examples)
    KTN_EXAMPLE_TOGGLE=ON
    ;;
    --build-unit-tests)
    KTN_UNIT_TEST_TOGGLE=ON
    ;;
    esac
done

if [ -z "$KTN_EXAMPLE_TOGGLE" ]; then
    KTN_EXAMPLE_TOGGLE=OFF
fi

if [ -z "$KTN_UNIT_TEST_TOGGLE" ]; then
    KTN_UNIT_TEST_TOGGLE=OFF
fi

if [ ! -d build ]; then
    mkdir build
fi

cd build

KTN_CMAKE_OPTIONS="
-DBUILD_EXAMPLE=$KTN_EXAMPLE_TOGGLE \
-DBUILD_UNIT_TEST=$KTN_UNIT_TEST_TOGGLE \
-DUSE_OPENGL=ON \
-DUSE_VULKAN=ON \
"

cmake $KTN_CMAKE_OPTIONS .. || {
    exit 1
}

make -j$(nproc) || {
    exit 1
}
