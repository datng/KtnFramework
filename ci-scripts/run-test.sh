if [ ! -f build/test/UnitTest ]; then
    ./ci-scripts/build.sh --build-unit-tests || {
        exit 1
    }
fi

cd build/test
./UnitTest || {
    exit 1
}
cd ..
