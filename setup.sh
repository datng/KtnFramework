# search for the name of the OS
if [ -f /etc/os-release ]; then
    # on systems with freedesktop.org and systemd, the distro name can be found in /etc/os-release
    source /etc/os-release
    OS=$PRETTY_NAME
    VER=$VERSION_ID
else
    # pretty much useless since it gives names and versions of the kernel and not the distro
    OS=$(uname -s)
    VER=$(uname -r)
fi

echo "Setup script running on $OS."

case $OS in
    openSUSE*)
    # build tools
    sudo zypper install --no-recommends gcc gcc-c++ libqt5-creator libqt5-qtbase-common-devel
    # library dependencies
    sudo zypper install --no-recommends assimp-devel glm-devel glu-devel libepoxy-devel libglfw-devel libvorbis-devel portaudio-devel vulkan-devel
    ;;

    Ubuntu*)
    sudo apt install cmake gcovr libassimp-dev libepoxy-dev libglfw3-dev libglm-dev libvorbis-dev libvulkan-dev portaudio19-dev qt5-default qtcreator --no-install-recommends
    ;;

    *)
    echo "\"$OS\" not supported."
    exit 1
    ;;
esac

./ci-scripts/prebuild.sh || {
    echo "Prebuild script failed."
    exit 1
}
