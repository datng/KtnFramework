#include <ktnEngine/Application/ktnRenderer_OpenGL.hpp>

#include <epoxy/gl.h>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>
#include <iostream>

using std::cout;
using std::endl;

using namespace ktn::Graphics;
namespace ktn {
ktnRenderer_OpenGL::ktnRenderer_OpenGL() = default;

ktnRenderer_OpenGL::~ktnRenderer_OpenGL() {
    delete m_BlurShader;

    delete m_BloomBuffer;

    delete m_FrameBuffer;
    delete m_FrameBufferMedium;
    delete m_FrameBufferSmall;
}

ExecutionStatus ktnRenderer_OpenGL::Initialize(GLFWwindow *window, const vk::Extent2D &eExtend2D, unsigned int shadowMapSize) {
    m_Window = window;

    auto width = eExtend2D.width;
    auto height = eExtend2D.height;

    glEnable(GL_DEPTH_TEST); // enable depth test
    // 3 lines for backface culling
    glFrontFace(GL_CCW); // set front face mode, clockwise or counter clockwise
    glEnable(GL_CULL_FACE); // enable culling
    glCullFace(GL_BACK); // set culliing mode to front, back or both

    // initialize renderer
    m_ShadowMapSize = shadowMapSize;

    m_Extend2D.setWidth(width);
    m_Extend2D.setHeight(height);
    // Set up the frame buffers
    m_FrameBuffer = new ktnFrameBuffer(FrameBufferType::COLOR_RGB, m_Extend2D, 2);

    m_DownscaleFactor_Medium = 2;
    vk::Extent2D downscaledExtent_Medium = vk::Extent2D(m_Extend2D.width / m_DownscaleFactor_Medium, m_Extend2D.height / m_DownscaleFactor_Medium);
    m_FrameBufferMedium = new ktnFrameBuffer(FrameBufferType::COLOR_RGB, downscaledExtent_Medium);
    m_BlurBuffersMedium = new ktnFrameBuffer[2] //
        {ktnFrameBuffer(FrameBufferType::COLOR_RGB, downscaledExtent_Medium), //
         ktnFrameBuffer(FrameBufferType::COLOR_RGB, downscaledExtent_Medium)};

    m_DownscaleFactor_Small = 16;
    vk::Extent2D downscaledExtent_Small = vk::Extent2D(m_Extend2D.width / m_DownscaleFactor_Small, m_Extend2D.height / m_DownscaleFactor_Small);
    m_FrameBufferSmall = new ktnFrameBuffer(FrameBufferType::COLOR_RGB, downscaledExtent_Small);
    m_BlurBuffersSmall = new ktnFrameBuffer[2] //
        {ktnFrameBuffer(FrameBufferType::COLOR_RGB, downscaledExtent_Small), //
         ktnFrameBuffer(FrameBufferType::COLOR_RGB, downscaledExtent_Small)};

    m_BloomBuffer = new ktnFrameBuffer(FrameBufferType::COLOR_RGB, m_Extend2D);

    // set up fullscreen quad
    float quadVertices[] = {// positions       // texCoords
                            -1.0f, 1.0f,  0.0f, 0.0f, 1.0f, //
                            -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, //
                            1.0f,  1.0f,  0.0f, 1.0f, 1.0f, //
                            1.0f,  -1.0f, 0.0f, 1.0f, 0.0f};

    glGenVertexArrays(1, &m_FullscreenRectangleVertexArray);
    glGenBuffers(1, &m_FullscreenRectangleVertexBuffer);
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, m_FullscreenRectangleVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
    return ExecutionStatus::OK;
}

void ktnRenderer_OpenGL::WaitIdle() {
    //
}

void ktnRenderer_OpenGL::CleanUp() {
    glDeleteVertexArrays(1, &m_FullscreenRectangleVertexArray);
    glDeleteBuffers(1, &m_FullscreenRectangleVertexBuffer);
}

void ktnRenderer_OpenGL::SetBloomShader(ktnShaderProgram *shader) {
    m_BloomShader = shader;
}

void ktnRenderer_OpenGL::SetBlurShader(ktnShaderProgram *shader) {
    m_BlurShader = shader;
}

void ktnRenderer_OpenGL::SetColorShader(ktnShaderProgram *shader) {
    m_ColorShader = shader;
}

void ktnRenderer_OpenGL::SetFullscreenRectangleShader(ktnShaderProgram *shader) {
    m_FullscreenRectangleShader = shader;
}

void ktnRenderer_OpenGL::SetShadowShader(ktnShaderProgram *shader) {
    m_ShadowShader = shader;
}

void ktnRenderer_OpenGL::Loop(ktnScene *scene) {
    RenderShadow(scene);
    RenderColor(scene);
    RenderBlurs();
    RenderBloom();
    RenderCombined();
    glfwSwapBuffers(m_Window);
}

void ktnRenderer_OpenGL::RenderBloom() {
    glViewport(0, 0, m_Extend2D.width, m_Extend2D.height);
    // apply blur to get bloom
    m_BloomBuffer->Bind();
    m_BloomShader->Use();

    glActiveTexture(GL_TEXTURE0 + m_FrameBuffer->Textures[0].ID());
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_FrameBuffer->Textures[0].ID());
    m_BloomShader->SetUniformInt("inputImage", m_FrameBuffer->Textures[0].ID());

    unsigned int lightBlurImgID = m_BlurBuffersMedium[1].Textures[0].ID();
    glActiveTexture(GL_TEXTURE0 + lightBlurImgID);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, lightBlurImgID);
    m_BloomShader->SetUniformInt("blurredImage", lightBlurImgID);

    unsigned int heavyBlurImgID = m_BlurBuffersSmall[1].Textures[0].ID();
    glActiveTexture(GL_TEXTURE0 + heavyBlurImgID);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, heavyBlurImgID);
    m_BloomShader->SetUniformInt("heavilyBlurredImage", heavyBlurImgID);

    m_BloomShader->SetUniformFloat("exposure", 1.0);
    m_BloomShader->SetUniformFloat("bigBlurIntensity", 0.5f);
    m_BloomShader->SetUniformFloat("smallBlurIntensity", 0.8f);
    WIP; // the values of big blur and small blur intensities should be variables
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    m_BloomBuffer->Unbind();
}

void ktnRenderer_OpenGL::RenderBlurs() {
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    m_BlurShader->Use();
    /*
     * for the lack of a better term,
     * orientation can be used to describe either horizontal or vertical,
     * with horizontal being true
     */
    bool orientation = true;
    bool firstIteration = true;

    glDisable(GL_DEPTH_TEST);
    // resize the original high-contrast image
    glViewport(0, 0, m_FrameBufferMedium->Extend2D().width, m_FrameBufferMedium->Extend2D().height);
    m_FrameBufferMedium->Bind();
    m_FullscreenRectangleShader->Use();
    glActiveTexture(GL_TEXTURE0 + m_FrameBuffer->Textures[1].ID());
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_FrameBuffer->Textures[1].ID());
    m_FullscreenRectangleShader->SetUniformInt("screenTexture", m_FrameBuffer->Textures[1].ID());
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    // framebufferMedium.Unbind();

    m_BlurShader->Use();
    unsigned int howmanytimes = 16;
    glDisable(GL_DEPTH_TEST); // so that the quad is not discarded
    for (unsigned int i = 0; i < howmanytimes; ++i) {
        m_BlurBuffersMedium[orientation].Bind();
        m_BlurShader->SetUniformInt("orientation", orientation);
        unsigned int textureNumber = firstIteration ? m_FrameBufferMedium->Textures[0].ID() : m_BlurBuffersMedium[!orientation].Textures[0].ID();
        glActiveTexture(GL_TEXTURE0 + textureNumber);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, textureNumber);
        m_BlurShader->SetUniformInt("inputImage", textureNumber);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        orientation = !orientation;
        if (firstIteration) firstIteration = false;
    }
    // glEnable(GL_DEPTH_TEST);
    m_BlurBuffersMedium[0].Unbind();

    // shrink the image down even more
    glViewport(0, 0, m_FrameBufferSmall->Extend2D().width, m_FrameBufferSmall->Extend2D().height);
    m_FrameBufferSmall->Bind();
    m_FullscreenRectangleShader->Use();
    glActiveTexture(GL_TEXTURE0 + m_FrameBufferMedium->Textures[0].ID());
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_FrameBufferMedium->Textures[0].ID());
    m_FullscreenRectangleShader->SetUniformInt("screenTexture", m_FrameBufferMedium->Textures[0].ID());
    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    m_FrameBufferSmall->Unbind();

    m_BlurShader->Use();
    /*
     * for the lack of a better term,
     * orientation can be used to describe either horizontal or vertical,
     * with horizontal being true
     */
    orientation = true;
    firstIteration = true;
    howmanytimes = 8;
    glDisable(GL_DEPTH_TEST); // so that the quad is not discarded
    for (unsigned int i = 0; i < howmanytimes; ++i) {
        m_BlurBuffersSmall[orientation].Bind();
        m_BlurShader->SetUniformInt("orientation", orientation);
        unsigned int textureNumber = firstIteration ? m_FrameBufferSmall->Textures[0].ID() : m_BlurBuffersSmall[!orientation].Textures[0].ID();
        glActiveTexture(GL_TEXTURE0 + textureNumber);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, textureNumber);
        m_BlurShader->SetUniformInt("inputImage", textureNumber);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        orientation = !orientation;
        if (firstIteration) firstIteration = false;
    }
    // glEnable(GL_DEPTH_TEST);
    m_BlurBuffersSmall[0].Unbind();
}

void ktnRenderer_OpenGL::RenderColor(ktnScene *scene) {
    glViewport(0, 0, m_FrameBuffer->Extend2D().width, m_FrameBuffer->Extend2D().height);
    m_FrameBuffer->Bind();
    m_ColorShader->Use();
    m_ColorShader->SetUniformFloat("time", 1.0f);
    WIP; // used for seeding random number generator, but not always needed
    m_ColorShader->SetUniformFloat("material.shininess", 32.0f);
    m_ColorShader->SetUniformInt("shadowmap", scene->ShadowBuffer_Directional[0]->ID());
    m_ColorShader->SetUniformMat4("LightSpaceMatrix", scene->Lights[0].ProjectionMatrix * scene->Lights[0].ViewMatrix);

    m_ColorShader->SetUpLight(scene->Lights[0]);

    float temp = std::chrono::steady_clock::now().time_since_epoch().count() % 10;
    m_ColorShader->SetUniformFloat("pseudorandomnumber", temp);

    m_ColorShader->SetUniformVec3("viewpos", scene->CurrentCamera()->Position());
    m_ColorShader->SetUniformMat4("ViewMatrix", scene->CurrentCamera()->ViewMatrix());

    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (ktnModel &model : scene->Models) {
        m_ColorShader->SetUniformMat4("ModelMatrix", model.ModelMatrix());
        for (ktnMesh &mesh : model.Meshes) mesh.Draw(*m_ColorShader);
    }
    m_FrameBuffer->Unbind();
}

void ktnRenderer_OpenGL::RenderCombined() {
    glViewport(0, 0, m_Extend2D.width, m_Extend2D.height);
    // use the fullscreen rectangle shader to render an arbitrary image from any of the previous stages
    m_FullscreenRectangleShader->Use();
    glDisable(GL_DEPTH_TEST);
    auto textureNumber = m_BloomBuffer->Textures[0].ID();
    glActiveTexture(GL_TEXTURE0 + textureNumber);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureNumber);
    m_FullscreenRectangleShader->SetUniformInt("screenTexture", textureNumber);
    glBindFramebuffer(GL_FRAMEBUFFER, 0); // set to draw to the on-screen framebuffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindVertexArray(m_FullscreenRectangleVertexArray);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glEnable(GL_DEPTH_TEST);
}

void ktnRenderer_OpenGL::RenderShadow(ktnScene *scene) {
    glEnable(GL_DEPTH_TEST); // enable depth test
    if (!scene->ShadowBuffer_Directional.empty()) {
        Graphics::ktnFrameBuffer *shadowBuffer = scene->ShadowBuffer_Directional[0];
        glViewport(0, 0, (int)shadowBuffer->Extend2D().width, (int)shadowBuffer->Extend2D().height);
        unsigned int depthMapLocation = scene->ShadowBuffer_Directional[0]->ID();
        shadowBuffer->Bind();
        // WIP
        float nearplane = 0.1f, farplane = 20.0f;
        glm::mat4 lightspace = IDENTITY_MATRIX_4;
        m_ShadowShader->Use();
        if (scene->Lights.empty()) {
            return; // prevent crash
        }

        m_ShadowShader->SetUniformMat4("ModelMatrix", scene->Lights[0].ModelMatrix());
        scene->Lights[0].ProjectionMatrix = glm::ortho(-2.0f, 2.0f, -2.0f, 2.0f, nearplane, farplane);
        scene->Lights[0].ViewMatrix =
            glm::lookAt(scene->Lights[0].Position(), scene->Lights[0].Position() + scene->Lights[0].Direction(), scene->CurrentCamera()->UniversalUp());
        lightspace = scene->Lights[0].ProjectionMatrix * scene->Lights[0].ViewMatrix;
        m_ShadowShader->SetUniformMat4("LightSpaceMatrix", lightspace);

        glActiveTexture(GL_TEXTURE0 + depthMapLocation);
        glBindTexture(GL_TEXTURE_2D, shadowBuffer->Textures[0].ID());
        // render objects
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        for (ktnModel &model : scene->Models) {
            m_ShadowShader->SetUniformMat4("ModelMatrix", model.ModelMatrix());
            for (ktnMesh &mesh : model.Meshes) mesh.Draw(*m_ShadowShader, depthMapLocation);
        }
        shadowBuffer->Unbind();
    }
}
} // namespace ktn
