﻿#include <ktnEngine/Application/ktnScene.hpp>

#include <iostream>

using ktn::Core::ktnName;
using std::cout;
using std::endl;
using std::string;

namespace ktn {
ktnScene::ktnScene() = default;

ktnScene::~ktnScene() = default;

void ktnScene::AddCamera(Graphics::ktnCamera eCamera) {
    eCamera.Name.AppendSuffix();
    ktnName tempName = eCamera.Name;
    if (!Cameras.empty()) {
        for (const Graphics::ktnCamera &camera : Cameras) {
            if (camera == eCamera) {
                return;
            }
            if (camera.Name == tempName) {
                tempName.BumpSuffix();
            }
        }
        if (tempName != eCamera.Name) {
            cout << "ktnScene: --duplicate name detected" << endl;
            cout << "ktnScene: --changing name of new camera to \"" << tempName.ToStdString() << "\"" << endl;
            eCamera.Name = tempName;
        }
    }
    Cameras.push_back(eCamera);
}

void ktnScene::RemoveCamera(const string &name) {
    cout << "ktnScene: RemoveCamera: removing camera: \"" << name << "\"" << endl;
    for (size_t i = 0; i < Cameras.size(); ++i) {
        if (Cameras.at(i).Name.ToStdString() == name) {
            Cameras.erase(Cameras.begin() + i);
            return;
        }
    }
    cout << "ktnScene::RemoveCamera: camera " << name << "\" not found for removal";
    WIP // what should the behavior be if we remove the current camera? reset it to the first one in the vector?
}

void ktnScene::AddLight(Graphics::ktnLight &eLight) {
    eLight.Name.AppendSuffix();
    ktnName tempName = eLight.Name;
    if (!Lights.empty()) {
        for (Graphics::ktnLight light : Lights) {
            if (light == eLight) {
                return;
            }
            if (light.Name == tempName) {
                tempName.BumpSuffix();
            }
        }
        if (tempName != eLight.Name) {
            cout << "ktnScene: --duplicate name detected" << endl;
            cout << "ktnScene: --changing name of new light to \"" << tempName.ToStdString() << "\"" << endl;
            eLight.Name = tempName;
        }
    }
    Lights.push_back(eLight);
}

void ktnScene::RemoveLight(const string &name) {
    cout << "ktnScene: RemoveLight: removing light: \"" << name << "\"" << endl;
    for (size_t i = 0; i < Lights.size(); ++i) {
        if (Lights.at(i).Name.ToStdString() == name) {
            Lights.erase(Lights.begin() + i);
            return;
        }
    }
    cout << "ktnScene::RemoveLight: light \"" << name << "\" not found for removal";
}

void ktnScene::AddModel(Graphics::ktnModel &eModel) {
    eModel.Name.AppendSuffix();
    ktnName tempName = eModel.Name;
    if (!Models.empty()) {
        for (Graphics::ktnModel &model : Models) {
            if (model == eModel) {
                return;
            }
            if (model.Name == tempName) {
                tempName.BumpSuffix();
            }
        }
        if (tempName != eModel.Name) {
            cout << "ktnScene: --duplicate name detected" << endl;
            cout << "ktnScene: --changing name of new model to \"" << tempName.ToStdString() << "\"" << endl;
            eModel.Name = tempName;
        }
    }
    Models.push_back(eModel);
}

void ktnScene::RemoveModel(const string &name) {
    cout << "ktnScene: RemoveModel: removing model: \"" << name << "\"" << endl;
    for (size_t i = 0; i < Models.size(); ++i) {
        if (Models.at(i).Name.ToStdString() == name) {
            Models.erase(Models.begin() + i);
            return;
        }
    }
    cout << "ktnScene::RemoveModel: model " << name << "\" not found for removal";
}

Graphics::ktnCamera *ktnScene::CurrentCamera() {
    return m_CurrentCamera;
}

void ktnScene::SetCurrentCamera(const string &name) {
    for (Graphics::ktnCamera &camera : Cameras) {
        if (name == camera.Name.ToStdString()) {
            m_CurrentCamera = &camera;
        }
        return;
    }
    cout << "ktnScene::SetCurrentCamera: camera \"" << name << "\" not found" << endl;
}

void ktnScene::ProcessInput() {}

void ktnScene::SetWindow(GLFWwindow *window) {
    m_Window = window;

    // cast the private member methods to static functions so that they can be assigned as callback for glfw
    glfwSetWindowUserPointer(m_Window, this);
    auto callback_MouseMove_static = [](GLFWwindow *window, double xPos, double yPos) {
        static_cast<ktnScene *>(glfwGetWindowUserPointer(window))->callback_MouseMove(window, xPos, yPos);
    };
    glfwSetCursorPosCallback(m_Window, callback_MouseMove_static);
    auto callback_MouseClick_static = [](GLFWwindow *window, int button, int action, int mods) {
        static_cast<ktnScene *>(glfwGetWindowUserPointer(window))->callback_MouseClick(window, button, action, mods);
    };
    glfwSetMouseButtonCallback(m_Window, callback_MouseClick_static);
    auto callback_FrameBufferSizeChange_static = [](GLFWwindow *window, int width, int height) {
        static_cast<ktnScene *>(glfwGetWindowUserPointer(window))->callback_FrameBufferSizeChange(window, width, height);
    };
    glfwSetFramebufferSizeCallback(m_Window, callback_FrameBufferSizeChange_static);
}
} // namespace ktn
