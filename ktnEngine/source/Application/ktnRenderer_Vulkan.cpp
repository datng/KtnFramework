﻿#include "ktnEngine/Application/ktnRenderer_Vulkan.hpp"

#include <algorithm> // for std::clamp
#include <fstream>
#include <iostream>
#include <set>

using std::array;
using std::cerr;
using std::cout;
using std::endl;
using std::exception;
using std::ifstream;
using std::ios;
using std::numeric_limits;
using std::runtime_error;
using std::set;
using std::string;
using std::vector;

static const vector<const char *> validationLayers = {"VK_LAYER_LUNARG_standard_validation"};

static const vector<const char *> requiredPhysicalDeviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

static const unsigned int MAX_NFRAMES = 3;

#ifdef KTN_DEBUG
static const bool validationLayersEnabled = true;

static VKAPI_ATTR VkBool32 VKAPI_CALL validationCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity, // TODO add description comments for the parameters
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT *pData,
    void *pUserData) {
    // A logger library should be used for this
    // for now everything is redirected to stdout
    if (severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
        cout << "Validation layers: Error: " << pData->pMessage << endl;
    } else if (severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
        cout << "Validation layers: Warning: " << pData->pMessage << endl;
    }
    //    else if(severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
    //        cout << "Validation layers: Info: " << pData->pMessage << endl;
    //    else if(severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
    //        cout << "Validation layers: Verbose: " << pData->pMessage << endl;
    else {
    }; // no other case
    return VK_FALSE;
}

#else
static const bool validationLayersEnabled = false;
#endif

namespace ktn {
ktnRenderer_Vulkan::ktnRenderer_Vulkan() {}

ktnRenderer_Vulkan::~ktnRenderer_Vulkan() {}

ExecutionStatus ktnRenderer_Vulkan::Initialize(GLFWwindow *window, const vk::Extent2D &eExtend2D, unsigned int shadowMapSize) {
    m_Window = window;
    m_Extend2D = eExtend2D;
    try {
        CreateInstance();
        SetUpDebugCallback();
        CreateSurface();
        PickPhysicalDevice();
        CreateLogicalDevice();
        CreateSwapchain();
        CreateImageViews();
        CreateRenderPass();
        CreatePipeline();
        CreateFrameBuffers();
        CreateCommandPool();
        CreateCommandBuffers();
        CreateSyncObjects();
    } catch (const exception &e) {
        cerr << e.what() << endl;
        return ExecutionStatus::FAILURE;
    }
    return ExecutionStatus::OK;
}

void ktnRenderer_Vulkan::CreateInstance() {
    if (validationLayersEnabled) {
        if (CheckValidationLayer() == ExecutionStatus::FAILURE) throw runtime_error("Could not find required validation layers.");
    }
    WIP // all parameters should be variables given by the user
        vk::ApplicationInfo appInfo(
            "Game", // TODO add description comments for the parameters
            VK_MAKE_VERSION(1, 0, 0),
            "KtnEngine",
            VK_MAKE_VERSION(1, 0, 0),
            VK_API_VERSION_1_0);

    auto extensions = GetRequiredExtentions();
    vk::InstanceCreateInfo ici(
        vk::InstanceCreateFlags(), // InstanceCreateFlags object
        &appInfo, // ApplicationInfo object
        validationLayersEnabled ? static_cast<uint32_t>(validationLayers.size()) : 0, // TODO add description comments for the parameters
        validationLayersEnabled ? validationLayers.data() : nullptr,
        static_cast<uint32_t>(extensions.size()),
        extensions.data());

    vk::Result result = vk::createInstance(
        &ici, // InstanceCreateInfo object
        nullptr, // TODO add description comments for the parameters
        &m_Instance);
    if (result != vk::Result::eSuccess) throw runtime_error("ktnRenderer_Vulkan::CreateInstance: Could not create Vulkan instance.");
}

void ktnRenderer_Vulkan::SetUpDebugCallback() {
#ifdef KTN_DEBUG
    VkDebugUtilsMessengerCreateInfoEXT dumci;
    dumci.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    dumci.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
                            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    dumci.messageType =
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    dumci.pfnUserCallback = validationCallback;
    dumci.flags = 0;

    PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT =
        reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(m_Instance.getProcAddr("vkCreateDebugUtilsMessengerEXT"));

    vkCreateDebugUtilsMessengerEXT(
        m_Instance, // TODO add description comments for the parameters
        &dumci,
        nullptr,
        &m_DebugUtilsMessenger);
#endif
}

void ktnRenderer_Vulkan::CreateSurface() {
    if (glfwCreateWindowSurface(m_Instance, m_Window, nullptr, &m_Surface) != VK_SUCCESS) {
        throw runtime_error("ktnRenderer_Vulkan::CreateSurface: Could not create Vulkan surface");
    }
}

void ktnRenderer_Vulkan::PickPhysicalDevice() {
    vector<vk::PhysicalDevice> availableDevices = m_Instance.enumeratePhysicalDevices();
    if (availableDevices.empty()) return;

    bool devicePicked = false;
    for (auto device : availableDevices) {
        if (ExecutionStatus::OK == CheckPhysicalDevice(device)) {
            WIP
                // instead of picking the first suitable device, we can iterate through a list of devices
                // and pick the ones with the best features.
                devicePicked = true;
            m_PhysicalDevice = device;
            break;
        }
    }
    if (!devicePicked) throw runtime_error("ktnRenderer_Vulkan::PickPhysicalDevice: No suitable device found.");
}

ExecutionStatus ktnRenderer_Vulkan::CreateLogicalDevice() {
    // create graphics and present queues
    if (!m_QueueFamilies.AreComplete()) return ExecutionStatus::FAILURE;

    vector<vk::DeviceQueueCreateInfo> dqcis;

    set<uint32_t> familiesIndexSet = {m_QueueFamilies.Graphics.Index().value(), m_QueueFamilies.Present.Index().value()};

    float priority = 1.0f;
    for (auto familyIndex : familiesIndexSet) {
        vk::DeviceQueueCreateInfo dqci = vk::DeviceQueueCreateInfo(vk::DeviceQueueCreateFlags(), dqci.queueFamilyIndex = familyIndex, 1, &priority);
        dqcis.push_back(dqci);
    }

    // for the time being we don't specify any neccessary physical device features
    vk::PhysicalDeviceFeatures pdf;

    vk::DeviceCreateInfo dci(
        vk::DeviceCreateFlags(), // DeviceCreateFlags object
        static_cast<uint32_t>(dqcis.size()), // DeviceQueueCreateInfo count
        dqcis.data(), // DeviceQueueCreateInfo objects
        validationLayersEnabled ? static_cast<uint32_t>(validationLayers.size()) : 0, // validation layer count
        validationLayersEnabled ? validationLayers.data() : nullptr, // validation layer names
        static_cast<uint32_t>(requiredPhysicalDeviceExtensions.size()), // device extension count
        requiredPhysicalDeviceExtensions.data(), // device extension names
        &pdf // PhysicalDeviceFeatures object
    );

    vk::Result r = m_PhysicalDevice.createDevice(
        &dci, // DeviceCreateInfo object
        nullptr, // allocation callback
        &m_LogicalDevice // Device object
    );
    if (vk::Result::eSuccess != r) return ExecutionStatus::FAILURE;

    m_GraphicsQueue = m_LogicalDevice.getQueue(m_QueueFamilies.Graphics.Index().value(), 0);
    m_PresentQueue = m_LogicalDevice.getQueue(m_QueueFamilies.Present.Index().value(), 0);
    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CreateSwapchain() {
    vk::SurfaceFormatKHR format = ChooseSwapSurfaceFormat(m_SurfaceFormats);
    vk::PresentModeKHR mode = ChooseSwapchainPresentMode(m_SwapchainPresentModes);
    vk::Extent2D extent = ChooseSwapExtent(m_SurfaceCapabilities);

    uint32_t nImages = m_SurfaceCapabilities.minImageCount + 1; // in case we want to use triple buffering
    // if maxImageCount == minImageCount then the above is obviously not possible and will produce error
    // 0 means there is no limits save for memory
    if (m_SurfaceCapabilities.maxImageCount > 0 && nImages > m_SurfaceCapabilities.maxImageCount) nImages = m_SurfaceCapabilities.maxImageCount;

    uint32_t queueFamilyIndices[2] = {m_QueueFamilies.Graphics.Index().value(), m_QueueFamilies.Present.Index().value()};

    vk::SwapchainCreateInfoKHR scik(
        vk::SwapchainCreateFlagsKHR(), // SwapchainCreateFlagsKHR object
        m_Surface, // VkSurfaceKHR object
        nImages, // number of images in the swapchain
        format.format, // VkSurfaceFormatKHR object
        format.colorSpace, // the color space of that image format
        extent, // Extent2D object
        1,
        vk::ImageUsageFlags(vk::ImageUsageFlagBits::eColorAttachment),
        m_QueueFamilies.Graphics.Index() != m_QueueFamilies.Present.Index() ? vk::SharingMode::eConcurrent : vk::SharingMode::eExclusive,
        m_QueueFamilies.Graphics.Index() != m_QueueFamilies.Present.Index() ? 2 : 0,
        m_QueueFamilies.Graphics.Index() != m_QueueFamilies.Present.Index() ? queueFamilyIndices : nullptr,
        m_SurfaceCapabilities.currentTransform,
        vk::CompositeAlphaFlagBitsKHR::eOpaque,
        mode,
        VK_TRUE,
        vk::SwapchainKHR());

    m_Swapchain = m_LogicalDevice.createSwapchainKHR(scik);
    if (m_Swapchain == vk::SwapchainKHR()) return ExecutionStatus::FAILURE;

    m_SwapchainImages = m_LogicalDevice.getSwapchainImagesKHR(m_Swapchain);
    if (m_SwapchainImages.empty()) return ExecutionStatus::FAILURE;

    m_SwapchainImageFormat = format.format;
    m_SwapchainExtent = extent;
    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CreateImageViews() {
    m_SwapchainImageViews.clear();
    for (size_t i = 0; i < m_SwapchainImages.size(); ++i) {
        vk::ImageViewCreateInfo ivci(
            vk::ImageViewCreateFlags(), // TODO add description comments for the parameters
            m_SwapchainImages[i],
            vk::ImageViewType::e2D,
            m_SwapchainImageFormat,
            vk::ComponentMapping(),
            vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));
        m_SwapchainImageViews.push_back(m_LogicalDevice.createImageView(ivci));
    }
    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CreateRenderPass() {
    vk::AttachmentDescription ad(
        vk::AttachmentDescriptionFlags(), // TODO add description comments for the parameters
        m_SwapchainImageFormat,
        vk::SampleCountFlagBits::e1,
        vk::AttachmentLoadOp::eClear,
        vk::AttachmentStoreOp::eStore,
        vk::AttachmentLoadOp::eDontCare,
        vk::AttachmentStoreOp::eDontCare,
        vk::ImageLayout::eUndefined,
        vk::ImageLayout::ePresentSrcKHR);

    vk::AttachmentReference ar(0, vk::ImageLayout::eColorAttachmentOptimal);

    vk::SubpassDescription sd(
        vk::SubpassDescriptionFlags(), // TODO add description comments for the parameters
        vk::PipelineBindPoint::eGraphics,
        0,
        nullptr,
        1,
        &ar,
        nullptr,
        nullptr,
        0,
        nullptr);

    vk::SubpassDependency dependency(
        VK_SUBPASS_EXTERNAL, // TODO add description comments for the parameters
        0,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        vk::AccessFlags(),
        vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite);

    vk::RenderPassCreateInfo rpci(
        vk::RenderPassCreateFlags(), // TODO add description comments for the parameters
        1,
        &ad,
        1,
        &sd,
        1,
        &dependency);

    m_RenderPass = m_LogicalDevice.createRenderPass(rpci);

    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CreatePipeline() {
    WIP // we want to have flexibility in this. Right now everything is hardcoded to show a triangle
        auto vertShaderBuffer = GetShaderFromFile("resources/shaders/basic_triangle/vert.spv");
    auto fragShaderBuffer = GetShaderFromFile("resources/shaders/basic_triangle/frag.spv");

    vk::ShaderModule vertShaderModule = CreateShaderModule(vertShaderBuffer);
    vk::ShaderModule fragShaderModule = CreateShaderModule(fragShaderBuffer);

    vk::PipelineShaderStageCreateInfo pssci_vert(
        vk::PipelineShaderStageCreateFlags(), // TODO add description comments for the parameters
        vk::ShaderStageFlagBits::eVertex,
        vertShaderModule,
        "main");

    vk::PipelineShaderStageCreateInfo pssci_frag(
        vk::PipelineShaderStageCreateFlags(), // TODO add description comments for the parameters
        vk::ShaderStageFlagBits::eFragment,
        fragShaderModule,
        "main");

    vector<vk::PipelineShaderStageCreateInfo> stages = {pssci_vert, pssci_frag};

    // start of fixed functions:
    auto pvisci = vk::PipelineVertexInputStateCreateInfo();
    WIP // all default values because we hard coded the vertex in the shader
        ;

    vk::PipelineInputAssemblyStateCreateInfo piasci(
        vk::PipelineInputAssemblyStateCreateFlags(), // TODO add description comments for the parameters
        vk::PrimitiveTopology::eTriangleList,
        VK_FALSE);

    vk::Viewport viewport(
        0.0f, // TODO add description comments for the parameters
        0.0f,
        m_SwapchainExtent.width,
        m_SwapchainExtent.height,
        0.0f,
        1.0f);

    vk::Rect2D scissor(vk::Offset2D(0, 0), m_SwapchainExtent);

    vk::PipelineViewportStateCreateInfo pvsci(
        vk::PipelineViewportStateCreateFlags(), // TODO add description comments for the parameters
        1,
        &viewport,
        1,
        &scissor);

    vk::PipelineRasterizationStateCreateInfo prsci(
        vk::PipelineRasterizationStateCreateFlags(), // TODO add description comments for the parameters
        VK_FALSE,
        VK_FALSE,
        vk::PolygonMode::eFill,
        vk::CullModeFlagBits::eBack,
        vk::FrontFace::eClockwise,
        VK_FALSE,
        0.0f,
        0.0f,
        0.0f,
        1.0f);

    vk::PipelineMultisampleStateCreateInfo pmsci(
        vk::PipelineMultisampleStateCreateFlags(), // TODO add description comments for the parameters
        vk::SampleCountFlagBits::e1,
        VK_FALSE,
        1.0f,
        nullptr,
        VK_FALSE,
        VK_FALSE);

    vk::PipelineColorBlendAttachmentState pcbas(
        VK_FALSE, // TODO add description comments for the parameters
        vk::BlendFactor::eOne,
        vk::BlendFactor::eZero,
        vk::BlendOp::eAdd,
        vk::BlendFactor::eOne,
        vk::BlendFactor::eZero,
        vk::BlendOp::eAdd,
        vk::ColorComponentFlags(
            vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA));

    vk::PipelineColorBlendStateCreateInfo pcbsci(
        vk::PipelineColorBlendStateCreateFlags(), // TODO add description comments for the parameters
        VK_FALSE,
        vk::LogicOp::eCopy,
        1,
        &pcbas,
        {{0.0f, 0.0f, 0.0f, 0.0f}});

    //    vk::DynamicState dynamicStates[] = {vk::DynamicState::eViewport,
    //                                        vk::DynamicState::eLineWidth};

    //    vk::PipelineDynamicStateCreateInfo(vk::PipelineDynamicStateCreateFlags(),
    //                                       2,
    //                                       dynamicStates);

    auto plci = vk::PipelineLayoutCreateInfo();
    m_PipelineLayout = m_LogicalDevice.createPipelineLayout(plci);

    // this part has weird line breaks because the IDE doesn't show hints after a certain amount of lines
    vk::GraphicsPipelineCreateInfo gpci(
        vk::PipelineCreateFlags(), // TODO add description comments for the parameters
        2,
        stages.data(),
        &pvisci,
        &piasci,
        nullptr,
        &pvsci,
        &prsci,
        &pmsci,
        nullptr,
        &pcbsci,
        nullptr,
        m_PipelineLayout,
        m_RenderPass,
        0,
        vk::Pipeline(),
        -1);

    // the return type of the create pipeline method is a vector, so we take the first element
    m_Pipeline = m_LogicalDevice.createGraphicsPipelines(m_PipelineCache, gpci)[0];

    m_LogicalDevice.destroyShaderModule(vertShaderModule);
    m_LogicalDevice.destroyShaderModule(fragShaderModule);
    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CreateFrameBuffers() {
    m_SwapchainFrameBuffers.clear();
    for (size_t i = 0; i < m_SwapchainImageViews.size(); ++i) {
        auto attachment = m_SwapchainImageViews[i];
        vk::FramebufferCreateInfo fci(
            vk::FramebufferCreateFlags(), // TODO add description comments for the parameters
            m_RenderPass,
            1,
            &attachment,
            m_SwapchainExtent.width,
            m_SwapchainExtent.height,
            1);
        m_SwapchainFrameBuffers.push_back(m_LogicalDevice.createFramebuffer(fci));
    }
    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CreateCommandPool() {
    vk::CommandPoolCreateInfo cpci(
        vk::CommandPoolCreateFlags(), // TODO add description comments for the parameters
        m_QueueFamilies.Graphics.Index().value());
    m_CommandPool = m_LogicalDevice.createCommandPool(cpci);
    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CreateCommandBuffers() {
    vk::CommandBufferAllocateInfo cbai(
        m_CommandPool, // TODO add description comments for the parameters
        vk::CommandBufferLevel::ePrimary,
        m_SwapchainFrameBuffers.size());
    m_CommandBuffers = m_LogicalDevice.allocateCommandBuffers(cbai);

    vk::ClearValue clearColor(array<float, 4>({0.0f, 0.0f, 0.0f, 1.0f}));

    for (size_t i = 0; i < m_CommandBuffers.size(); ++i) {
        vk::CommandBufferBeginInfo cbbi(vk::CommandBufferUsageFlagBits::eSimultaneousUse, nullptr);
        m_CommandBuffers[i].begin(cbbi);

        vk::RenderPassBeginInfo rpbi(m_RenderPass, m_SwapchainFrameBuffers[i], vk::Rect2D(vk::Offset2D(0, 0), m_SwapchainExtent), 1, &clearColor);
        m_CommandBuffers[i].beginRenderPass(rpbi, vk::SubpassContents::eInline);
        m_CommandBuffers[i].bindPipeline(vk::PipelineBindPoint::eGraphics, m_Pipeline);
        m_CommandBuffers[i].draw(3, 1, 0, 0);
        m_CommandBuffers[i].endRenderPass();
        m_CommandBuffers[i].end();
    }

    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CreateSyncObjects() {
    vk::SemaphoreCreateInfo sci;
    vk::FenceCreateInfo fci(vk::FenceCreateFlagBits::eSignaled);
    m_Semaphores_ImgAvailable.clear();
    m_Semaphores_RenderDone.clear();
    m_Fences.clear();
    for (unsigned int i = 0; i < MAX_NFRAMES; ++i) {
        m_Semaphores_ImgAvailable.push_back(m_LogicalDevice.createSemaphore(sci));
        m_Semaphores_RenderDone.push_back(m_LogicalDevice.createSemaphore(sci));
        m_Fences.push_back(m_LogicalDevice.createFence(fci));
    }
    return ExecutionStatus::OK;
}

void ktnRenderer_Vulkan::Draw() {
    m_LogicalDevice.waitForFences(m_Fences[m_CurrentFrame], VK_TRUE, numeric_limits<uint64_t>::max());
    m_LogicalDevice.resetFences(m_Fences[m_CurrentFrame]);

    uint32_t index;
    m_LogicalDevice.acquireNextImageKHR(
        m_Swapchain, // TODO add description comments for the parameters
        numeric_limits<uint64_t>::max(),
        m_Semaphores_ImgAvailable[m_CurrentFrame],
        vk::Fence(),
        &index);

    vk::Semaphore waitSemaphores[] = {m_Semaphores_ImgAvailable[m_CurrentFrame]};
    vk::Semaphore signalSemaphores[] = {m_Semaphores_RenderDone[m_CurrentFrame]};
    vk::PipelineStageFlags waitStages[] = {vk::PipelineStageFlagBits::eColorAttachmentOutput};
    vk::SubmitInfo si(
        1,
        waitSemaphores, // TODO add description comments for the parameters
        waitStages,
        1,
        &m_CommandBuffers[index],
        1,
        signalSemaphores);
    m_GraphicsQueue.submit(1, &si, m_Fences[m_CurrentFrame]);

    vk::SwapchainKHR chains[] = {m_Swapchain};
    vk::PresentInfoKHR presentInfo(1, signalSemaphores, 1, chains, &index, nullptr);
    m_PresentQueue.presentKHR(&presentInfo);
    m_PresentQueue.waitIdle();
    m_CurrentFrame = (m_CurrentFrame + 1) % MAX_NFRAMES;
}

ExecutionStatus ktnRenderer_Vulkan::CheckValidationLayer() {
    vector<vk::LayerProperties> availableLayers = vk::enumerateInstanceLayerProperties();

    // search for available validation layers, and see if all the layers defined in validationLayers are found
    for (auto layer : validationLayers) {
        bool layerAvailable = false;
        for (auto availableLayer : availableLayers) {
            // Comparing arrays of characters means comparing the values that the pointer points to
            if (*availableLayer.layerName == *layer) {
                layerAvailable = true;
                break;
            }
        }
        if (layerAvailable == false) {
            cerr << "ktnRenderer_Vulkan::CheckValidationLayer: Validation layer " << layer << " not found" << endl;
            return ExecutionStatus::FAILURE;
        }
    }
    return ExecutionStatus::OK;
}

ExecutionStatus ktnRenderer_Vulkan::CheckQueueFamilies(vk::PhysicalDevice device) {
    vector<vk::QueueFamilyProperties> vQFP = device.getQueueFamilyProperties();
    if (vQFP.size() == 0) return ExecutionStatus::FAILURE;
    // Find graphics queue family
    for (size_t i = 0; i < vQFP.size(); ++i) {
        if (vQFP[i].queueCount > 0) {
            if (vQFP[i].queueFlags & vk::QueueFlagBits::eGraphics) {
                m_QueueFamilies.Graphics.SetIndex(i);
                m_QueueFamilies.Graphics.SetCount(vQFP[i].queueCount);
                break;
            }
        }
    }

    // Find present queue family
    for (size_t i = 0; i < vQFP.size(); ++i) {
        if (vQFP[i].queueCount > 0) {
            VkBool32 presentModeSupported = true;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, m_Surface, &presentModeSupported);
            if (presentModeSupported) {
                m_QueueFamilies.Present.SetIndex(i);
                m_QueueFamilies.Present.SetCount(vQFP[i].queueCount);
                break;
            }
        }
    }

    if (m_QueueFamilies.AreComplete()) {
        return ExecutionStatus::OK;
    } else {
        return ExecutionStatus::FAILURE;
    }
}

ExecutionStatus ktnRenderer_Vulkan::CheckPhysicalDevice(vk::PhysicalDevice device) {
    // Check for certain features/properties of the physical devices
    // For the time being it is not needed.
    vk::PhysicalDeviceFeatures devFeats = device.getFeatures();
    vk::PhysicalDeviceProperties devProps = device.getProperties();

    // Check for the queue families of the physical devices to make sure that we can submit commands to queues later on.
    if (ExecutionStatus::OK == CheckQueueFamilies(device)) {
        if (ExecutionStatus::OK == CheckPhysicalDeviceExtensionsSupport(device)) {
            if (ExecutionStatus::OK == CheckPhysicalDeviceSwapchainSupport(device)) return ExecutionStatus::OK;
        }
    }
    return ExecutionStatus::FAILURE;
}

ExecutionStatus ktnRenderer_Vulkan::CheckPhysicalDeviceExtensionsSupport(vk::PhysicalDevice device) {
    vector<vk::ExtensionProperties> availableExtensions = device.enumerateDeviceExtensionProperties();
    set<string> requiredExtensions(requiredPhysicalDeviceExtensions.begin(), requiredPhysicalDeviceExtensions.end());

    for (auto extension : availableExtensions) requiredExtensions.erase(extension.extensionName);

    if (requiredExtensions.empty()) {
        return ExecutionStatus::OK;
    } else {
        return ExecutionStatus::FAILURE;
    }
}

ExecutionStatus ktnRenderer_Vulkan::CheckPhysicalDeviceSwapchainSupport(vk::PhysicalDevice device) {
    // get surface capabilities
    m_SurfaceCapabilities = device.getSurfaceCapabilitiesKHR(m_Surface);
    if (m_SurfaceCapabilities == vk::SurfaceCapabilitiesKHR()) return ExecutionStatus::FAILURE;

    // get surface formats
    m_SurfaceFormats = device.getSurfaceFormatsKHR(m_Surface);
    if (m_SurfaceFormats.empty()) return ExecutionStatus::FAILURE;
    // get surface present modes
    m_SwapchainPresentModes = device.getSurfacePresentModesKHR(m_Surface);
    if (m_SwapchainPresentModes.empty()) return ExecutionStatus::FAILURE;
    return ExecutionStatus::OK;
}

vk::SurfaceFormatKHR ktnRenderer_Vulkan::ChooseSwapSurfaceFormat(const vector<vk::SurfaceFormatKHR> &formats) {
    vk::SurfaceFormatKHR preferedFormat;
    preferedFormat.format = vk::Format::eB8G8R8A8Unorm;
    preferedFormat.colorSpace = vk::ColorSpaceKHR::eSrgbNonlinear;

    // best case: surface has no prefered format, take our preferedFormat
    if (formats.size() == 1 && formats[0].format == vk::Format::eUndefined) return preferedFormat;
    // otherwise, see if there's any match to our prefered format;
    for (auto const &format : formats) {
        if (format.format == vk::Format::eB8G8R8A8Unorm //
            && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
            return preferedFormat;
        }
    }
    // otherwise, find way to settle with the "best matched" format
    WIP // settle for the first supported format in the mean time
        return formats[0];
}

vk::PresentModeKHR ktnRenderer_Vulkan::ChooseSwapchainPresentMode(const vector<vk::PresentModeKHR> &modes) {
    WIP
        // The individual application should be able to set this instead of letting the engine choose automatically.

        // Triple buffering if possible
        for (auto const &mode : modes) {
        if (mode == vk::PresentModeKHR::eMailbox) return mode;
    }
    // Otherwise double buffering
    for (auto const &mode : modes) {
        if (mode == vk::PresentModeKHR::eFifo) return mode;
    }
    // Otherwise relaxed double buffering, may introduce some tearing when an image is displayed too long
    for (auto const &mode : modes) {
        if (mode == vk::PresentModeKHR::eFifoRelaxed) return mode;
    }
    // Otherwise no v-sync whatsoever
    return vk::PresentModeKHR::eImmediate;
}

vk::Extent2D ktnRenderer_Vulkan::ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR &capabilities) {
    if (capabilities.currentExtent.width != numeric_limits<uint32_t>::max() && capabilities.currentExtent.height != numeric_limits<uint32_t>::max()) {
        return capabilities.currentExtent;
    } else {
        return vk::Extent2D(
            std::clamp(m_Extend2D.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width),
            std::clamp(m_Extend2D.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height));
    }
}

vk::ShaderModule ktnRenderer_Vulkan::CreateShaderModule(const vector<char> &buffer) {
    vk::ShaderModuleCreateInfo smci(
        vk::ShaderModuleCreateFlags(), // TODO add description comments for the parameters
        buffer.size(),
        reinterpret_cast<const uint32_t *>(buffer.data()));
    return m_LogicalDevice.createShaderModule(smci);
}

vector<const char *> ktnRenderer_Vulkan::GetRequiredExtentions() {
    uint32_t nGlfwExtensions = 0;
    const char **glfwExtensions = glfwGetRequiredInstanceExtensions(&nGlfwExtensions);
    vector<const char *> extensions(glfwExtensions, glfwExtensions + nGlfwExtensions);

#ifdef KTN_DEBUG
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
    return extensions;
}

vector<char> ktnRenderer_Vulkan::GetShaderFromFile(const string &filepath) {
    // read file as binary from the end of file
    // ate is there so that we can allocate a buffer with the right size
    ifstream file(filepath, ios::ate | ios::binary);
    if (!file.is_open()) throw runtime_error("ktnRenderer_Vulkan::GetShaderFromFile: Could not open " + filepath);
    size_t howBig = file.tellg();
    vector<char> buffer(howBig);
    file.seekg(0);
    file.read(buffer.data(), howBig);
    file.close();
    return buffer;
}

void ktnRenderer_Vulkan::Loop(ktnScene *scene) {
    Draw();
}

void ktnRenderer_Vulkan::WaitIdle() {
    m_LogicalDevice.waitIdle();
}

void ktnRenderer_Vulkan::CleanUp() {
    for (unsigned int i = 0; i < MAX_NFRAMES; ++i) {
        m_LogicalDevice.destroyFence(m_Fences[i]);
        m_LogicalDevice.destroySemaphore(m_Semaphores_ImgAvailable[i]);
        m_LogicalDevice.destroySemaphore(m_Semaphores_RenderDone[i]);
    }
    m_LogicalDevice.destroyCommandPool(m_CommandPool);
    for (auto framebuffer : m_SwapchainFrameBuffers) m_LogicalDevice.destroyFramebuffer(framebuffer);
    m_LogicalDevice.destroyPipeline(m_Pipeline);
    m_LogicalDevice.destroyPipelineLayout(m_PipelineLayout);
    m_LogicalDevice.destroyRenderPass(m_RenderPass);
    for (auto iv : m_SwapchainImageViews) m_LogicalDevice.destroyImageView(iv);
    m_LogicalDevice.destroySwapchainKHR(m_Swapchain);
    m_LogicalDevice.destroy();
#ifdef KTN_DEBUG
    // dynamic loader for vkDestroyDebugUtilsMessengerEXT, since it's not a core functionality of vulkan
    PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT =
        reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(m_Instance.getProcAddr("vkDestroyDebugUtilsMessengerEXT"));
    vkDestroyDebugUtilsMessengerEXT(m_Instance, m_DebugUtilsMessenger, nullptr);
#endif

    m_Instance.destroySurfaceKHR(m_Surface);
    m_Instance.destroy();
}
void ktnRenderer_Vulkan::RenderShadow(ktnScene *scene) {}
void ktnRenderer_Vulkan::RenderColor(ktnScene *scene) {}
void ktnRenderer_Vulkan::RenderBlurs() {}
void ktnRenderer_Vulkan::RenderBloom() {}
void ktnRenderer_Vulkan::RenderCombined() {}
void ktnRenderer_Vulkan::SetShadowShader(Graphics::ktnShaderProgram *shader) {}
void ktnRenderer_Vulkan::SetColorShader(Graphics::ktnShaderProgram *shader) {}
void ktnRenderer_Vulkan::SetBlurShader(Graphics::ktnShaderProgram *shader) {}
void ktnRenderer_Vulkan::SetFullscreenRectangleShader(Graphics::ktnShaderProgram *shader) {}
void ktnRenderer_Vulkan::SetBloomShader(Graphics::ktnShaderProgram *shader) {}

} // namespace ktn
