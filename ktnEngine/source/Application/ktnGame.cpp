#include <ktnEngine/Application/ktnGame.hpp>

#include <ktnEngine/Application/ktnRenderer_OpenGL.hpp>
#include <ktnEngine/Application/ktnRenderer_Vulkan.hpp>

#include <chrono>
#include <iostream>
#include <mutex>

using std::cout;
using std::endl;
using std::string;
using std::chrono::system_clock;

namespace ktn {
ktnGame::ktnGame(const Graphics::Backend &backend, const string &windowName) {
    Extent2D.width = 1280;
    Extent2D.height = 720;
    ShadowMapSize = 1024;
    GraphicsBackend = backend;
    switch (GraphicsBackend) {
    case Graphics::Backend::OPENGL:
        Renderer = new ktnRenderer_OpenGL();
        break;
    case Graphics::Backend::VULKAN:
        Renderer = new ktnRenderer_Vulkan();
        break;
    }
    Initialize(windowName);
}

ktnGame::~ktnGame() {
    if (Renderer != nullptr) {
        Renderer->CleanUp();
        delete Renderer;
    }
    glfwDestroyWindow(Window);
    glfwTerminate();
}

void ktnGame::Run() {
    auto processInput = [this]() { // cast the ProcessInput method so that it can use the one defined in the inherited class
        m_ActiveScene->ProcessInput();
    };
    while (glfwWindowShouldClose(Window) == 0) {
        processInput();
        Renderer->Loop(m_ActiveScene);
        glfwPollEvents();
    }
    Renderer->WaitIdle();
}

void ktnGame::SetActiveScene(ktnScene *scene) {
    if (m_ActiveScene != nullptr) {
        for (Graphics::ktnModel &model : m_ActiveScene->Models) { model.UnloadFromGPU(); }
    }
    m_ActiveScene = scene;
    for (Graphics::ktnModel &model : m_ActiveScene->Models) { model.LoadToGPU(); }
    m_ActiveScene->SetWindow(Window);
}

int ktnGame::Initialize(const string &windowName) {
    glfwInit();
    switch (GraphicsBackend) {
    case Graphics::Backend::OPENGL:
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        break;
    case Graphics::Backend::VULKAN:
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        break;
    }

    Window = glfwCreateWindow(Extent2D.width, Extent2D.height, windowName.c_str(), nullptr, nullptr);
    if (Window == nullptr) {
        cout << "ktnGame: could not create GLFW window" << endl;
        return -1;
    }

    glfwMakeContextCurrent(Window);

    glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSwapInterval(0); // vsync option, wait till the number inside is rendered
    Renderer->Initialize(Window, Extent2D, ShadowMapSize);
    return 0;
}

ktnScene *ktnGame::ActiveScene() {
    return m_ActiveScene;
}
} // namespace ktn
