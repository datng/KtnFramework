#include <ktnEngine/Graphics/ktnLight.hpp>

using glm::vec3;
using ktn::Core::ktnName;
using ktn::Core::Utils::CompareFloat;
using ktn::Core::Utils::CompareFloatResult;
using std::string;

namespace ktn {
namespace Graphics {
////////////////////
// ktnLight::hCutoff
ktnLight::hCutoff::hCutoff(const float &eInnerAngle, const float &eOuterAngle) {
    assert(eInnerAngle <= eOuterAngle);
    m_Inner = glm::cos(glm::radians(eInnerAngle));
    m_Outer = glm::cos(glm::radians(eOuterAngle));
}

bool ktnLight::hCutoff::operator==(const hCutoff &eCutoff) const {
    if (CompareFloat(m_Inner, eCutoff.InnerCos()) != CompareFloatResult::Equal //
        || CompareFloat(m_Outer, eCutoff.OuterCos()) != CompareFloatResult::Equal) {
        return false;
    }
    return true;
}

bool ktnLight::hCutoff::operator!=(hCutoff eCutoff) const {
    if (CompareFloat(m_Inner, eCutoff.InnerCos()) != CompareFloatResult::Equal //
        || CompareFloat(m_Outer, eCutoff.OuterCos()) != CompareFloatResult::Equal) {
        return true;
    }
    return false;
}

/////////////////////
// ktnLight::hFalloff
ktnLight::hFalloff::hFalloff(const float &eLinearTerm, const float &eQuadraticTerm) {
    LinearTerm = eLinearTerm;
    QuadraticTerm = eQuadraticTerm;
}

bool ktnLight::hFalloff::operator==(hFalloff eFalloff) const {
    if (CompareFloat(LinearTerm, eFalloff.LinearTerm) != CompareFloatResult::Equal //
        || CompareFloat(QuadraticTerm, eFalloff.QuadraticTerm) != CompareFloatResult::Equal) {
        return false;
    }
    return true;
}

bool ktnLight::hFalloff::operator!=(hFalloff eFalloff) const {
    if (CompareFloat(LinearTerm, eFalloff.LinearTerm) != CompareFloatResult::Equal //
        || CompareFloat(QuadraticTerm, eFalloff.QuadraticTerm) != CompareFloatResult::Equal) {
        return true;
    }
    return false;
}

///////////
// ktnLight
ktnLight::ktnLight(const ktnName &eName) : ktn3DElement(eName) {}

ktnLight::~ktnLight() = default;

bool ktnLight::operator==(const ktnLight &eLight) {
    if (ktn3DElement::operator!=(eLight) //
        || Cutoff != eLight.Cutoff //
        || Falloff != eLight.Falloff //
        || CompareFloat(Intensity, eLight.Intensity) != CompareFloatResult::Equal //
        || IsCastingShadows != eLight.IsCastingShadows //
        || m_Color != eLight.Color() //
        || m_Direction != eLight.Direction() //
        || ProjectionMatrix != eLight.ProjectionMatrix //
        || Type != eLight.Type //
        || ViewMatrix != eLight.ViewMatrix) {
        return false;
    }
    return true;
}

bool ktnLight::operator!=(const ktnLight &eLight) {
    if (ktn3DElement::operator!=(eLight) //
        || Cutoff != eLight.Cutoff //
        || Falloff != eLight.Falloff //
        || CompareFloat(Intensity, eLight.Intensity) != CompareFloatResult::Equal //
        || IsCastingShadows != eLight.IsCastingShadows //
        || m_Color != eLight.Color() //
        || m_Direction != eLight.Direction() //
        || ProjectionMatrix != eLight.ProjectionMatrix //
        || Type != eLight.Type //
        || ViewMatrix != eLight.ViewMatrix) {
        return true;
    }
    return false;
}

ktnLight &ktnLight::operator=(const ktnLight &eLight) {
    ktn3DElement::operator=(eLight);
    Cutoff = eLight.Cutoff;
    Falloff = eLight.Falloff;
    Intensity = eLight.Intensity;
    IsCastingShadows = eLight.IsCastingShadows;
    m_Color = eLight.Color();
    m_Direction = eLight.Direction();
    ProjectionMatrix = eLight.ProjectionMatrix;
    Type = eLight.Type;
    ViewMatrix = eLight.ViewMatrix;

    return *this;
}

vec3 ktnLight::Color() const {
    return m_Color;
}

vec3 ktnLight::Direction() const {
    return m_Direction;
}

void ktnLight::SetColor(const vec3 &col) {
    m_Color = col;
}

void ktnLight::SetColor(const float &r, const float &g, const float &b) {
    m_Color.r = r;
    m_Color.g = g;
    m_Color.b = b;
}

void ktnLight::SetDirection(const vec3 &dir) {
    WIP; // direction can be calculated by rotating the default direction with the rotation matrix
    m_Direction = dir;
}
} // namespace Graphics
} // namespace ktn
