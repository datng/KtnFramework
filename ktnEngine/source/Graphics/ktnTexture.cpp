#include "ktnEngine/Graphics/ktnTexture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"
#include <epoxy/gl.h>

#include <iostream>

using std::cout;
using std::endl;
using std::string;

namespace ktn {
namespace Graphics {
ktnTexture::ktnTexture(const FrameBufferType &type, const vk::Extent2D &eExtend2D) {
    m_Path = "";
    m_Extend2D = eExtend2D;
    CreateOnGPU(type);
}

ktnTexture::ktnTexture(const string &path, const TextureType &type) {
    m_Path = path;
    m_Type = type;
    // LoadToGPU();
}

ktnTexture::~ktnTexture() = default;

bool ktnTexture::operator==(const ktnTexture &eTexture) {
    if (m_Path != eTexture.Path() //
        || m_Type != eTexture.Type() //
        || m_ID != eTexture.ID() //
        || m_Extend2D != eTexture.Extend2D() //
        || m_nChannels != eTexture.nChannels()) {
        return false;
    }
    return true;
}

bool ktnTexture::operator!=(const ktnTexture &eTexture) {
    if (m_Path != eTexture.Path() //
        || m_Type != eTexture.Type() //
        || m_ID != eTexture.ID() //
        || m_Extend2D != eTexture.Extend2D() //
        || m_nChannels != eTexture.nChannels()) {
        return true;
    }
    return false;
}

void ktnTexture::CreateOnGPU(const FrameBufferType &type) {
    glGenTextures(1, &m_ID);
    glBindTexture(GL_TEXTURE_2D, m_ID);
    if (FrameBufferType::COLOR_RGB == type) {
        glTexImage2D( //
            GL_TEXTURE_2D,
            0,
            GL_RGB16F,
            static_cast<GLsizei>(m_Extend2D.width),
            static_cast<GLsizei>(m_Extend2D.height),
            0,
            GL_RGB,
            GL_FLOAT,
            nullptr);
        // set texture wrapping parameters
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    } else if (FrameBufferType::DEPTH == type) {
        glTexImage2D( //
            GL_TEXTURE_2D,
            0,
            GL_DEPTH_COMPONENT,
            static_cast<GLsizei>(m_Extend2D.width),
            static_cast<GLsizei>(m_Extend2D.height),
            0,
            GL_DEPTH_COMPONENT,
            GL_FLOAT,
            nullptr);
        // set texture wrapping parameters
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

        /* For shadow mapping, we want the value outside of the border to be white
         * so that when shadow is mapped on to the final image,
         * the pixels that map to outside the light frustum will not have a black shadow
         * This does not account for the pseudoshadow caused by pixels that are out of the far plane
         * which we have to address inside the fragment shader of the combined pass
         */
        std::vector<float> bordercolor = {1.0, 1.0, 1.0, 1.0};
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bordercolor.data());
    }
    // set texture filtering parameters
    SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void ktnTexture::LoadToGPU() {
    glGenTextures(1, &m_ID);
    int width = 0;
    int height = 0;
    unsigned char *data = stbi_load(m_Path.c_str(), &width, &height, &m_nChannels, 0);
    if (data != nullptr) {
        m_Extend2D.width = static_cast<uint32_t>(width);
        m_Extend2D.height = static_cast<uint32_t>(height);
        GLenum format;
        if (m_nChannels == 1) {
            format = GL_RED;
        } else if (m_nChannels == 3) {
            format = GL_RGB;
        } else if (m_nChannels == 4) {
            format = GL_RGBA;
        } else { // to prevent format being uninitialized
            cout << "ktnTexture: The texture at " << m_Path << " has " << m_nChannels << " color channels, which is not supported. "
                 << "An image with 1, 3 or 4 channels is needed." << endl;
            stbi_image_free(data);
            return;
        }
        glBindTexture(GL_TEXTURE_2D, m_ID);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            static_cast<GLint>(format),
            static_cast<GLsizei>(m_Extend2D.width),
            static_cast<GLsizei>(m_Extend2D.height),
            0,
            format,
            GL_UNSIGNED_BYTE,
            data);
        glGenerateMipmap(GL_TEXTURE_2D);

        // set texture wrapping parameters
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        // set texture filtering parameters
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        SetTextureParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
        stbi_image_free(data);
    } else {
        cout << "ktnTexture: Failed to load texture: " << m_Path << endl;
        stbi_image_free(data);
    }
}

void ktnTexture::Unload() {
    glDeleteTextures(1, &m_ID);
}

void ktnTexture::SetTextureParameter(unsigned int target, unsigned int paramName, int param) {
    glTexParameteri(target, paramName, param);
}

string ktnTexture::Path() const {
    return m_Path;
}

TextureType ktnTexture::Type() const {
    return m_Type;
}

GLuint ktnTexture::ID() const {
    return m_ID;
}

vk::Extent2D ktnTexture::Extend2D() const {
    return m_Extend2D;
}

int ktnTexture::nChannels() const {
    return m_nChannels;
}
} // namespace Graphics
} // namespace ktn
