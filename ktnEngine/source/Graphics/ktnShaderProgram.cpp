﻿#include <ktnEngine/Graphics/ktnShaderProgram.hpp>

#include <epoxy/gl.h>

#include <fstream>
#include <iostream>
#include <sstream>

using glm::mat2;
using glm::mat3;
using glm::mat4;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using std::cerr;
using std::endl;
using std::ifstream;
using std::string;
using std::stringstream;

namespace ktn {
namespace Graphics {
ktnShaderProgram::ktnShaderProgram() {
    ProgramID = glCreateProgram();
}

ktnShaderProgram::ktnShaderProgram(const string &VertexShaderPath, const string &FragmentShaderPath, const string &GeometryShaderPath) {
    ProgramID = glCreateProgram();
    WIP;
    // assigning a certain pass should be done in the constructor, here we manipulate it later, and set the default
    // pass to color
    Pass = ShaderPass::COLOR;
    ShaderCode = "";
    SetShader(VertexShaderPath, ShaderType::VERTEX);
    SetShader(FragmentShaderPath, ShaderType::FRAGMENT);
    if (!GeometryShaderPath.empty()) { SetShader(GeometryShaderPath, ShaderType::GEOMETRY); }

    glLinkProgram(ProgramID);
    CheckLinkErrors(ProgramID);
    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);
    if (!GeometryShaderPath.empty()) { glDeleteShader(GeometryShaderID); }
}

void ktnShaderProgram::CheckCompileErrors(GLuint ShaderID, ShaderType type) {
    GLint success;
    GLchar log[1024];
    glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &success);
    if (success == 0) {
        glGetShaderInfoLog(ShaderID, 1024, nullptr, log);
        cerr << "Error compiling " << ShaderTypeString[type].c_str() << " shader." << endl << log << endl;
    }
}

void ktnShaderProgram::CheckLinkErrors(GLuint ProgramID) {
    GLint success;
    GLchar log[1024];
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &success);
    if (success == 0) {
        glGetProgramInfoLog(ProgramID, 1024, nullptr, log);
        cerr << "Error linking shader program." << endl << log << endl;
    }
}

void ktnShaderProgram::Update() {
    // needs testing to see if we can update shaders on the fly
}

void ktnShaderProgram::SetShader(const string &Path, const ShaderType &type) {
    if (ExecutionStatus::OK == GetShaderCode(type, Path)) {
        GLuint ID = CompileShader(type);
        switch (type) {
        case ShaderType::VERTEX:
            VertexShaderID = ID;
            CheckCompileErrors(VertexShaderID, ShaderType::VERTEX);
            glAttachShader(ProgramID, VertexShaderID);
            break;
        case ShaderType::FRAGMENT:
            FragmentShaderID = ID;
            CheckCompileErrors(VertexShaderID, ShaderType::FRAGMENT);
            glAttachShader(ProgramID, FragmentShaderID);
            break;
        case ShaderType::GEOMETRY:
            GeometryShaderID = ID;
            CheckCompileErrors(VertexShaderID, ShaderType::GEOMETRY);
            glAttachShader(ProgramID, GeometryShaderID);
            break;
        }
    } else {
        cerr << "Failed to set " << ShaderTypeString[type] << " shader." << endl;
    }
}

ExecutionStatus ktnShaderProgram::GetShaderCode(const ShaderType &type, const string &Path) {
    ifstream file;
    file.open(Path.c_str());
    if (!file) {
        cerr << "Cannot open " << ShaderTypeString[type] << " shader file: " << Path << endl;
        return ExecutionStatus::FAILURE;
    }
    stringstream stream;
    stream << file.rdbuf();
    ShaderCode = stream.str();
    return ExecutionStatus::OK;
}

unsigned int ktnShaderProgram::CompileShader(const ShaderType &type) {
    const char *RawShaderCode;
    GLuint shaderID;
    switch (type) {
    case ShaderType::VERTEX:
        shaderID = glCreateShader(GL_VERTEX_SHADER);
        break;
    case ShaderType::FRAGMENT:
        shaderID = glCreateShader(GL_FRAGMENT_SHADER);
        break;
    case ShaderType::GEOMETRY:
        shaderID = glCreateShader(GL_GEOMETRY_SHADER);
        break;
    }
    RawShaderCode = ShaderCode.c_str();
    glShaderSource(shaderID, 1, &RawShaderCode, nullptr);
    glCompileShader(shaderID);
    return shaderID;
}

void ktnShaderProgram::Use() {
    glUseProgram(ProgramID);
}

void ktnShaderProgram::SetUpLight(ktnLight &light) {
    SetUniformFloat("light.intensity", light.Intensity);
    SetUniformVec3("light.position", light.Position());
    SetUniformVec3("light.direction", light.Direction());
    SetUniformFloat("light.cutoff.inner", light.Cutoff.InnerCos());
    SetUniformFloat("light.cutoff.outer", light.Cutoff.OuterCos());
    SetUniformVec3("light.color", light.Color());
    SetUniformFloat("light.falloff.linearterm", light.Falloff.LinearTerm);
    SetUniformFloat("light.falloff.quadraticterm", light.Falloff.QuadraticTerm);
}

//-----Set Uniform Bool-----//
void ktnShaderProgram::SetUniformBool(const string &name, const bool &value) {
    glUniform1i(glGetUniformLocation(ProgramID, name.c_str()), static_cast<GLint>(value));
}
void ktnShaderProgram::SetUniformBool(const string &name, const bool &&value) {
    glUniform1i(glGetUniformLocation(ProgramID, name.c_str()), static_cast<GLint>(value));
}

//-----Set Uniform Float-----//
void ktnShaderProgram::SetUniformFloat(const string &name, const float &value) {
    glUniform1f(glGetUniformLocation(ProgramID, name.c_str()), value);
}
void ktnShaderProgram::SetUniformFloat(const string &name, const float &&value) {
    glUniform1f(glGetUniformLocation(ProgramID, name.c_str()), value);
}

//-----Set Uniform Int-----//
void ktnShaderProgram::SetUniformInt(const string &name, const int &value) {
    glUniform1i(glGetUniformLocation(ProgramID, name.c_str()), value);
}
void ktnShaderProgram::SetUniformInt(const string &name, const int &&value) {
    glUniform1i(glGetUniformLocation(ProgramID, name.c_str()), value);
}

//-----Set Uniform Mat2-----//
void ktnShaderProgram::SetUniformMat2(const string &name, const mat2 &mat) {
    glUniformMatrix2fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
void ktnShaderProgram::SetUniformMat2(const string &name, const mat2 &&mat) {
    glUniformMatrix2fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

//-----Set Uniform Mat3-----//
void ktnShaderProgram::SetUniformMat3(const string &name, const mat3 &mat) {
    glUniformMatrix3fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
void ktnShaderProgram::SetUniformMat3(const string &name, const mat3 &&mat) {
    glUniformMatrix3fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

//-----Set Uniform Mat4-----//
void ktnShaderProgram::SetUniformMat4(const string &name, const mat4 &mat) {
    glUniformMatrix4fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
void ktnShaderProgram::SetUniformMat4(const string &name, const mat4 &&mat) {
    glUniformMatrix4fv(glGetUniformLocation(ProgramID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

//-----Set Uniform Vec2-----//
void ktnShaderProgram::SetUniformVec2(const string &name, const vec2 &vec) {
    glUniform2fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec2(const string &name, const vec2 &&vec) {
    glUniform2fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec2(const string &name, const float &x, const float &y) {
    glUniform2f(glGetUniformLocation(ProgramID, name.c_str()), x, y);
}
void ktnShaderProgram::SetUniformVec2(const string &name, const float &&x, const float &&y) {
    glUniform2f(glGetUniformLocation(ProgramID, name.c_str()), x, y);
}

//-----Set Uniform Vec3-----//
void ktnShaderProgram::SetUniformVec3(const string &name, const vec3 &vec) {
    glUniform3fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec3(const string &name, const vec3 &&vec) {
    glUniform3fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec3(const string &name, const float &x, const float &y, const float &z) {
    glUniform3f(glGetUniformLocation(ProgramID, name.c_str()), x, y, z);
}
void ktnShaderProgram::SetUniformVec3(const string &name, const float &&x, const float &&y, const float &&z) {
    glUniform3f(glGetUniformLocation(ProgramID, name.c_str()), x, y, z);
}

//-----Set Uniform Vec4-----//
void ktnShaderProgram::SetUniformVec4(const string &name, const vec4 &vec) {
    glUniform4fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec4(const string &name, const vec4 &&vec) {
    glUniform4fv(glGetUniformLocation(ProgramID, name.c_str()), 1, &vec[0]);
}
void ktnShaderProgram::SetUniformVec4(const string &name, const float &x, const float &y, const float &z, const float &w) {
    glUniform4f(glGetUniformLocation(ProgramID, name.c_str()), x, y, z, w);
}
void ktnShaderProgram::SetUniformVec4(const string &name, const float &&x, const float &&y, const float &&z, const float &&w) {
    glUniform4f(glGetUniformLocation(ProgramID, name.c_str()), x, y, z, w);
}
} // namespace Graphics
} // namespace ktn
