#include <ktnEngine/Graphics/ktnCamera.hpp>

#include <glm/gtc/matrix_transform.hpp>

using glm::mat4;
using glm::vec3;
using std::string;

namespace ktn {
namespace Graphics {
ktnCamera::ktnCamera(const vec3 &initialPosition, const Math::ktnEulerAngles &initialEulerAngles) : ktn3DElement("ktnCamera") {
    SetPosition(initialPosition, true);
    EulerAngles = initialEulerAngles;
}

ktnCamera::~ktnCamera() = default;

bool ktnCamera::operator==(const ktnCamera &eCamera) const {
    if (ktn3DElement::operator!=(eCamera) //
        || m_ViewMatrix != eCamera.ViewMatrix() //
        || m_Mode != eCamera.Mode() //
        || m_Type != eCamera.Type()) {
        return false;
    }
    return true;
}

bool ktnCamera::operator!=(const ktnCamera &eCamera) const {
    if (ktn3DElement::operator!=(eCamera) //
        || m_ViewMatrix != eCamera.ViewMatrix() //
        || m_Mode != eCamera.Mode() //
        || m_Type != eCamera.Type()) {
        return true;
    }
    return false;
}

ktnCamera &ktnCamera::operator=(const ktnCamera &eCamera) {
    ktn3DElement::operator=(eCamera);

    EulerAngles = eCamera.EulerAngles;
    m_Direction = eCamera.Direction();
    m_Front = eCamera.Front();
    m_Mode = eCamera.Mode();
    m_Right = eCamera.Right();
    m_Sensitivity = eCamera.Sensitivity();
    m_Target = eCamera.Target();
    m_Top = eCamera.Top();
    m_Type = eCamera.Type();
    m_UniversalUp = eCamera.UniversalUp();
    m_ViewMatrix = eCamera.ViewMatrix();
    return *this;
}

void ktnCamera::SetMode(const CameraMode &Mode) {
    m_Mode = Mode;
}

void ktnCamera::SetType(const CameraType &Type) {
    m_Type = Type;
}

void ktnCamera::ChangePosition(const vec3 &offset) {
    m_Position += offset;
}

void ktnCamera::SetTarget(const vec3 &target) {
    m_Target = target;
    if (CameraMode::TRACKING == m_Mode) {
        UpdateRightAndTop();
        CONSIDER
        /* should this be in UpdateFront for the sake of consistency
         * or here for less overhead? */
        m_Front = glm::normalize(glm::cross(m_Top, m_Right));
    }
}

void ktnCamera::SetTarget(const float &x, const float &y, const float &z) {
    m_Target.x = x;
    m_Target.y = y;
    m_Target.z = z;
    if (CameraMode::TRACKING == m_Mode) {
        UpdateRightAndTop();
        CONSIDER
        /* should this be in UpdateFront for the sake of consistency
         * or here for less overhead? */
        m_Front = glm::normalize(glm::cross(m_Top, m_Right));
    }
}

void ktnCamera::SetDirection(const vec3 &dir) {
    m_Direction = dir;
}

void ktnCamera::SetDirection(const float &x, const float &y, const float &z) {
    m_Direction.x = x;
    m_Direction.y = y;
    m_Direction.z = z;
}

void ktnCamera::SetFront(const vec3 &front) {
    m_Front = front;
}

void ktnCamera::SetFront(const float &x, const float &y, const float &z) {
    m_Front.x = x;
    m_Front.y = y;
    m_Front.z = z;
}

void ktnCamera::SetUniversalUp(const vec3 &up) {
    m_UniversalUp = up;
}

void ktnCamera::MoveForward(const float &thismuch) {
    m_Position += thismuch * m_Front;
}

void ktnCamera::MoveBackward(const float &thismuch) {
    m_Position -= thismuch * m_Front;
}

void ktnCamera::MoveLeft(const float &thismuch) {
    m_Position -= thismuch * m_Right;
}

void ktnCamera::MoveRight(const float &thismuch) {
    m_Position += thismuch * m_Right;
}

void ktnCamera::RegulatePitch() {
    if (EulerAngles.Pitch > 89.0f) { EulerAngles.Pitch = 89.0f; }
    if (EulerAngles.Pitch < -89.0f) { EulerAngles.Pitch = -89.0f; }
}

void ktnCamera::TurnUp(const float &thismuch) {
    EulerAngles.Pitch += thismuch * m_Sensitivity;
    RegulatePitch();
}

void ktnCamera::TurnDown(const float &thismuch) {
    EulerAngles.Pitch -= thismuch * m_Sensitivity;
    RegulatePitch();
}

void ktnCamera::TurnLeft(const float &thismuch) {
    EulerAngles.Yaw -= thismuch * m_Sensitivity;
}

void ktnCamera::TurnRight(const float &thismuch) {
    EulerAngles.Yaw += thismuch * m_Sensitivity;
}

void ktnCamera::UpdateFront() {
    if (CameraMode::FREELOOK == m_Mode && CameraType::FIRSTPERSON == m_Type) {
        m_Front.x = cosf(Math::DegreeToRadian(EulerAngles.Pitch)) * cosf(Math::DegreeToRadian(EulerAngles.Yaw));
        m_Front.y = -(cosf(Math::DegreeToRadian(EulerAngles.Pitch)) * sinf(Math::DegreeToRadian(EulerAngles.Yaw)));
        m_Front.z = sinf(Math::DegreeToRadian(EulerAngles.Pitch));
        m_Front = glm::normalize(m_Front);
        UpdateRightAndTop();
    }
}

void ktnCamera::UpdateRightAndTop() {
    if (CameraMode::FREELOOK == m_Mode && CameraType::FIRSTPERSON == m_Type) {
        m_Right = glm::normalize(glm::cross(m_Front, m_UniversalUp));
        m_Top = glm::normalize(glm::cross(m_Right, m_Front));
    } else if (CameraMode::TRACKING == m_Mode) {
        m_Direction = glm::normalize(m_Position - m_Target);
        m_Right = glm::normalize(glm::cross(m_UniversalUp, m_Direction));
        m_Top = glm::cross(m_Direction, m_Right);
    }
}

void ktnCamera::UpdateViewMatrix() {
    m_ViewMatrix = glm::lookAt(m_Position, m_Position + m_Front, m_Top);
}

CameraMode ktnCamera::Mode() const {
    return m_Mode;
}

CameraType ktnCamera::Type() const {
    return m_Type;
}

vec3 ktnCamera::UniversalUp() const {
    return m_UniversalUp;
}

vec3 ktnCamera::Target() const {
    return m_Target;
}

vec3 ktnCamera::Direction() const {
    return m_Direction;
}

vec3 ktnCamera::Right() const {
    return m_Right;
}

vec3 ktnCamera::Top() const {
    return m_Top;
}

vec3 ktnCamera::Front() const {
    return m_Front;
}

mat4 ktnCamera::ViewMatrix() const {
    return m_ViewMatrix;
}

float ktnCamera::Sensitivity() const {
    return m_Sensitivity;
}
} // namespace Graphics
} // namespace ktn
