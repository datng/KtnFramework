#include <ktnEngine/Graphics/ktnMaterial.hpp>

namespace ktn {
namespace Graphics {
ktnColor::ktnColor(float x, float y, float z)
    : Math::ktnVector3D( //
          std::clamp(x, 0.0f, 1.0f),
          std::clamp(y, 0.0f, 1.0f),
          std::clamp(z, 0.0f, 1.0f) //
      ) {
    //
}

void ktnColor::SetX(const float &x) {
    ktnVector3D::SetX(std::clamp(x, 0.0f, 1.0f));
}

void ktnColor::SetY(const float &y) {
    ktnVector3D::SetY(std::clamp(y, 0.0f, 1.0f));
}
void ktnColor::SetZ(const float &z) {
    ktnVector3D::SetZ(std::clamp(z, 0.0f, 1.0f));
}

} // namespace Graphics
} // namespace ktn
