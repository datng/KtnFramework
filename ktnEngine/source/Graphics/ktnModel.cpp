#include <ktnEngine/Graphics/ktnModel.hpp>

#include <assimp/Importer.hpp>

#include <iostream>

using glm::vec2;
using glm::vec3;
using ktn::Core::ktnName;
using std::cerr;
using std::endl;
using std::string;
using std::vector;

namespace ktn {
namespace Graphics {
ktnModel::ktnModel(const std::string &ePath, const ktnName &eName) : ktn3DElement(eName) {
    LoadFromDisk(ePath);
}

ktnModel::~ktnModel() = default;

ktnModel &ktnModel::operator=(const ktnModel &eModel) = default;

bool ktnModel::operator==(ktnModel eModel) {
    if (ktn3DElement::operator!=(eModel)) { return false; }
    for (size_t i = 0; i < Meshes.size(); ++i) {
        if (Meshes[i] != eModel.Meshes[i]) { return false; }
    }
    return true;
}

bool ktnModel::operator!=(const ktnModel &eModel) {
    return !operator==(eModel);
}

ExecutionStatus ktnModel::LoadFromDisk(const string &path) {
    // Unload();
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(
        path,
        (aiProcess_CalcTangentSpace | // to use in normal mapping
         aiProcess_JoinIdenticalVertices | // to reduce memory cost
         aiProcess_Triangulate | // to make triangles out of n-gons
         aiProcess_FlipUVs));
    // in case of error while loading the scene
    if (!scene) {
        cerr << "ktnModel: " << importer.GetErrorString() << endl;
        return ExecutionStatus::FAILURE;
    }
    // save the file location so we can use it to load textures later
    m_FileLocation = path.substr(0, path.find_last_of('/'));
    m_FileName = path.substr(path.find_last_of('/') + 1, path.length());
    m_FileExtension = m_FileName.substr(m_FileName.find_last_of('.') + 1, m_FileName.length());
    // process the loaded scene recursively from the root node
    ProcessNode(scene->mRootNode, scene);
    return ExecutionStatus::OK;
}

void ktnModel::LoadToGPU() {
    for (auto &mesh : Meshes) { mesh.LoadToGPU(); }
    m_IsLoadedToGPU = true;
}

void ktnModel::UnloadFromGPU() {
    if (m_IsLoadedToGPU) {
        for (auto &mesh : Meshes) { mesh.UnloadFromGPU(); }
        m_IsLoadedToGPU = false;
    }
}
ExecutionStatus ktnModel::Clear() {
    m_FileLocation = "";
    m_FileName = "";
    m_FileExtension = "";
    UnloadFromGPU();
    Meshes.clear();
    return ExecutionStatus::OK;
}

void ktnModel::ProcessNode(aiNode *n, const aiScene *s) {
    for (unsigned int meshIndex = 0; meshIndex < n->mNumMeshes; ++meshIndex) {
        aiMesh *m = s->mMeshes[n->mMeshes[meshIndex]];

        // process this mesh
        vector<ktnVertex> vertices;
        vector<unsigned int> indices;
        vector<ktnTexture> textures;

        // fill in the vertices
        for (unsigned int vertIndex = 0; vertIndex < m->mNumVertices; ++vertIndex) {
            ktnVertex v;
            // when loading from an .obj file, we have to transform the model
            // so that its "up" direction aligns with the positive z axis
            if (m_FileExtension == "obj" || m_FileExtension == "OBJ") {
                v.Position = vec3(m->mVertices[vertIndex].x, -m->mVertices[vertIndex].z, m->mVertices[vertIndex].y);
                v.Normal = vec3(m->mNormals[vertIndex].x, -m->mNormals[vertIndex].z, m->mNormals[vertIndex].y);
            }
            // loading from a .blend file requires no such transformation
            else if (m_FileExtension == "blend") {
                v.Position = vec3(m->mVertices[vertIndex].x, m->mVertices[vertIndex].y, m->mVertices[vertIndex].z);
                v.Normal = vec3(m->mNormals[vertIndex].x, m->mNormals[vertIndex].y, m->mNormals[vertIndex].z);
            }
            CONSIDER
            // use the first texture coordinates found
            if (m->mTextureCoords[0] != nullptr) {
                v.TextureCoordinates = vec2(m->mTextureCoords[0][vertIndex].x, m->mTextureCoords[0][vertIndex].y);
            } else {
                v.TextureCoordinates = vec2(0.0f, 0.0f);
            }
            vertices.push_back(v);
        }

        // fill in the indices
        for (unsigned int faceIndex = 0; faceIndex < m->mNumFaces; ++faceIndex) {
            aiFace f = m->mFaces[faceIndex];
            for (unsigned int k = 0; k < f.mNumIndices; ++k) { indices.push_back(f.mIndices[k]); }
        }

        // fill in the textures
        if (m->mMaterialIndex > 0) {
            aiMaterial *mat = s->mMaterials[m->mMaterialIndex];
            // load diffuse maps
            // vector<ktnTexture> diffuse;
            CONSIDER
            // this part could be a separate function to call multiple times
            for (unsigned int texIndex = 0; texIndex < mat->GetTextureCount(aiTextureType_DIFFUSE); ++texIndex) {
                aiString str;
                // get the texture path from the current material
                mat->GetTexture(aiTextureType_DIFFUSE, texIndex, &str);
                // assuming the path of the texture is relative, load it
                ktnTexture t(m_FileLocation + "/" + str.C_Str(), TextureType::DIFFUSE);
                // if loading relative path fails, we can assume str is the absolute path
                // and try loading it
                // this requires a rewrite of ktnTexture to give out status code somehow
                textures.push_back(t);
            }
            // or, really, it should not be a separate function, since the return type should then be ktnTexture,
            // and we don't know if it would slow down the program or not yet.
            for (unsigned int texIndex = 0; texIndex < mat->GetTextureCount(aiTextureType_SPECULAR); ++texIndex) {
                aiString str;
                // get the texture path from the current material
                mat->GetTexture(aiTextureType_SPECULAR, texIndex, &str);
                // assuming the path of the texture is relative, load it
                ktnTexture t(m_FileLocation + "/" + str.C_Str(), TextureType::SPECULAR);
                // if loading relative path fails, we can assume str is the absolute path
                // and try loading it
                // this requires a rewrite of ktnTexture to give out status code somehow
                textures.push_back(t);
            }
        }
        Meshes.emplace_back(vertices, indices, textures);
    }
    for (unsigned int childIndex = 0; childIndex < n->mNumChildren; ++childIndex) {
        // recursively process all the children nodes
        ProcessNode(n->mChildren[childIndex], s);
    }
}
} // namespace Graphics
} // namespace ktn
