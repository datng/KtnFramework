#include <ktnEngine/Graphics/ktn3DElement.hpp>

#include <glm/gtc/matrix_transform.hpp>

using glm::mat4;
using glm::vec3;
using glm::vec4;
using ktn::Core::ktnName;

namespace ktn {
namespace Graphics {
ktn3DElement::ktn3DElement(const ktnName &eName) : ktnObject(eName) {}

ktn3DElement::~ktn3DElement() = default;

bool ktn3DElement::operator==(const ktn3DElement &element) const {
    return !(
        ktnObject::operator!=(element) //
        || m_ModelMatrix != element.ModelMatrix() //
        || m_Position != element.Position() //
        || m_Rotation != element.Rotation() //
        || m_Scale != element.Scale());
}

bool ktn3DElement::operator!=(const ktn3DElement &element) const {
    return ktnObject::operator!=(element) //
           || m_ModelMatrix != element.ModelMatrix() //
           || m_Position != element.Position() //
           || m_Rotation != element.Rotation() //
           || m_Scale != element.Scale();
}

ktn3DElement &ktn3DElement::operator=(const ktn3DElement &element) {
    ktnObject::operator=(element);
    m_ModelMatrix = element.ModelMatrix();
    m_Position = element.Position();
    m_Rotation = element.Rotation();
    m_Scale = element.Scale();
    return *this;
}

mat4 ktn3DElement::ModelMatrix() const {
    return m_ModelMatrix;
}

vec3 ktn3DElement::Position() const {
    return m_Position;
}

vec3 ktn3DElement::Scale() const {
    return m_Scale;
}

vec4 ktn3DElement::Rotation() const {
    return m_Rotation;
}

//-----Set Position, Scale and Rotation using vectors-----//
void ktn3DElement::SetPosition(const vec3 &pos, const bool &updatingModelMatrix) {
    m_Position = pos;
    if (updatingModelMatrix) { UpdateModelMatrix(); }
}

void ktn3DElement::SetScale(const vec3 &scl, const bool &updatingModelMatrix) {
    m_Scale = scl;
    if (updatingModelMatrix) { UpdateModelMatrix(); }
}

void ktn3DElement::SetRotation(const vec4 &rotation, const bool &updatingModelMatrix) {
    m_Rotation = rotation;
    if (updatingModelMatrix) { UpdateModelMatrix(); }
}

void ktn3DElement::SetRotation(const vec3 &axis, const float &angle, const bool &updatingModelMatrix) {
    m_Rotation = vec4(axis, angle);
    if (updatingModelMatrix) { UpdateModelMatrix(); }
}

//-----Set Position, Scale and Rotation using tuples-----//
void ktn3DElement::SetPosition(const float &x, const float &y, const float &z, const bool &updatingModelMatrix) {
    m_Position = vec3(x, y, z);
    if (updatingModelMatrix) { UpdateModelMatrix(); }
}

void ktn3DElement::SetScale(const float &x, const float &y, const float &z, const bool &updatingModelMatrix) {
    m_Scale = vec3(x, y, z);
    if (updatingModelMatrix) { UpdateModelMatrix(); }
}

void ktn3DElement::SetRotation(const float &x, const float &y, const float &z, const float &angle, const bool &updatingModelMatrix) {
    m_Rotation = vec4(x, y, z, angle);
    if (updatingModelMatrix) { UpdateModelMatrix(); }
}

//-----Update Model Matrix-----//
void ktn3DElement::UpdateModelMatrix() {
    m_ModelMatrix = translate(IDENTITY_MATRIX_4, m_Position);
    m_ModelMatrix = rotate(m_ModelMatrix, m_Rotation.w, vec3(m_Rotation.x, m_Rotation.y, m_Rotation.z));
    m_ModelMatrix = scale(m_ModelMatrix, m_Scale);
}
} // namespace Graphics
} // namespace ktn
