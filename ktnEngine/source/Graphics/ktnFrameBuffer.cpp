#include <ktnEngine/Graphics/ktnFrameBuffer.hpp>

#include <epoxy/gl.h>

#include <iostream>

using std::cerr;
using std::cout;
using std::endl;
using std::vector;

namespace ktn {
namespace Graphics {
ktnFrameBuffer::ktnFrameBuffer(const FrameBufferType &type, const vk::Extent2D &eExtend2D, const unsigned int &nTextures) {
    if (nTextures < 1) cout << "ktnFrameBuffer: invalid number of textures";
    glGenFramebuffers(1, &m_ID);
    glBindFramebuffer(GL_FRAMEBUFFER, m_ID);
    m_nTextures = nTextures;
    if (FrameBufferType::COLOR_RGB == type) {
        // attach the color textures to this framebuffer in the GPU
        // but first create the attachment list
        vector<unsigned int> attachments(nTextures);
        for (unsigned int i = 0; i < nTextures; ++i) {
            attachments[i] = GL_COLOR_ATTACHMENT0 + i;
            ktnTexture t = ktnTexture(type, eExtend2D);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, t.ID(), 0);
            Textures.push_back(t);
        }
        glDrawBuffers(m_nTextures, attachments.data());
        /*
        the rbo in here is used to store depth and stencil values
        we do not need to read these values
        but they are needed for depth checking
        and therefore will be kept here with a local variable
        */
        GLuint rbo;
        glGenRenderbuffers(1, &rbo);
        glBindRenderbuffer(GL_RENDERBUFFER, rbo);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, eExtend2D.width, eExtend2D.height);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
    } else if (FrameBufferType::DEPTH == type) {
        // attach the texture to this framebuffer in the GPU
        ktnTexture t = ktnTexture(type, eExtend2D);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, t.ID(), 0);
        Textures.push_back(t);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
    }

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        cerr << "ktnFrameBuffer::ktnFrameBuffer: Frame buffer is not complete." << endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    m_Extend2D = eExtend2D;
}

ktnFrameBuffer::~ktnFrameBuffer() {
    for (auto &texture : Textures) { texture.Unload(); }
}

void ktnFrameBuffer::Bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_ID);
}

void ktnFrameBuffer::Unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ktnFrameBuffer::SetResolution(const uint32_t &width, const uint32_t &height) {
    m_Extend2D.width = width;
    m_Extend2D.height = height;
    WIP; // is the viewport setting necessary?
    glViewport(0, 0, width, height);
}

GLuint ktnFrameBuffer::ID() const {
    return m_ID;
}

vk::Extent2D ktnFrameBuffer::Extend2D() const {
    return m_Extend2D;
}
} // namespace Graphics
} // namespace ktn
