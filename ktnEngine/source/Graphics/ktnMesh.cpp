#include <ktnEngine/Graphics/ktnMesh.hpp>

#include <epoxy/gl.h>

#include <utility>

using std::string;
using std::to_string;
using std::vector;

namespace ktn {
namespace Graphics {
ktnMesh::ktnMesh(vector<ktnVertex> vertices, vector<unsigned int> indices, vector<ktnTexture> textures) : ktn3DElement("ktnMesh") {
    Vertices = std::move(vertices);
    Indices = std::move(indices);
    Textures = std::move(textures);
}

// Unload cannot be used here since we create ktnMeshes to push into the
// mesh vector of a ktnModel. The meshes have to be cleaned up elsewhere
// not sure where, models are also created and copied
ktnMesh::~ktnMesh() = default;

ktnMesh &ktnMesh::operator=(const ktnMesh &eMesh) {
    ktn3DElement::operator=(eMesh);
    Vertices = eMesh.Vertices;
    Indices = eMesh.Indices;
    Textures = eMesh.Textures;
#ifdef KTN_USE_OPENGL
    m_VertexArrayObject = eMesh.VertexArray();
    m_VertexBufferObject = eMesh.VertexBuffer();
    m_ElementBufferObject = eMesh.ElementBuffer();
#endif
    return *this;
}

bool ktnMesh::operator==(const ktnMesh &eMesh) {
    if (ktn3DElement::operator!=(eMesh)) { return false; }
    for (size_t i = 0; i < Vertices.size(); ++i) {
        if (Vertices[i].Normal != eMesh.Vertices[i].Normal || //
            Vertices[i].Position != eMesh.Vertices[i].Position || //
            Vertices[i].TextureCoordinates != eMesh.Vertices[i].TextureCoordinates) {
            return false;
        }
    }

    for (size_t i = 0; i < Indices.size(); ++i) {
        if (Indices[i] != eMesh.Indices[i]) { return false; }
    }

    for (size_t i = 0; i < Textures.size(); ++i) {
        if (Textures[i] != eMesh.Textures[i]) { return false; }
    }
    return true;
}

bool ktnMesh::operator!=(const ktnMesh &eMesh) {
    return !operator==(eMesh);
}

#ifdef KTN_USE_OPENGL
unsigned int ktnMesh::VertexArray() const {
    return m_VertexArrayObject;
}

unsigned int ktnMesh::VertexBuffer() const {
    return m_VertexBufferObject;
}

unsigned int ktnMesh::ElementBuffer() const {
    return m_ElementBufferObject;
}
#endif

void ktnMesh::LoadToGPU() {
#ifdef KTN_USE_OPENGL
    for (auto &texture : Textures) { texture.LoadToGPU(); }
    glGenVertexArrays(1, &m_VertexArrayObject);
    glGenBuffers(1, &m_VertexBufferObject);
    glGenBuffers(1, &m_ElementBufferObject);
    glBindVertexArray(m_VertexArrayObject);
    glBindBuffer(GL_ARRAY_BUFFER, m_VertexBufferObject);

    glBufferData(
        GL_ARRAY_BUFFER, // TODO add description comments for the parameters
        Vertices.size() * sizeof(ktnVertex),
        &Vertices[0],
        GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ElementBufferObject);
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER, // TODO add description comments for the parameters
        Indices.size() * sizeof(GLuint),
        &Indices[0],
        GL_STATIC_DRAW);

    // vertex positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(ktnVertex), nullptr);
    // vertex normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
        1, // TODO add description comments for the parameters
        3,
        GL_FLOAT,
        GL_FALSE,
        sizeof(ktnVertex),
        (void *)offsetof(ktnVertex, Normal));
    // vertex texture coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
        2, // TODO add description comments for the parameters
        2,
        GL_FLOAT,
        GL_FALSE,
        sizeof(ktnVertex),
        (void *)offsetof(ktnVertex, TextureCoordinates));
    glBindVertexArray(0);
#endif
}

void ktnMesh::UnloadFromGPU() {
#ifdef KTN_USE_OPENGL
    for (auto &texture : Textures) { texture.Unload(); }
    glDeleteBuffers(1, &m_ElementBufferObject);
    glDeleteBuffers(1, &m_VertexBufferObject);
    glDeleteVertexArrays(1, &m_VertexArrayObject);
#endif
}

void ktnMesh::Draw(ktnShaderProgram shader, unsigned int textureIndex) {
#ifdef KTN_USE_OPENGL
    GLuint diffuseIndex = 1;
    GLuint specularIndex = 1;
    string shadermaterialstring;
    if (ShaderPass::COLOR == shader.Pass) {
        for (unsigned int i = 0; i < Textures.size(); ++i) {
            glActiveTexture(GL_TEXTURE0 + i); // we have to set the active texture before binding any texture to it
            GLuint index; // currently we bind only one diffuse and one specular texture to a mesh, so this variable is
                          // not used
            TextureType type = Textures[i].Type();

            switch (type) {
            case TextureType::DIFFUSE:
                index = diffuseIndex;
                ++diffuseIndex;
                shadermaterialstring = "material.diffuse";
                break;
            case TextureType::SPECULAR:
                index = specularIndex;
                ++specularIndex;
                shadermaterialstring = "material.specular";
                break;
            default:
                break;
            }
            // shadermaterialstring = string("Material."+TextureTypeString[type]); // + to_string(index);
            shader.SetUniformInt(shadermaterialstring, i);
            glBindTexture(GL_TEXTURE_2D, Textures[i].ID());
        }
        glActiveTexture(GL_TEXTURE0 + textureIndex); // reset active texture
    }

    if (ShaderPass::SHADOW == shader.Pass) {
        // don't do any of the texture gymnastics
    }
    glBindVertexArray(m_VertexArrayObject);
    glDrawElements(
        GL_TRIANGLES, // TODO add description comments for the parameters
        Indices.size(),
        GL_UNSIGNED_INT,
        nullptr); // draw

    glBindVertexArray(0); // reset vertex array object
#endif
}
} // namespace Graphics
} // namespace ktn
