#include "ktnEngine/ktnAudio"

#include <array>
#include <iostream>
#include <thread>

using std::array;
using std::cout;
using std::endl;
using std::string;

namespace ktn {
namespace audio {
ktnAudio::ktnAudio(string path) : ktnObject() {
    m_Path = path;
    Setup();
}

ktnAudio::ktnAudio() : ktnObject() {
    Setup();
}

ktnAudio::~ktnAudio() {
    ov_clear(&m_VorbisFile);
    Pa_StopStream(m_Stream);
    Pa_CloseStream(m_Stream);
    Pa_Terminate();
}

void ktnAudio::Setup() {
    m_CurrentVolume = 1.0f;
    Pa_Initialize();
    WIP; // get device info and set up the output parameter
    m_OutputStreamParameters.channelCount = 2;
    m_OutputStreamParameters.sampleFormat = paFloat32;

    // open the stream in blocking mode
    Pa_OpenDefaultStream(
        &m_Stream, //
        0,
        m_OutputStreamParameters.channelCount,
        m_OutputStreamParameters.sampleFormat,
        44100,
        static_cast<unsigned long>(m_FramesPerBuffer),
        nullptr,
        nullptr //
    );
}

void ktnAudio::Loop() {
    // while(true)play();
}

void ktnAudio::Play(string path) {
    m_Path = path;
    Play();
}

void ktnAudio::Play() {
    // open file
    if (ov_fopen(m_Path.c_str(), &m_VorbisFile) != 0) {
        cout << "ktnAudio::play: could not open file " << m_Path << endl;
        return;
    }
    float **inputBuffer;
    // taken for granted that the output buffer has 2 channels

    array<array<float, 2>, m_FramesPerBuffer> outputBuffer;
    Pa_StartStream(m_Stream);

    int eof = 0; // indicates if we finished reading the file or not
    while (!eof) {
        long vorbisReadStatus = ov_read_float(&m_VorbisFile, &inputBuffer, m_FramesPerBuffer, &m_VorbisFile.current_link);
        if (0 == vorbisReadStatus) {
            eof = 1;
        } else if (vorbisReadStatus < 0) {
            cout << "ktnAudio::play: error reading file " << m_Path << endl;
            return;
        } else {
            // mix the audio to produce output
            for (size_t i = 0; i < static_cast<unsigned long>(vorbisReadStatus); ++i) {
                outputBuffer[i][0] = *(*inputBuffer + i) * m_CurrentVolume;
                outputBuffer[i][1] = *(*inputBuffer + i) * m_CurrentVolume;
            }
            Pa_WriteStream(m_Stream, outputBuffer.data(), static_cast<unsigned long>(vorbisReadStatus));
        }
    }
}

} // namespace audio
} // namespace ktn
