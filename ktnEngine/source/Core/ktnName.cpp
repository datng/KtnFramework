﻿#include <ktnEngine/Core/ktnName.hpp>

#include <queue>

using std::string;
using std::to_string;

namespace ktn {
namespace Core {
ktnName::ktnName(const char *eString) {
    SetName(string(eString));
}

ktnName::ktnName(const string &eString) {
    SetName(eString);
}

ktnName::ktnName(const ktnName &eName) {
    SetName(eName.ToStdString());
}

bool ktnName::operator==(const ktnName &eName) const {
    return m_String == eName.ToStdString();
}

bool ktnName::operator!=(const ktnName &eName) const {
    return m_String != eName.ToStdString();
}

ktnName &ktnName::operator=(const ktnName &eName) {
    SetName(eName.ToStdString());
    return *this;
}

getter string ktnName::ToStdString() const {
    return m_String;
}

void ktnName::AppendSuffix() {
    if (m_String.length() == 0) { m_String = string("unnamed"); }
    bool appendNumberNeeded = false;
    if (m_String.length() <= 5) {
        appendNumberNeeded = true;
    } else {
        for (unsigned int i = 4; i > 0; --i) {
            char c = m_String.at(m_String.length() - i);
            if (isdigit(c) == 0) { appendNumberNeeded = true; }
        }
    }
    if (appendNumberNeeded) { m_String = m_String + "_0000"; }
}

void ktnName::BumpSuffix() {
    AppendSuffix(); // make sure there is a suffix to bump
    // get the number after the last underscore as a string
    string nameString = m_String.substr(0, m_String.find_last_of('_'));
    string suffixString = m_String.substr(m_String.find_last_of('_') + 1, m_String.length());
    // convert that string to int
    int suffixNumber = stoi(suffixString);
    ++suffixNumber;
    // convert the number back to a string
    suffixString = to_string(suffixNumber);
    // pad '0' in until the string has 4 characters
    // this will be ignored if the number already has more than 3 digits
    while (suffixString.length() < 4) { suffixString.insert(0, "0"); }
    // return new name
    m_String = (nameString + "_" + suffixString);
}

std::string ktnName::Format(const std::string &eString) {
    std::deque<char> q;
    for (char i : eString) { // push the valid characters into a formatted queue
        if ((isdigit(i) != 0) || Utils::IsAlphabetic(i) || i == '_') { q.push_back(i); }
    }

    if (q.empty()) { return "unnamed"; }

    // remove leading non-alphabetic characters
    while (!Utils::IsAlphabetic(q[0])) {
        q.pop_front();
        if (q.empty()) { return "unnamed"; }
    }

    // remove trailing underscores
    while (q[q.size() - 1] == '_') {
        q.pop_back();
        if (q.empty()) { return "unnamed"; }
    }

    return std::string(q.begin(), q.end());
}
} // namespace Core
} // namespace ktn
