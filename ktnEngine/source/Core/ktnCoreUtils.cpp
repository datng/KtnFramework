#include <ktnEngine/Core/ktnCoreUtils.hpp>

namespace ktn {
namespace Core {
namespace Utils {
CompareFloatResult CompareFloat(const float &f1, const float &f2, const float &threshold) {
    if (std::fabs(f1 - f2) < threshold) return CompareFloatResult::Equal;
    if (f1 > f2) return CompareFloatResult::Greater;
    if (f1 < f2) return CompareFloatResult::Smaller;
    return CompareFloatResult::Equal;
}

bool IsAlphabetic(const char &c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}
} // namespace Utils
} // namespace Core
} // namespace ktn
