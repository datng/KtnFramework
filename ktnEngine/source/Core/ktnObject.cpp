#include <utility>

#include <ktnEngine/Core/ktnObject.hpp>

using namespace std;
namespace ktn {
namespace Core {
ktnObject::ktnObject(ktnName eName) : Name(std::move(eName)) {}

bool ktnObject::operator==(const ktnObject &eObj) const {
    return Name == eObj.Name;
}

bool ktnObject::operator!=(const ktnObject &eObj) const {
    return Name != eObj.Name;
}

ktnObject &ktnObject::operator=(const ktnObject &eObj) = default;
} // namespace Core
} // namespace ktn
