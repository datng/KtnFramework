#include <ktnEngine/Math/ktnQuaternion.hpp>

#include <iostream>

namespace ktn::Math {
///////////////////////////
// ktnQuaternion::Component
ktnQuaternion::Component::Component(const ComponentBasis &basis, const float &scale) : Basis(basis) {
    Scale = scale;
}

///////////////////////////////
// ktnQuaternion::ComponentReal
ktnQuaternion::ComponentReal::ComponentReal(const float &scale) noexcept : Component(ComponentBasis::Real, scale) {}

ktnQuaternion::ComponentReal::ComponentReal(const ComponentReal &c) : Component(ComponentBasis::Real, c.Scale) {}

ktnQuaternion::ComponentReal::~ComponentReal() = default;

ktnQuaternion::ComponentReal &ktnQuaternion::ComponentReal::operator=(const ComponentReal &c) {
    Scale = c.Scale;
    return *this;
}

ktnQuaternion::ComponentReal ktnQuaternion::ComponentReal::operator+(const ComponentReal &c) const {
    return ComponentReal(Scale + c.Scale);
}

ktnQuaternion::ComponentReal ktnQuaternion::ComponentReal::operator+(const float &addend) const {
    return ComponentReal(Scale + addend);
}

ktnQuaternion::ComponentReal ktnQuaternion::ComponentReal::operator*(const float &scale) const {
    return ComponentReal(Scale * scale);
}

////////////////////////////
// ktnQuaternion::ComponentI
ktnQuaternion::ComponentI::ComponentI(const float &scale) noexcept : Component(ComponentBasis::I, scale) {}

ktnQuaternion::ComponentI::ComponentI(const ComponentI &c) : Component(ComponentBasis::I, c.Scale) {}

ktnQuaternion::ComponentI::~ComponentI() = default;

ktnQuaternion::ComponentI &ktnQuaternion::ComponentI::operator=(const ComponentI &c) {
    Scale = c.Scale;
    return *this;
}

ktnQuaternion::ComponentI ktnQuaternion::ComponentI::operator+(const ComponentI &c) const {
    return ComponentI(Scale + c.Scale);
}

ktnQuaternion::ComponentI ktnQuaternion::ComponentI::operator*(const float &scale) const {
    return ComponentI(Scale * scale);
}

////////////////////////////
// ktnQuaternion::ComponentJ
ktnQuaternion::ComponentJ::ComponentJ(const float &scale) noexcept : Component(ComponentBasis::J, scale) {}

ktnQuaternion::ComponentJ::ComponentJ(const ComponentJ &c) : Component(ComponentBasis::J, c.Scale) {}

ktnQuaternion::ComponentJ::~ComponentJ() = default;

ktnQuaternion::ComponentJ &ktnQuaternion::ComponentJ::operator=(const ComponentJ &c) {
    Scale = c.Scale;
    return *this;
}

ktnQuaternion::ComponentJ ktnQuaternion::ComponentJ::operator+(const ComponentJ &c) const {
    return ComponentJ(Scale + c.Scale);
}

ktnQuaternion::ComponentJ ktnQuaternion::ComponentJ::operator*(const float &scale) const {
    return ComponentJ(Scale * scale);
}

////////////////////////////
// ktnQuaternion::ComponentK
ktnQuaternion::ComponentK::ComponentK(const float &scale) noexcept : Component(ComponentBasis::K, scale) {}

ktnQuaternion::ComponentK::ComponentK(const ComponentK &c) : Component(ComponentBasis::K, c.Scale) {}

ktnQuaternion::ComponentK::~ComponentK() = default;

ktnQuaternion::ComponentK &ktnQuaternion::ComponentK::operator=(const ComponentK &c) {
    Scale = c.Scale;
    return *this;
}

ktnQuaternion::ComponentK ktnQuaternion::ComponentK::operator+(const ComponentK &c) const {
    return ComponentK(Scale + c.Scale);
}

ktnQuaternion::ComponentK ktnQuaternion::ComponentK::operator*(const float &scale) const {
    return ComponentK(Scale * scale);
}

////////////////
// ktnQuaternion
ktnQuaternion::ktnQuaternion() = default;

ktnQuaternion::ktnQuaternion(const ktnQuaternion &q) {
    W = q.W;
    X = q.X;
    Y = q.Y;
    Z = q.Z;
}

ktnQuaternion::ktnQuaternion(const ComponentReal &real, const ComponentI &i, const ComponentJ &j, const ComponentK &k) {
    Set(real, i, j, k);
}

ktnQuaternion::ktnQuaternion(float real, float i, float j, float k) {
    Set(real, i, j, k);
}

ktnQuaternion::~ktnQuaternion() = default;

ktnQuaternion &ktnQuaternion::operator=(const ktnQuaternion &eQuaternion) {
    Set(eQuaternion.W, eQuaternion.X, eQuaternion.Y, eQuaternion.Z);
    return *this;
}

ktnQuaternion ktnQuaternion::operator+(const ktnQuaternion &eQuaternion) {
    return ktnQuaternion(W + eQuaternion.W, X + eQuaternion.X, Y + eQuaternion.Y, Z + eQuaternion.Z);
}

ktnQuaternion ktnQuaternion::operator*(const ComponentI &c) const {
    // (w + xi + yj + zk) * i = wi + xii + yji + zki = -x + wi + zj - yk
    return ktnQuaternion(-X.Scale, W.Scale, Z.Scale, -Y.Scale) * c.Scale;
}

ktnQuaternion ktnQuaternion::operator*(const ComponentJ &c) const {
    // (w + xi + yj + zk) * j = wj + xij + yjj + zkj = -y - zi + wj + xk
    return ktnQuaternion(-Y.Scale, -Z.Scale, W.Scale, X.Scale) * c.Scale;
}

ktnQuaternion ktnQuaternion::operator*(const ComponentK &c) const {
    // (w + xi + yj + zk) * k = wk + xik + yjk + zkk = -z + yi - xj + wk
    return ktnQuaternion(-Z.Scale, Y.Scale, -X.Scale, W.Scale) * c.Scale;
}

ktnQuaternion ktnQuaternion::operator*(const ComponentReal &c) const {
    return operator*(c.Scale);
}

ktnQuaternion ktnQuaternion::operator*(const float &scale) const {
    return ktnQuaternion(W * scale, X * scale, Y * scale, Z * scale);
}

setter void ktnQuaternion::Set(const ComponentReal &real, const ComponentI &i, const ComponentJ &j, const ComponentK &k) {
    W = real;
    X = i;
    Y = j;
    Z = k;
}

setter void ktnQuaternion::Set(const float &real, const float &i, const float &j, const float &k) {
    W = ComponentReal(real);
    X = ComponentI(i);
    Y = ComponentJ(j);
    Z = ComponentK(k);
}

bool ktnQuaternion::Equals(const ktnQuaternion &q, const float &tolerance) {
    if (std::abs(W.Scale - q.W.Scale) > tolerance) { return false; }
    if (std::abs(X.Scale - q.X.Scale) > tolerance) { return false; }
    if (std::abs(Y.Scale - q.Y.Scale) > tolerance) { return false; }
    if (std::abs(Z.Scale - q.Z.Scale) > tolerance) { return false; }
    return true;
}

ktnQuaternion ktnQuaternion::Conjugated() const {
    return ktnQuaternion(W.Scale, -X.Scale, -Y.Scale, -Z.Scale);
}
float ktnQuaternion::Norm() const {
    float real = W.Scale, i = X.Scale, j = Y.Scale, k = Z.Scale;
    return sqrtf((real * real) + (i * i) + (j * j) + (k * k));
}

////////////
// utilities
ktnQuaternion operator*(float scale, ktnQuaternion q) {
    return ktnQuaternion(q.W * scale, q.X * scale, q.Y * scale, q.Z * scale);
}

ktnQuaternion operator*(const ktnQuaternion::ComponentReal &basis, const ktnQuaternion &q) {
    return q * basis;
}

ktnQuaternion operator*(const ktnQuaternion::ComponentI &basis, const ktnQuaternion &q) {
    auto temp = q * basis;
    temp.Y = temp.Y * -1;
    temp.Z = temp.Z * -1;
    return temp;
}

ktnQuaternion operator*(const ktnQuaternion::ComponentJ &basis, const ktnQuaternion &q) {
    auto temp = q * basis;
    temp.X = temp.X * -1;
    temp.Z = temp.Z * -1;
    return temp;
}

ktnQuaternion operator*(const ktnQuaternion::ComponentK &basis, const ktnQuaternion &q) {
    auto temp = q * basis;
    temp.X = temp.X * -1;
    temp.Y = temp.Y * -1;
    return temp;
}

QuaternionNormalizationResult Normalize(ktnQuaternion q) {
    float norm = q.Norm();
    if (norm == 0.0f) {
        std::cerr << "ktnQuaternion.cpp: Normalize: The given quaternion has size 0 and therefore cannot be normalized. " << std::endl;
        return {ktn::ExecutionStatus::FAILURE, ktnQuaternion(0, 0, 0, 0)};
    }
    return {ktn::ExecutionStatus::OK, ktnQuaternion(q.W, q.X, q.Y, q.Z) * (1 / norm)};
}
} // namespace ktn::Math
