#ifndef KTNGAME_HPP
#define KTNGAME_HPP
#include "ktnRenderer.hpp"

#include <GLFW/glfw3.h>

namespace ktn {
class ktnGame : public SignalSlot::ktnSignalSlot {
public:
    ktnGame(const Graphics::Backend &backend = Graphics::Backend::OPENGL, const std::string &windowName = "Window Name");
    virtual ~ktnGame();

    getter ktnScene *ActiveScene();

    setter void SetActiveScene(ktnScene *scene);

    void Run();

    vk::Extent2D Extent2D;
    Graphics::Backend GraphicsBackend;
    ktnRenderer *Renderer = nullptr;
    unsigned int ShadowMapSize;
    GLFWwindow *Window = nullptr;

protected:
    int Initialize(const std::string &windowName);

    ktnScene *m_ActiveScene = nullptr;
}; // class ktnGame
} // namespace ktn
#endif // KTNGAME_HPP
