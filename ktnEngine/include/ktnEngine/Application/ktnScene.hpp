#ifndef KTNSCENE_HPP
#define KTNSCENE_HPP
#include "ktnEngine/ktnAudio"
#include "ktnEngine/ktnGraphics"
#include "ktnInputMapper.hpp"

namespace ktn {
class ktnScene : public SignalSlot::ktnSignalSlot {
public:
    ktnScene();
    virtual ~ktnScene();

    getter Graphics::ktnCamera *CurrentCamera();

    setter void SetCurrentCamera(const std::string &name);
    setter void SetWindow(GLFWwindow *window);

    void AddCamera(Graphics::ktnCamera eCamera);
    void AddLight(Graphics::ktnLight &eLight);
    void AddModel(Graphics::ktnModel &eModel);
    virtual void ProcessInput();
    void RemoveCamera(const std::string &name);
    void RemoveLight(const std::string &name);
    void RemoveModel(const std::string &name);

    std::vector<Graphics::ktnFrameBuffer *> ShadowBuffer_Directional;
    std::vector<Graphics::ktnFrameBuffer *> ShadowBuffer_Omnidirectional;

    std::vector<Graphics::ktnCamera> Cameras;
    std::vector<Graphics::ktnLight> Lights;
    std::vector<Graphics::ktnModel> Models;

protected:
    virtual void callback_FrameBufferSizeChange(GLFWwindow *window, int width, int height) = 0;
    virtual void callback_MouseClick(GLFWwindow *window, int button, int action, int mods) = 0;
    virtual void callback_MouseMove(GLFWwindow *window, double xpos, double ypos) = 0;

    Graphics::ktnCamera *m_CurrentCamera = nullptr;
    float m_DeltaTime = 0.0;
    bool m_FirstMouse = true;
    ktnInputMapper m_InputMapper;
    float m_LastFrameTime = 0.0;
    double m_LastX = 0.0;
    double m_LastY = 0.0;
    bool m_LeftMouseIsClicking = false;
    GLFWwindow *m_Window = nullptr;
};
} // namespace ktn
#endif // KTNSCENE_HPP
