#ifndef KTNRENDERER_OPENGL_HPP
#define KTNRENDERER_OPENGL_HPP
#include "ktnRenderer.hpp"

namespace ktn {
class ktnRenderer_OpenGL : public ktnRenderer {
public:
    ktnRenderer_OpenGL();
    ~ktnRenderer_OpenGL();

    ExecutionStatus Initialize(GLFWwindow *window, const vk::Extent2D &eExtend2D, unsigned int shadowMapSize) override;
    void Loop(ktnScene *scene) override;
    void CleanUp() override;
    void WaitIdle() override;

    void RenderShadow(ktnScene *scene) override;
    void RenderColor(ktnScene *scene) override;
    void RenderBlurs() override;
    void RenderBloom() override;
    void RenderCombined() override;

    void SetShadowShader(Graphics::ktnShaderProgram *shader) override;
    void SetColorShader(Graphics::ktnShaderProgram *shader) override;
    void SetBlurShader(Graphics::ktnShaderProgram *shader) override;
    void SetFullscreenRectangleShader(Graphics::ktnShaderProgram *shader) override;
    void SetBloomShader(Graphics::ktnShaderProgram *shader) override;

    Graphics::ktnFrameBuffer *m_FrameBuffer = nullptr;
    Graphics::ktnFrameBuffer *m_FrameBufferMedium = nullptr;
    Graphics::ktnFrameBuffer *m_FrameBufferSmall = nullptr;
    Graphics::ktnFrameBuffer *m_BlurBuffersMedium = nullptr;
    Graphics::ktnFrameBuffer *m_BlurBuffersSmall = nullptr;
    Graphics::ktnFrameBuffer *m_BloomBuffer = nullptr;
    unsigned int m_FullscreenRectangleVertexArray = 0;
    unsigned int m_FullscreenRectangleVertexBuffer = 0;

private:
    Graphics::ktnShaderProgram *m_ShadowShader = nullptr;
    Graphics::ktnShaderProgram *m_ColorShader = nullptr;
    Graphics::ktnShaderProgram *m_BlurShader = nullptr;
    Graphics::ktnShaderProgram *m_FullscreenRectangleShader = nullptr;
    Graphics::ktnShaderProgram *m_BloomShader = nullptr;

    unsigned int m_ShadowMapSize = 0;

    unsigned int m_DownscaleFactor_Medium = 0;
    unsigned int m_DownscaleFactor_Small = 0;
    unsigned int m_DownscaleFactor_Tiny = 0;
};
} // namespace ktn
#endif // KTNRENDERER_OPENGL_HPP
