#ifndef KTNRENDERER_HPP
#define KTNRENDERER_HPP
#include <ktnEngine/ktnGraphics>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h> //for the window handle used to create vulkan surface

#include "ktnScene.hpp"

namespace ktn {
class ktnRenderer {
public:
    virtual ~ktnRenderer() = default;

    virtual ExecutionStatus Initialize(GLFWwindow *window, const vk::Extent2D &eExtend2D, unsigned int shadowMapSize) = 0;
    virtual void Loop(ktnScene *scene) = 0;
    virtual void WaitIdle() = 0;
    virtual void CleanUp() = 0;

    virtual void RenderShadow(ktnScene *scene) = 0;
    virtual void RenderColor(ktnScene *scene) = 0;
    virtual void RenderBlurs() = 0;
    virtual void RenderBloom() = 0;
    virtual void RenderCombined() = 0;

    virtual void SetShadowShader(Graphics::ktnShaderProgram *shader) = 0;
    virtual void SetColorShader(Graphics::ktnShaderProgram *shader) = 0;
    virtual void SetBlurShader(Graphics::ktnShaderProgram *shader) = 0;
    virtual void SetFullscreenRectangleShader(Graphics::ktnShaderProgram *shader) = 0;
    virtual void SetBloomShader(Graphics::ktnShaderProgram *shader) = 0;

protected:
    GLFWwindow *m_Window = nullptr;
    vk::Extent2D m_Extend2D;
}; // class ktnRenderer
} // namespace ktn
#endif // KTNRENDERER_HPP
