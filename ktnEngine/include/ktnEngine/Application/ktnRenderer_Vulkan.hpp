#ifndef KTNRENDERER_VULKAN_HPP
#define KTNRENDERER_VULKAN_HPP
#include "ktnRenderer.hpp"

#include <optional>

namespace ktn {
////////////////////
// class QueueFamily
class QueueFamily {
public:
    QueueFamily() {}
    ~QueueFamily() {}

    void SetIndex(uint32_t index) {
        m_Index = index;
    }
    void SetCount(uint32_t count) {
        m_Count = count;
    }
    std::optional<uint32_t> Index() {
        return m_Index;
    }
    std::optional<uint32_t> Count() {
        return m_Count;
    }

private:
    std::optional<uint32_t> m_Index;
    std::optional<uint32_t> m_Count;
};

//////////////////////
// class QueueFamilies
class QueueFamilies {
public:
    QueueFamilies() {}
    ~QueueFamilies() {}

    bool AreComplete() {
        return (Graphics.Count().has_value() && Graphics.Index().has_value() && Present.Count().has_value() && Present.Index().has_value());
    }

    QueueFamily Graphics;
    QueueFamily Present;
};

///////////////////////////
// class ktnRenderer_Vulkan
class ktnRenderer_Vulkan : public ktnRenderer {
public:
    ktnRenderer_Vulkan();
    ~ktnRenderer_Vulkan();

    virtual ExecutionStatus Initialize(GLFWwindow *window, const vk::Extent2D &eExtend2D, unsigned int shadowMapSize);
    virtual void Loop(ktnScene *scene);
    virtual void WaitIdle();
    virtual void CleanUp();

    virtual void RenderShadow(ktnScene *scene);
    virtual void RenderColor(ktnScene *scene);
    virtual void RenderBlurs();
    virtual void RenderBloom();
    virtual void RenderCombined();

    virtual void SetShadowShader(Graphics::ktnShaderProgram *shader);
    virtual void SetColorShader(Graphics::ktnShaderProgram *shader);
    virtual void SetBlurShader(Graphics::ktnShaderProgram *shader);
    virtual void SetFullscreenRectangleShader(Graphics::ktnShaderProgram *shader);
    virtual void SetBloomShader(Graphics::ktnShaderProgram *shader);

    // Vulkan specific methods:
    void CreateInstance();
    void SetUpDebugCallback();
    void CreateSurface();
    void PickPhysicalDevice();
    ExecutionStatus CreateLogicalDevice();
    ExecutionStatus CreateSwapchain();
    ExecutionStatus CreateImageViews();
    ExecutionStatus CreateRenderPass();
    ExecutionStatus CreatePipeline();
    ExecutionStatus CreateFrameBuffers();
    ExecutionStatus CreateCommandPool();
    ExecutionStatus CreateCommandBuffers();
    ExecutionStatus CreateSyncObjects();
    void Draw();

    // helper methods
    ExecutionStatus CheckValidationLayer();
    ExecutionStatus CheckPhysicalDevice(vk::PhysicalDevice device);
    ExecutionStatus CheckPhysicalDeviceExtensionsSupport(vk::PhysicalDevice device);
    ExecutionStatus CheckPhysicalDeviceSwapchainSupport(vk::PhysicalDevice device);
    ExecutionStatus CheckQueueFamilies(vk::PhysicalDevice device);

    vk::SurfaceFormatKHR ChooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR> &supportedFormats);
    vk::PresentModeKHR ChooseSwapchainPresentMode(const std::vector<vk::PresentModeKHR> &supportedModes);
    vk::Extent2D ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR &supportedCapabilities);
    vk::ShaderModule CreateShaderModule(const std::vector<char> &buffer);

    std::vector<const char *> GetRequiredExtentions();
    std::vector<char> GetShaderFromFile(const std::string &filepath);

protected:
    vk::Instance m_Instance;
    vk::PhysicalDevice m_PhysicalDevice;
    vk::Device m_LogicalDevice;
    vk::Queue m_GraphicsQueue;
    vk::Queue m_PresentQueue;
    QueueFamilies m_QueueFamilies;
    VkSurfaceKHR m_Surface;
    vk::SwapchainKHR m_Swapchain;
    std::vector<vk::Image> m_SwapchainImages;
    std::vector<vk::ImageView> m_SwapchainImageViews;
    std::vector<vk::PresentModeKHR> m_SwapchainPresentModes;
    std::vector<vk::Framebuffer> m_SwapchainFrameBuffers;
    vk::SurfaceCapabilitiesKHR m_SurfaceCapabilities;
    std::vector<vk::SurfaceFormatKHR> m_SurfaceFormats;

    vk::Format m_SwapchainImageFormat;
    vk::Extent2D m_SwapchainExtent;

    vk::RenderPass m_RenderPass;

    vk::PipelineCache m_PipelineCache;

    vk::PipelineLayout m_PipelineLayout;
    vk::Pipeline m_Pipeline;

    vk::CommandPool m_CommandPool;
    std::vector<vk::CommandBuffer> m_CommandBuffers;

    std::vector<vk::Semaphore> m_Semaphores_ImgAvailable;
    std::vector<vk::Semaphore> m_Semaphores_RenderDone;
    unsigned int m_CurrentFrame = 0;
    std::vector<vk::Fence> m_Fences;

#ifdef KTN_DEBUG
    VkDebugUtilsMessengerEXT m_DebugUtilsMessenger;
#endif
};
} // namespace ktn

#endif // KTNRENDERER_VULKAN_HPP
