﻿/*****************************
Copyright 2019 Tien Dat Nguyen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*****************************/

#ifndef KTNENGINE_CORE_KTNSIGNALSLOT_HPP
#define KTNENGINE_CORE_KTNSIGNALSLOT_HPP
#include "ktnSignalSlotManager.hpp"

#include <algorithm>
#include <vector>

namespace ktn::SignalSlot {
/**
 * @brief The ktnSignalSlot class is the base class for all classes that need to use the signal/slot mechanism.
 */
class ktnSignalSlot {
public:
    /**
     * @brief ktnSignalSlot is the default constructor.
     * It register the object as a potential signal receiver.
     */
    explicit ktnSignalSlot(Manager *eManager = &StaticManager) {
        m_Manager = eManager;
        auto receiverAddress = reinterpret_cast<size_t>(this);
        m_Manager->RegisterReceiver(receiverAddress);
    }

    /**
     * @brief ~ktnSignalSlot is the default destructor.
     * It deregister the object from the static manager's receiver list.
     */
    ~ktnSignalSlot() {
        auto receiverAddress = reinterpret_cast<size_t>(this);
        m_Manager->DeregisterReceiver(receiverAddress);
        m_Manager = nullptr;
    }

    /**
     * @brief The ktnSignal class is the template class for all signals.
     */
    template <typename... Ts> class ktnSignal {
    public:
        /**
         * @brief ktnSignal is the default constructor for a signal.
         * It register the signal to the static manager's signal list.
         */
        explicit ktnSignal(Manager *eManager = &StaticManager) {
            m_Manager = eManager;
            auto signalAddress = reinterpret_cast<size_t>(this);
            m_Manager->RegisterSignal(signalAddress);
        }

        /**
         * @brief ~ktnSignal is the default destructor for a signal.
         * It deregister the signal from the static manager's signal list.
         */
        ~ktnSignal() {
            auto signalAddress = reinterpret_cast<size_t>(this);
            m_Manager->DeregisterSignal(signalAddress);
        }

        /**
         * @brief operator () is used to emit the signal.
         * @param eArgs is the argument pack to be used by the slots.
         */
        void operator()(Ts... eArgs) {
            if (m_Manager->UncleanSignals.find(reinterpret_cast<size_t>(this)) != m_Manager->UncleanSignals.end()) { //
                CleanUp();
            }
            for (auto &slot : m_Slots) { slot->Execute(eArgs...); }
        }

        /**
         * @brief CleanUp removes slots that belong to deregistered (deleted) receivers.
         */
        void CleanUp() {
            for (auto &slot : m_Slots) {
                auto address = slot->InstanceAddress();
                if (m_Manager->Receivers.find(address) == m_Manager->Receivers.end() && address != 0) {
                    m_Slots.erase(std::find(m_Slots.begin(), m_Slots.end(), slot));
                }
            }
            m_Manager->UncleanSignals.erase(m_Manager->UncleanSignals.find(reinterpret_cast<size_t>(this)));
        }

        /**
         * @brief Connect connects the signal to a member function of class C's instance eInstance.
         * @param eInstance is the instance of class C which is connected to this signal.
         * @param eFunction is the function in class C that is used as the slot.
         */
        template <class C, typename N> void Connect(C *eInstance, N (C::*eFunction)(Ts...)) {
            if (eInstance == nullptr) return; // invalid pointer
            auto *slot = new ktnSlot_ClassMember<C, N>(eInstance, eFunction);
            for (size_t i = 0; i < m_Slots.size(); ++i) {
                if (*slot == m_Slots[i]) return; // slot already exists
            }
            m_Slots.push_back(dynamic_cast<ktnSlot_Interface *>(slot));
        }

        /**
         * @brief Connect connects the slot to a free function.
         * @param eFunction is the free function that is used as the slot.
         */
        template <typename N> void Connect(N (*eFunction)(Ts...)) {
            auto *slot = new ktnSlot_FreeFunction<N>(eFunction);
            for (size_t i = 0; i < m_Slots.size(); ++i) {
                if (*slot == m_Slots[i]) return; // slot already exists
            }
            m_Slots.push_back(dynamic_cast<ktnSlot_Interface *>(slot));
        }

        /**
         * @brief Disconnect disconnects the signal from all slots of class C's instance eInstance.
         * @param eInstance
         */
        template <class C> void Disconnect(C *eInstance) {
            auto instanceAddress = reinterpret_cast<size_t>(eInstance);
            if (instanceAddress == 0) return;
            if (m_Slots.size() == 0) return;
            size_t i = 0;
            while (i < m_Slots.size()) {
                if (m_Slots[i]->InstanceAddress() == instanceAddress) {
                    m_Slots.erase(m_Slots.begin() + i);
                } else {
                    ++i;
                }
            }
        }

        /**
         * @brief Disconnect disconnects the signal from a member function of class C's instance eInstance.
         * @param eInstance is the instance of class C which is connected to this signal.
         * @param eFunction is the function in class C that will be disconnected.
         */
        template <class C, typename N> void Disconnect(C *eInstance, N (C::*eFunction)(Ts...)) {
            ktnSlot_ClassMember<C, N> slot(eInstance, eFunction);
            for (size_t i = 0; i < m_Slots.size(); ++i) {
                if (slot == m_Slots[i]) {
                    delete m_Slots[i];
                    m_Slots.erase(m_Slots.begin() + i);
                    break;
                }
            }
        }

        /**
         * @brief Disconnect disconnects the slot from a free function.
         * @param eFunction is the free function that will be disconnected.
         */
        template <typename N> void Disconnect(N (*eFunction)(Ts...)) {
            ktnSlot_FreeFunction<N> slot(eFunction);
            for (size_t i = 0; i < m_Slots.size(); ++i) {
                if (slot == m_Slots[i]) {
                    delete m_Slots[i];
                    m_Slots.erase(m_Slots.begin() + i);
                    break;
                }
            }
        }

    private:
        /**
         * @brief The ktnSlot_Interface class describe the common properties of different slot types.
         * Since it is an interface class, all of its member functions are pure virtual.
         */
        /////////////////////////
        class ktnSlot_Interface {
        public:
            virtual bool operator==(ktnSlot_Interface *eSlot) = 0;

            virtual void Execute(Ts...) = 0;
            virtual size_t InstanceAddress() = 0;
        }; // class ktnSlot_Interface

        /**
         * @brief The ktnSlot_ClassMember class describes the slot corresponding to an object's member function.
         */
        /////////////////////////////////////////////////////////////////////////////////////
        template <class C, typename N> class ktnSlot_ClassMember : public ktnSlot_Interface {
        public:
            ktnSlot_ClassMember(C *eInstance, N (C::*eFunction)(Ts...)) {
                m_Instance = eInstance;
                m_Function = eFunction;
            }

            bool operator==(ktnSlot_Interface *eSlot) override {
                auto eSlotCast = dynamic_cast<ktnSlot_ClassMember *>(eSlot);
                if (eSlotCast == nullptr) {
                    return false; // type mismatch
                }
                return m_Instance == eSlotCast->m_Instance && m_Function == eSlotCast->m_Function;
            }

            /**
             * @brief Execute executes the function.
             * @param eArguments is the argument pack given when the signal emits.
             */
            void Execute(Ts... eArguments) override {
                (m_Instance->*m_Function)(eArguments...);
            }

            /**
             * @brief InstanceAddress
             * @return the address of m_Instance, cast as size_t for straightforward comparison
             */
            size_t InstanceAddress() override {
                return reinterpret_cast<size_t>(m_Instance);
            }

        private:
            C *m_Instance;
            N (C::*m_Function)(Ts...);
        }; // class ktnSlot_ClassMember

        /**
         * @brief The ktnSlot_FreeFunction class describes the slot corresponding to a free function.
         */
        /////////////////////////////////////////////////////////////////////////////
        template <typename N> class ktnSlot_FreeFunction : public ktnSlot_Interface {
        public:
            explicit ktnSlot_FreeFunction(N (*eFunction)(Ts...)) {
                m_Function = eFunction;
            }

            virtual bool operator==(ktnSlot_Interface *eSlot) {
                auto slot = dynamic_cast<ktnSlot_FreeFunction *>(eSlot);
                if (slot == nullptr) {
                    return false; // type mismatch
                }
                return m_Function == slot->m_Function;
            }

            /**
             * @brief Execute executes the function.
             * @param eArguments is the argument pack given when the signal emits.
             */
            void Execute(Ts... eArguments) override {
                m_Function(eArguments...);
            }

            /**
             * @brief InstanceAddress
             * @return 0 since there is no instance attached.
             */
            size_t InstanceAddress() override {
                return 0;
            }

            N (*m_Function)(Ts...);
        }; // class ktnSlot_FreeFunction

        /**
         * @brief m_Manager is a workaround to the problem of the static manager deregistering signals with no parameters without being asked to.
         * This happens strictly during unit testing.
         */
        Manager *m_Manager = nullptr;
        /**
         * @brief m_Slots is the container of connected slots. In general, a vector works fine.
         * Change this to deque or list based on the project's need.
         */
        std::vector<ktnSlot_Interface *> m_Slots;
    }; // class ktnSignal

private:
    /**
     * @brief m_Manager is a workaround to the problem of the static manager deregistering signals with no parameters without being asked to.
     * This happens strictly during unit testing.
     */
    Manager *m_Manager = nullptr;
}; // class ktnSignalSlot
} // namespace ktn::SignalSlot
#endif // KTNENGINE_CORE_KTNSIGNALSLOT_HPP
