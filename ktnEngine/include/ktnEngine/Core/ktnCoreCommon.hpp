#ifndef KTNENGINE_CORE_KTNCORECOMMON_HPP
#define KTNENGINE_CORE_KTNCORECOMMON_HPP

#include <glm/glm.hpp>
// since glm v0.9.9.0 deprecated the initialization of the identity matrix using glm::mat4(), a constant for identity matrix has to be defined
const glm::mat4 IDENTITY_MATRIX_4 = glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

// defines to cause compiler warnings for incomplete features and questionable segments of code
#define WIP \
    { bool Unfinished = true; }
#define CONSIDER \
    { bool ThinkAboutThis = true; }
#define BACKEND_DEPENDENT \
    { bool shouldSplitIntoInheritedClass = true; }

// defines for readability in header files
#define getter [[nodiscard]]
#define helper
#define setter

namespace ktn {
enum class ExecutionStatus { OK, FAILURE };

/**
 * @brief ComparisonThreshold_Float is the comparison threshold for floats.
 * Any number whose absolute value lies below this threshold will be treated as zero.
 */
static const float ComparisonThreshold_Float = 1e-18F;

enum class AngleUnit { degree, radian };
} // namespace ktn
#endif // KTNENGINE_CORE_KTNCORECOMMON_HPP
