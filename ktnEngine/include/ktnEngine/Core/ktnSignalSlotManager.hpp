﻿/*****************************
Copyright 2019 Tien Dat Nguyen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*****************************/

#ifndef KTNENGINE_CORE_KTNSIGNALSLOTMANAGER_HPP
#define KTNENGINE_CORE_KTNSIGNALSLOTMANAGER_HPP
#include <set>

namespace ktn::SignalSlot {
/**
 * @brief The ktnSignalSlotManager class describes the structure that keeps track of all signals and slots in the program.
 * A static variable of this type is used by all ktnSignalSlot objects to register/deregister signals/receivers.
 */
class Manager {
public:
    /**
     * @brief DeregisterSignal removes the signal object at eAddress from the signal list.
     * @param eAddress is the address of the signal to be removed.
     */
    void DeregisterSignal(const size_t &eAddress) {
        const auto iterator = Signals.find(eAddress);
        if (iterator != Signals.end()) {
            Signals.erase(iterator);
        }
    }

    /**
     * @brief DeregisterReceiver removes the receiver object at eAddress from the receiver list.
     * @param eAddress is the address of the receiver to be removed.
     */
    void DeregisterReceiver(const size_t &eAddress) {
        const auto iterator = Receivers.find(eAddress);
        if (iterator != Receivers.end()) {
            Receivers.extract(iterator);
            std::set<size_t> cleanuplist;
            for (auto &signal : Signals) {
                cleanuplist.insert(signal);
            }
            UncleanSignals = cleanuplist;
        }
    }

    /**
     * @brief RegisterSignal adds the signal object at eAddress to the signal list.
     * @param eAddress is the address of the signal to be added.
     */
    void RegisterSignal(const size_t &eAddress) {
        Signals.insert(eAddress);
    }

    /**
     * @brief RegisterReceiver adds the receiver object at eAddress to the receiver list.
     * @param eAddress is the address of the receiver to be added.
     */
    void RegisterReceiver(const size_t &eAddress) {
        Receivers.insert(eAddress);
    }

    /**
     * @brief Signals is the signal list. It contains the addresses of signal objects that are registered.
     */
    std::set<size_t> Signals;
    /**
     * @brief Receivers is the receiver list. It contains the addresses of receiver objects that are registered.
     */
    std::set<size_t> Receivers;
    /**
     * @brief UncleanSignals is a list of all signals that is not yet cleaned after a receiver goes out of scope.
     */
    std::set<size_t> UncleanSignals;
};
/**
 * @brief StaticManager is the static ktnSignalSlotManager that manages all instances of ktnSignalSlot.
 */
static Manager StaticManager;
} // namespace ktn::SignalSlot
#endif // KTNENGINE_CORE_KTNSIGNALSLOTMANAGER_HPP
