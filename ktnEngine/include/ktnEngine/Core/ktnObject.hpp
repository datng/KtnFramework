#ifndef KTNENGINE_CORE_KTNOBJECT_HPP
#define KTNENGINE_CORE_KTNOBJECT_HPP
#include "ktnCoreCommon.hpp"
#include "ktnName.hpp"

namespace ktn::Core {
class ktnObject {
public:
    explicit ktnObject(ktnName eName = "ktnObject");
    virtual ~ktnObject() = default;

    bool operator==(const ktnObject &eObj) const;
    bool operator!=(const ktnObject &eObj) const;
    ktnObject &operator=(const ktnObject &eObj);

    ktnName Name;
};
} // namespace ktn::Core
#endif // KTNENGINE_CORE_KTNOBJECT_HPP
