#ifndef KTN3DELEMENT_HPP
#define KTN3DELEMENT_HPP
#include <ktnEngine/ktnCore>

#include "ktnGraphicsCommon.hpp"

#include <glm/glm.hpp>

namespace ktn {
namespace Graphics {
class ktn3DElement : public Core::ktnObject {
public:
    ktn3DElement(const Core::ktnName &eName = "ktn3DElement");
    virtual ~ktn3DElement();

    bool operator==(const ktn3DElement &element) const;
    bool operator!=(const ktn3DElement &element) const;
    ktn3DElement &operator=(const ktn3DElement &element);

    getter virtual glm::mat4 ModelMatrix() const;
    getter virtual glm::vec3 Position() const;
    getter virtual glm::vec3 Scale() const;
    getter virtual glm::vec4 Rotation() const;

    virtual setter void SetPosition(const glm::vec3 &pos, const bool &updatingModelMatrix = false);
    virtual setter void SetPosition(const float &x, const float &y, const float &z, const bool &updatingModelMatrix = false);
    virtual setter void SetRotation(const glm::vec4 &rotation, const bool &updatingModelMatrix = false);
    virtual setter void SetRotation(const glm::vec3 &axis, const float &angle, const bool &updatingModelMatrix = false);
    virtual setter void SetRotation(const float &x, const float &y, const float &z, const float &angle, const bool &updatingModelMatrix = false);
    virtual setter void SetScale(const glm::vec3 &scl, const bool &updatingModelMatrix = false);
    virtual setter void SetScale(const float &x, const float &y, const float &z, const bool &updatingModelMatrix = false);

protected:
    void UpdateModelMatrix();

    glm::mat4 m_ModelMatrix = IDENTITY_MATRIX_4;
    glm::vec3 m_Position = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec4 m_Rotation = glm::vec4(0.0f, 0.0f, 1.0f, 0.0f);
    glm::vec3 m_Scale = glm::vec3(1.0f, 1.0f, 1.0f);
};
} // namespace Graphics
} // namespace ktn
#endif // KTN3DELEMENT_HPP
