#ifndef KTNLIGHT_HPP
#define KTNLIGHT_HPP
#include "ktn3DElement.hpp"

#include <glm/glm.hpp>

#include <cmath>

namespace ktn {
namespace Graphics {
// class ktnLight
class ktnLight : public ktn3DElement {
public:
    /**
     * @brief The hCutoff class describes the light cutoff parameters.
     */
    helper class hCutoff {
    public:
        /**
         * @brief hCutoff is the default constructor.
         * @param eInnerAngle is the initial inner angle. Defaults to 12.5 degree, corresponding to a 25 degree opening.
         * @param eOuterAngle is the initial outer angle. Defaults to 17.5 degree, corresponding to a 35 degree opening.
         */
        explicit hCutoff(const float &eInnerAngle = 12.5f, const float &eOuterAngle = 17.5f);

        bool operator==(const hCutoff &eCutoff) const;
        bool operator!=(hCutoff eCutoff) const;

        /**
         * @brief InnerCos
         * @return the cosinus of the inner angle.
         */
        getter inline float InnerCos() const {
            return m_Inner;
        }

        /**
         * @brief OuterCos
         * @return the cosinus of the outer angle.
         */
        getter inline float OuterCos() const {
            return m_Outer;
        }

        /**
         * @brief SetInnerAngle sets the inner angle to eInnerAngle degree
         * @param eAngle is the angle to be set.
         */
        setter inline void SetInnerAngle(const float &eAngle) {
            m_Inner = glm::cos((glm::radians(eAngle)));
        }

        /**
         * @brief SetOuterAngle sets the inner angle to eAngle degree
         * @param eAngle is the angle to be set.
         */
        setter inline void SetOuterAngle(const float &eAngle) {
            m_Outer = glm::cos(glm::radians(eAngle));
        }

    private:
        float m_Inner;
        float m_Outer;
    }; // class hCutoff

    /**
     * @brief The hFalloff class describes the light falloff parameters.
     * The members of this class are based on this equation: i = I / (1 + (linearterm * d) + (quadraticterm * d * d))
     * in which:
     * i is the resulting intensity of the light at distance d
     * I is the intensity of the light at distance 0
     * linearterm and quadraticterm are adjustment parameters to control the falloff curve.
     * This equation is loosely based on the inverse square law: i = I / (d * d), which has 2 practical problems
     * Problem 1: There is a singularity at d = 0, when the light approaches an object, it gets infinitely bright.
     * Therefore the constant term 1 is added, so that i approaches I as d approaches 0;
     * Problem 2: The lighting of the scene is used to induce its mood regardless of physical accuracy.
     * The linear and quadratic terms are added to give the level designer more control over this.
     */
    helper class hFalloff {
    public:
        /**
         * @brief hFalloff is the default constructor.
         * @param eLinearTerm
         * @param eQuadraticTerm
         */
        explicit hFalloff(const float &eLinearTerm = 0.0f, const float &eQuadraticTerm = 1.0f);

        bool operator==(hFalloff eFalloff) const;
        bool operator!=(hFalloff eFalloff) const;

        float LinearTerm;
        float QuadraticTerm;
    }; // class hFalloff

    explicit ktnLight(const Core::ktnName &name = "ktnLight");
    virtual ~ktnLight();

    bool operator==(const ktnLight &eLight);
    bool operator!=(const ktnLight &eLight);
    ktnLight &operator=(const ktnLight &eLight);

    getter glm::vec3 Color() const;
    getter glm::vec3 Direction() const;

    setter void SetColor(const glm::vec3 &col);
    setter void SetColor(const float &r, const float &g, const float &b);
    setter void SetDirection(const glm::vec3 &dir);

    hCutoff Cutoff;
    hFalloff Falloff;
    float Intensity = 1.0f;
    bool IsCastingShadows = false;
    glm::mat4 ProjectionMatrix = IDENTITY_MATRIX_4;
    LightType Type = LightType::POINT;
    glm::mat4 ViewMatrix = IDENTITY_MATRIX_4;

private:
    glm::vec3 m_Color = glm::vec3(1.0, 1.0, 1.0);
    glm::vec3 m_Direction = glm::vec3(0.0f, 0.0f, -1.0f); // set the default direction to directly downwards
}; // class ktnLight
} // namespace Graphics
} // namespace ktn
#endif // KTNLIGHT_HPP
