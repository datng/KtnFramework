#ifndef KTNGRAPHICSCOMMON_H
#define KTNGRAPHICSCOMMON_H
#include <ktnEngine/ktnCore>

#include <glm/glm.hpp>
#include <vulkan/vulkan.hpp>

#include <map>
#include <string>

namespace ktn {
namespace Graphics {
enum class CameraMode { FREELOOK, TRACKING };

enum class CameraType { FIRSTPERSON, THIRDPERSONFREE, THIRDPERSONOVERSHOULDER, FLIGHT, SUPERVISION };

enum class FrameBufferType { DEPTH, COLOR_RGB };

enum class GraphicsLevels { NONE, LOW, MEDIUM, HIGH };

enum class LightType { POINT, DIRECTIONAL, SPOT };

enum class ShaderPass { SHADOW, COLOR, POSTPROCESS };

enum class ShaderType { VERTEX, FRAGMENT, GEOMETRY };

static std::map<ShaderType, std::string> ShaderTypeString = {
    {ShaderType::VERTEX, "vertex"}, //
    {ShaderType::FRAGMENT, "fragment"},
    {ShaderType::GEOMETRY, "geometry"} // clang-format
};

enum class TextureType { DIFFUSE, SPECULAR, HEIGHT, NORMAL, ROUGHNESS, BASECOLOR, METALLIC, EMISSIONCOLOR };

typedef struct {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TextureCoordinates;
} ktnVertex;

enum class Backend { OPENGL, VULKAN };
} // namespace Graphics
} // namespace ktn
#endif // KTNGRAPHICSCOMMON_H
