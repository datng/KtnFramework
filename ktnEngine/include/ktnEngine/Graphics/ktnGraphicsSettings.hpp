#ifndef KTNGRAPHICSSETTINGS_HPP
#define KTNGRAPHICSSETTINGS_HPP
#include "ktnGraphicsCommon.hpp"

namespace ktn {
namespace Graphics {
class GraphicsSettings {
public:
    GraphicsSettings();
    std::string AspectRatio;
    std::string Resolution;
    unsigned int BloomLevel;
    unsigned int FogLevel;
    unsigned int MotionBlurLevel;
    bool DepthOfField;
    bool HighDynamicRange;
    bool MultiSampleAntiAliasing;
    bool ScreenSpaceGlobalIllumination;
    bool ScreenSpaceReflection;
    bool ScreenSpaceAmbientOcclusion;
    bool Tesselation;
    bool VerticalSynchronization;
    GraphicsLevels TextureDetails;
    GraphicsLevels ShadowDetails;
};
} // namespace graphics
} // namespace ktn
#endif // KTNGRAPHICSSETTINGS_HPP
