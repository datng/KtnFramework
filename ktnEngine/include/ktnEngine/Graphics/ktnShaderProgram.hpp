﻿#ifndef KTNSHADERPROGRAM_HPP
#define KTNSHADERPROGRAM_HPP
#include "ktnLight.hpp"

#include <string>

namespace ktn {
namespace Graphics {
class ktnShaderProgram {
public:
    ktnShaderProgram();
    ktnShaderProgram(
        const std::string &VertexShaderPath, // TODO add description comments for the parameters
        const std::string &FragmentShaderPath,
        const std::string &GeometryShaderPath = std::string());

    ExecutionStatus GetShaderCode(const ShaderType &type, const std::string &Path);
    unsigned int CompileShader(const ShaderType &type);
    void SetShader(const std::string &Path, const ShaderType &type);
    void Update(); // not sure if possible to update shader on the fly
    void Use();

    void SetVertexShader(const std::string &VertexShaderPath);
    void SetFragmentShader(const std::string &FragmentShaderPath);
    void SetGeometryShader(const std::string &GeometryShaderPath);

    void SetUpLight(ktnLight &light);

    /**
     * Notice the references with 2 '&'s.
     * Without the extra '&', the compiler will give an error
     * when we pass a function or a class method to this parameter
     * For example: yourshader.SetUniformMat4("ViewMatrix", Camera::ViewMatrix())
     * wouldn't work without the &&
     * This problem could also be solved by using template, but templates are defined in the header,
     * and generally not smooth to use, so I resorted to this.
     */
    void SetUniformBool(const std::string &name, const bool &value);
    void SetUniformBool(const std::string &name, const bool &&value);

    void SetUniformFloat(const std::string &name, const float &value);
    void SetUniformFloat(const std::string &name, const float &&value);

    void SetUniformInt(const std::string &name, const int &value);
    void SetUniformInt(const std::string &name, const int &&value);

    void SetUniformMat2(const std::string &name, const glm::mat2 &mat);
    void SetUniformMat2(const std::string &name, const glm::mat2 &&mat);
    void SetUniformMat3(const std::string &name, const glm::mat3 &mat);
    void SetUniformMat3(const std::string &name, const glm::mat3 &&mat);
    void SetUniformMat4(const std::string &name, const glm::mat4 &mat);
    void SetUniformMat4(const std::string &name, const glm::mat4 &&mat);

    void SetUniformVec2(const std::string &name, const glm::vec2 &vec);
    void SetUniformVec2(const std::string &name, const glm::vec2 &&vec);
    void SetUniformVec2(const std::string &name, const float &x, const float &y);
    void SetUniformVec2(const std::string &name, const float &&x, const float &&y);

    void SetUniformVec3(const std::string &name, const glm::vec3 &vec);
    void SetUniformVec3(const std::string &name, const glm::vec3 &&vec);
    void SetUniformVec3(const std::string &name, const float &x, const float &y, const float &z);
    void SetUniformVec3(const std::string &name, const float &&x, const float &&y, const float &&z);

    void SetUniformVec4(const std::string &name, const glm::vec4 &vec);
    void SetUniformVec4(const std::string &name, const glm::vec4 &&vec);
    void SetUniformVec4(const std::string &name, const float &x, const float &y, const float &z, const float &w);
    void SetUniformVec4(const std::string &name, const float &&x, const float &&y, const float &&z, const float &&w);

    ShaderPass Pass;

private:
    void CheckCompileErrors(unsigned int ShaderID, ShaderType type);
    void CheckLinkErrors(unsigned int ProgramID);
    unsigned int ProgramID;
    unsigned int FragmentShaderID{};
    unsigned int GeometryShaderID{};
    unsigned int VertexShaderID{};
    std::string ShaderCode;
};
} // namespace Graphics
} // namespace ktn
#endif // KTNSHADERPROGRAM_HPP
