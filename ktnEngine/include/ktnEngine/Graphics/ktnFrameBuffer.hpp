#ifndef KTNFRAMEBUFFER_HPP
#define KTNFRAMEBUFFER_HPP
#include "ktnGraphicsCommon.hpp"
#include "ktnTexture.hpp"

#include <vector>

namespace ktn {
namespace Graphics {
class ktnFrameBuffer {
public:
    ktnFrameBuffer(const FrameBufferType &type, const vk::Extent2D &eExtend2D, const unsigned int &nTextures = 1);
    ~ktnFrameBuffer();
    void Bind();
    void Unbind();
    void SetResolution(const uint32_t &width, const uint32_t &height);

    unsigned int ID() const;
    vk::Extent2D Extend2D() const;
    unsigned int m_nTextures;
    std::vector<ktnTexture> Textures;

private:
    unsigned int m_ID;
    vk::Extent2D m_Extend2D;
};
} // namespace Graphics
} // namespace ktn
#endif // KTNFRAMEBUFFER_HPP
