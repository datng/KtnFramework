#ifndef KTNTEXTURE_HPP
#define KTNTEXTURE_HPP
#include "ktnGraphicsCommon.hpp"

#include <string>

namespace ktn {
namespace Graphics {
class ktnTexture {
public:
    /**
     * @brief ktnTexture generates a texture for a frame buffer.
     * @param type is the appropriate frame buffer type as defined in "ktnCommon.h".
     * @param eExtend2D is the 2D extent of the texture.
     */
    ktnTexture(const FrameBufferType &type, const vk::Extent2D &eExtend2D);
    ktnTexture(const std::string &path, const TextureType &t);
    ~ktnTexture();

    bool operator==(const ktnTexture &eTexture);
    bool operator!=(const ktnTexture &eTexture);

    getter std::string Path() const;
    getter TextureType Type() const;
    getter unsigned int ID() const;
    getter vk::Extent2D Extend2D() const;
    getter int nChannels() const;

    setter void SetTextureParameter(unsigned int target, unsigned int paramName, int param);

    void CreateOnGPU(const FrameBufferType &type);
    void LoadToGPU();
    void Unload();

protected:
    std::string m_Path;
    TextureType m_Type;
    unsigned int m_ID = 0;
    vk::Extent2D m_Extend2D;
    int m_nChannels = -1;
};
} // namespace Graphics
} // namespace ktn
#endif // KTNTEXTURE_HPP
