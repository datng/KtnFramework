#ifndef CAMERA_HPP
#define CAMERA_HPP
#include "ktn3DElement.hpp"

#include "ktnEngine/Math/ktnEulerAngles.hpp"
#include "ktnEngine/ktnCore"
#include "ktnGraphicsCommon.hpp"

#include <glm/glm.hpp>

namespace ktn {
namespace Graphics {
class ktnCamera : public ktn3DElement {
public:
    ktnCamera(const glm::vec3 &initialEulerAngles = glm::vec3(0.0f), const Math::ktnEulerAngles &eAngles = Math::ktnEulerAngles());

    virtual ~ktnCamera();

    bool operator==(const ktnCamera &eCamera) const;
    bool operator!=(const ktnCamera &eCamera) const;
    ktnCamera &operator=(const ktnCamera &eCamera);

    getter glm::vec3 Direction() const;
    getter glm::vec3 Front() const;
    getter CameraMode Mode() const;
    getter glm::vec3 Right() const;
    getter float Sensitivity() const;
    getter glm::vec3 Target() const;
    getter glm::vec3 Top() const;
    getter CameraType Type() const;
    getter glm::vec3 UniversalUp() const;
    getter glm::mat4 ViewMatrix() const;

    setter void SetDirection(const glm::vec3 &dir);
    setter void SetDirection(const float &x, const float &y, const float &z);
    setter void SetFront(const glm::vec3 &front);
    setter void SetFront(const float &x, const float &y, const float &z);
    setter void SetMode(const CameraMode &Mode);
    setter void SetTarget(const glm::vec3 &targetpos);
    setter void SetTarget(const float &x, const float &y, const float &z);
    setter void SetType(const CameraType &Type);
    setter void SetUniversalUp(const glm::vec3 &up);

    void ChangePosition(const glm::vec3 &offset);
    void MoveForward(const float &thismuch);
    void MoveBackward(const float &thismuch);
    void MoveLeft(const float &thismuch);
    void MoveRight(const float &thismuch);
    void RegulatePitch();
    void TurnDown(const float &thismuch);
    void TurnLeft(const float &thismuch);
    void TurnRight(const float &thismuch);
    void TurnUp(const float &thismuch);
    /**
     * @brief UpdateRightAndTop updates the camera's Right and Top vectors
     * depending on the current camera type and mode
     */
    void UpdateRightAndTop();
    void UpdateFront();
    void UpdateViewMatrix();

    Math::ktnEulerAngles EulerAngles;

private:
    CameraType m_Type;
    CameraMode m_Mode;
    glm::vec3 m_UniversalUp = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::vec3 m_PositionOffset;
    glm::vec3 m_Target; // not needed for fps but does need for other types
    glm::vec3 m_Direction;
    glm::vec3 m_Right;
    glm::vec3 m_Top = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::vec3 m_Front = glm::vec3(0.0f, -1.0f, 0.0f);
    glm::mat4 m_ViewMatrix = IDENTITY_MATRIX_4;

    float m_Sensitivity = 0.1f;
};
} // namespace Graphics
} // namespace ktn
#endif // CAMERA_HPP
