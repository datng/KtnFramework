#ifndef KTNMODEL_HPP
#define KTNMODEL_HPP
#include "ktn3DElement.hpp"
#include "ktnMesh.hpp"

#include <assimp/postprocess.h>
#include <assimp/scene.h>

namespace ktn {
namespace Graphics {
class ktnModel : public ktn3DElement {
public:
    ktnModel(const std::string &ePath, const Core::ktnName &eName = "ktnModel");
    ~ktnModel();

    bool operator==(const ktnModel eModel);
    bool operator!=(const ktnModel& eModel);
    ktnModel &operator=(const ktnModel &eModel);

    ExecutionStatus Clear();
    ExecutionStatus LoadFromDisk(const std::string& path);
    void LoadToGPU();
    void UnloadFromGPU();

    std::vector<ktnMesh> Meshes;

    bool m_IsLoadedToGPU = false;
    std::string m_FileLocation;
    std::string m_FileName;
    std::string m_FileExtension;

private:
    void ProcessNode(aiNode *n, const aiScene *s);
};
} // namespace Graphics
} // namespace ktn
#endif // KTNMODEL_HPP
