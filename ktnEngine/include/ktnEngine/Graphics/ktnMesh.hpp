#ifndef KTNMESH_HPP
#define KTNMESH_HPP
#include "ktn3DElement.hpp"
#include "ktnShaderProgram.hpp"
#include "ktnTexture.hpp"

#include <vector>

namespace ktn {
namespace Graphics {
class ktnMesh : public ktn3DElement {
public:
    ktnMesh(
        std::vector<ktnVertex> vertices, //
        std::vector<unsigned int> indices,
        std::vector<ktnTexture> textures);
    virtual ~ktnMesh();

    bool operator==(const ktnMesh &eMesh);
    bool operator!=(const ktnMesh &eMesh);
    ktnMesh &operator=(const ktnMesh &eMesh);

#ifdef KTN_USE_OPENGL
    getter unsigned int VertexArray() const;
    getter unsigned int VertexBuffer() const;
    getter unsigned int ElementBuffer() const;
#endif
    /**
     * @brief LoadToGPU loads the resources to the GPU
     */
    void LoadToGPU();
    /**
     * @brief UnloadFromGPU releases the resources from the GPU, deleting all the buffers
     */
    void UnloadFromGPU();
    /**
     * @brief Draw tells the GPU to execute the specified shader program using parameters from this mesh
     * @param shader is the GPU shader program to be used
     * @param textureIndex is te texture inside the graphics card where the output of the drawing operation goes to.
     * By default it is 0, but if we do advanced lighting techniques, for example shadow mapping,
     * then we need a different index than our final pass.
     * Also be careful not to set it to the same value as one of the maps we use, e.g. diffuse, specular, etc.
     */
    void Draw(ktnShaderProgram shader, unsigned int textureIndex = 0);

    std::vector<ktnVertex> Vertices;
    std::vector<unsigned int> Indices;
    std::vector<ktnTexture> Textures;

private:
#ifdef KTN_USE_OPENGL
    unsigned int m_VertexArrayObject; // vertex attribute object
    unsigned int m_VertexBufferObject; // vertex buffer
    unsigned int m_ElementBufferObject; //
#endif
}; // class ktnMesh
} // namespace graphics
} // namespace ktn
#endif // KTNMESH_HPP
