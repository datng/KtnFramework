#ifndef KTNMATERIAL_HPP
#define KTNMATERIAL_HPP
#include "ktnTexture.hpp"
#include <ktnEngine/ktnCore>
#include <ktnEngine/ktnMath>

#include <algorithm> // for std::clamp

namespace ktn {
namespace Graphics {
class ktnColor : public Math::ktnVector3D {
public:
    ktnColor(float x, float y, float z);
    ~ktnColor() override {}

    ktnColor &operator=(const ktnColor &eColor) {
        ktnVector3D::SetX(eColor.X());
        ktnVector3D::SetY(eColor.Y());
        ktnVector3D::SetZ(eColor.Z());
        return *this;
    }

    virtual void SetX(const float &x) override;
    virtual void SetY(const float &y) override;
    virtual void SetZ(const float &z) override;
};

class ktnMaterial {
public:
    ktnMaterial() {}

    ~ktnMaterial() {}

    getter inline ktnColor *BaseColor() {
        return &m_BaseColor;
    }

    getter inline float Metallic() const {
        return m_Metallic;
    }

    getter inline float Roughness() const {
        return m_Roughness;
    }

    getter inline float Specular() const {
        return m_Specular;
    }

    getter inline ktnColor *EmissionColor() {
        return &m_EmissionColor;
    }

    setter inline void SetBaseColor(const ktnColor eBaseColor) {
        m_BaseColor = eBaseColor;
    }

    setter inline void SetMetallic(const float &eMetallic) {
        m_Metallic = std::clamp(eMetallic, 0.0f, 1.0f);
    }

    setter inline void SetRoughness(const float &eRoughness) {
        m_Roughness = std::clamp(eRoughness, 0.0f, 1.0f);
    }

    setter inline void SetSpecular(const float &eSpecular) {
        m_Specular = std::clamp(eSpecular, 0.0f, 1.0f);
    }

private:
    ktnColor m_BaseColor{1, 1, 1};
    float m_Metallic = 0.0;
    float m_Roughness = 1.0;
    float m_Specular = 0.0;
    ktnColor m_EmissionColor{0, 0, 0};
};
} // namespace Graphics
} // namespace ktn

#endif // KTNMATERIAL_HPP
