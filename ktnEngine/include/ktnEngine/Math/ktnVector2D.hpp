#ifndef KTNENGINE_MATH_KTNVECTOR2D_HPP
#define KTNENGINE_MATH_KTNVECTOR2D_HPP
#include "ktnVector.hpp"
namespace ktn::Math {
class ktnVector2D : public ktnVector<2> {
public:
    explicit ktnVector2D(const float &x, const float &y) : ktnVector({x, y}) {}

    getter inline float X() const {
        return (*this)[0];
    }

    getter inline float Y() const {
        return (*this)[1];
    }

    setter virtual void SetX(const float &x);
    setter virtual void SetY(const float &y);
};

inline float Angle(const ktn::AngleUnit &eUnit, const ktnVector2D &vec1, const ktnVector2D &vec2) {
    return Angle<2>(eUnit, vec1, vec2);
}

inline float DotProduct(const ktnVector2D &vec1, const ktnVector2D &vec2) {
    return DotProduct<2>(vec1, vec2);
}
} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNVECTOR2D_HPP
