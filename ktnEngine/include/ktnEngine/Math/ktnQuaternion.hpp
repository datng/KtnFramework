#ifndef KTNENGINE_MATH_KTNQUATERNION_HPP
#define KTNENGINE_MATH_KTNQUATERNION_HPP
#include <ktnEngine/Core/ktnCoreCommon.hpp>

namespace ktn::Math {
class ktnQuaternion {
public:
    enum class ComponentBasis { Real, I, J, K };

    /**
     * @brief The Component class describes the individual components of a quaternion,
     * namely its basis (real, i, j, k) and scale.
     */
    struct Component {
    public:
        Component(const ComponentBasis &basis, const float &scale);

        const ComponentBasis Basis;
        float Scale = 0;
    };

    /**
     * @brief The ComponentReal class describes the real component of a quaternion.
     */
    class ComponentReal : public Component {
    public:
        explicit ComponentReal(const float &scale) noexcept;
        ComponentReal(const ComponentReal &c);
        virtual ~ComponentReal();

        ComponentReal &operator=(const ComponentReal &c);
        ComponentReal operator+(const ComponentReal &c) const;
        ComponentReal operator+(const float &addend) const;
        ComponentReal operator*(const float &scale) const;
    };

    /**
     * @brief The ComponentI class describes the component of a quaternion along the i basis.
     */
    class ComponentI : public Component {
    public:
        explicit ComponentI(const float &scale) noexcept;
        ComponentI(const ComponentI &c);
        virtual ~ComponentI();

        ComponentI &operator=(const ComponentI &c);
        ComponentI operator+(const ComponentI &c) const;
        ComponentI operator*(const float &scale) const;
    };

    /**
     * @brief The ComponentJ class describes the component of a quaternion along the j basis.
     */
    class ComponentJ : public Component {
    public:
        explicit ComponentJ(const float &scale) noexcept;
        ComponentJ(const ComponentJ &c);
        virtual ~ComponentJ();

        ComponentJ &operator=(const ComponentJ &c);
        ComponentJ operator+(const ComponentJ &c) const;
        ComponentJ operator*(const float &scale) const;
    };

    /**
     * @brief The ComponentK class describes  the component of a quaternion along the k basis.
     */
    class ComponentK : public Component {
    public:
        explicit ComponentK(const float &scale) noexcept;
        ComponentK(const ComponentK &c);
        virtual ~ComponentK();

        ComponentK &operator=(const ComponentK &c);
        ComponentK operator+(const ComponentK &c) const;
        ComponentK operator*(const float &scale) const;
    };

    ktnQuaternion();
    ktnQuaternion(const ktnQuaternion &q);
    ktnQuaternion(const ComponentReal &real, const ComponentI &i, const ComponentJ &j, const ComponentK &k);
    ktnQuaternion(float real, float i, float j, float k);
    ~ktnQuaternion();

    ktnQuaternion &operator=(const ktnQuaternion &eQuaternion);
    ktnQuaternion operator+(const ktnQuaternion &eQuaternion);

    // All of these multiplications are right multiplications,
    // except with the real component, where a left multiplication has the same result as a right multiplication.
    ktnQuaternion operator*(const ComponentI &c) const;
    ktnQuaternion operator*(const ComponentJ &c) const;
    ktnQuaternion operator*(const ComponentK &c) const;
    ktnQuaternion operator*(const ComponentReal &c) const;
    ktnQuaternion operator*(const float &scale) const;

    ComponentReal W = ComponentReal(0);
    ComponentI X = ComponentI(0);
    ComponentJ Y = ComponentJ(0);
    ComponentK Z = ComponentK(0);

    setter void Set(const ComponentReal &real, const ComponentI &i, const ComponentJ &j, const ComponentK &k);
    setter void Set(const float &real, const float &i, const float &j, const float &k);

    bool Equals(const ktnQuaternion &q, const float &tolerance = ComparisonThreshold_Float);
    [[nodiscard]] ktnQuaternion Conjugated() const;
    [[nodiscard]] float Norm() const;

    inline static const ComponentReal BasisReal = ComponentReal(1.0F);
    inline static const ComponentI BasisI = ComponentI(1.0F);
    inline static const ComponentJ BasisJ = ComponentJ(1.0F);
    inline static const ComponentK BasisK = ComponentK(1.0F);
};

////////////
// utilities
// left multiplications for quaternions
ktnQuaternion operator*(float scale, ktnQuaternion q);
ktnQuaternion operator*(const ktnQuaternion::ComponentReal &basis, const ktnQuaternion &q);
ktnQuaternion operator*(const ktnQuaternion::ComponentI &basis, const ktnQuaternion &q);
ktnQuaternion operator*(const ktnQuaternion::ComponentJ &basis, const ktnQuaternion &q);
ktnQuaternion operator*(const ktnQuaternion::ComponentK &basis, const ktnQuaternion &q);

struct QuaternionNormalizationResult {
    ktn::ExecutionStatus Status;
    ktnQuaternion Quaternion;
};

QuaternionNormalizationResult Normalize(ktnQuaternion q);
} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNQUATERNION_HPP
