#ifndef KTNENGINE_MATH_KTNVECTOR3D_HPP
#define KTNENGINE_MATH_KTNVECTOR3D_HPP
#include "ktnVector.hpp"

namespace ktn::Math {
class ktnVector3D : public ktnVector<3> {
public:
    ktnVector3D() = default;
    ktnVector3D(const float &x, const float &y, const float &z) : ktnVector({x, y, z}) {}

    getter inline float X() const {
        return (*this)[0];
    }

    getter inline float Y() const {
        return (*this)[1];
    }

    getter inline float Z() const {
        return (*this)[2];
    }

    setter virtual void SetX(const float &x);
    setter virtual void SetY(const float &y);
    setter virtual void SetZ(const float &z);

    [[nodiscard]] ktnVector3D CrossProduct(const ktnVector3D &eVector) const;
};

inline float Angle(const ktn::AngleUnit &eUnit, const ktnVector3D &vec1, const ktnVector3D &vec2) {
    return Angle<3>(eUnit, vec1, vec2);
}

[[nodiscard]] inline ktnVector3D CrossProduct(const ktnVector3D &vec1, const ktnVector3D &vec2) {
    return vec1.CrossProduct(vec2);
}

[[nodiscard]] inline float DotProduct(const ktnVector3D &vec1, const ktnVector3D &vec2) {
    return DotProduct<3>(vec1, vec2);
}
} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNVECTOR3D_HPP
