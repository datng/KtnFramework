#ifndef KTNENGINE_MATH_KTNVECTOR_HPP
#define KTNENGINE_MATH_KTNVECTOR_HPP
#include <ktnEngine/ktnCore>

#include "ktnMathCommon.hpp"

#include <array>
#include <iostream>
#include <vector>

namespace ktn::Math {
template <const size_t size> class ktnVector {
public:
    ktnVector() {
        static_assert(size > 0, "A ktnVector must have at least one component.");
        for (size_t i = 0; i < size; ++i) {
            m_Data[i] = 0.0F;
        }
    }

    ktnVector(const ktnVector &eOther) {
        for (size_t i = 0; i < size; ++i) {
            m_Data[i] = eOther[i];
        }
    }

    explicit ktnVector(const std::array<float, size> &eInitList) {
        for (size_t i = 0; i < size; ++i) {
            m_Data[i] = eInitList[i];
        }
    }

    virtual ~ktnVector() = default;

    virtual float operator[](const size_t &index) const {
        assert(index < size);
        return m_Data[index];
    }

    virtual ktnVector &operator+(const ktnVector &eOther) {
        for (size_t i = 0; i < size; ++i) {
            m_Data[i] += eOther[i];
        }
        return *this;
    }

    virtual ktnVector &operator*(const float &eScalar) {
        for (auto &component : m_Data) {
            component *= eScalar;
        }
        return *this;
    }

    [[nodiscard]] inline float Angle(const ktn::AngleUnit &eUnit, const ktnVector &eOther) const {
        const float mag1 = Magnitude();
        const float mag2 = eOther.Magnitude();

        // The angle between a vector and a null vector is defined as 0.
        // To reduce numerical instability, very short vectors are also treated as null vectors.
        if (mag1 < ktn::ComparisonThreshold_Float || mag2 < ktn::ComparisonThreshold_Float) {
            return 0.0F;
        }

        switch (eUnit) {
        case ktn::AngleUnit::degree:
            return RadianToDegree(acosf(DotProduct(eOther) / mag1 / mag2));
        case ktn::AngleUnit::radian:
            return acosf(DotProduct(eOther) / mag1 / mag2);
        }
    }

    [[nodiscard]] inline float DotProduct(const ktnVector &eOther) const {
        float sum = 0;
        for (size_t i = 0; i < size; ++i) {
            sum += m_Data[i] * eOther[i];
        }
        return sum;
    }

    [[nodiscard]] inline float Length() const {
        return Magnitude();
    }

    [[nodiscard]] inline float Magnitude() const {
        float sum = 0;
        for (auto &component : m_Data) {
            sum += powf(component, 2.0F);
        }
        return sqrtf(sum);
    }

    // TODO find better name for this
    ktn::ExecutionStatus Set(const size_t &index, const float &value) {
        if (index < size) {
            m_Data[index] = value;
            return ktn::ExecutionStatus::OK;
        } else {
            std::cerr //
                << "The given index (" << index //
                << ") does not belong in a ktnVector of size " << size //
                << "." << std::endl;
            return ktn::ExecutionStatus::FAILURE;
        }
    }

    /**
     * @brief Normalize
     * @return the unit vector of the direction to which the vector is pointing.
     * Note that a magnitude of 0 will result in undefined behavior, as the null vector cannot be normalized.
     * If such is the case, throw an exception to crash the program as early as possible, while printing an error message.
     */
    [[nodiscard]] inline ktnVector &Normalize() {
        auto currentMagnitude = Magnitude();
        if (currentMagnitude < ktn::ComparisonThreshold_Float) {
            std::cerr << "ktnVector::Normalize: the vector has a magnitude of " //
                      << currentMagnitude //
                      << ", which is smaller than the currently set comparison threshold for floating point numbers." //
                      << std::endl;
            throw std::domain_error("ktnVector::Normalize: degenerative case");
        }
        for (auto &component : m_Data) {
            component /= currentMagnitude;
        }
        return *this;
    }

    [[nodiscard]] inline size_t Size() const {
        return size;
    }

private:
    std::vector<float> m_Data = std::vector<float>(size, 0.0);
}; // class ktnVector

/**
 * @brief Angle
 * @param unit
 * @param vec1
 * @param vec2
 * @return the angle between vec1 and vec2, in the unit
 */
template <size_t size> float Angle(const ktn::AngleUnit &unit, const ktnVector<size> &vec1, const ktnVector<size> &vec2) {
    return vec1.Angle(unit, vec2);
}

template <size_t size> inline float DotProduct(const ktnVector<size> &vec1, const ktnVector<size> &vec2) {
    return vec1.DotProduct(vec2);
}

} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNVECTOR_HPP
