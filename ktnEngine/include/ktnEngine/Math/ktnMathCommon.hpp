#ifndef KTNENGINE_MATH_KTNMATHCOMMON_HPP
#define KTNENGINE_MATH_KTNMATHCOMMON_HPP
#include <ktnEngine/Core/ktnCoreCommon.hpp>
#ifdef _WIN32
#include <corecrt_math_defines.h>
#else
#include <cmath>
#endif

namespace ktn::Math {
/**
 * @brief RadToDeg is the constant ratio between
 * the value of any angle measured in degrees and the same thing in radians
 * RadToDeg = y/x where x [rad] = (x / pi * 180) [deg] = y [deg]
 */
static constexpr float RadToDeg = static_cast<float>(180.0 / M_PI);

/**
 * @brief DegToRad is the constant ratio between
 * the value of any angle measured in radians and the same thing in degrees
 * DegToRad = x/y where x [rad] = (x / pi * 180) [deg] = y [deg]
 */
static constexpr float DegToRad = static_cast<float>(M_PI / 180.0);

/**
 * @brief RadianToDegree converts the size of an angle from radian to degree.
 * @param eSize is the angle size in radian.
 * @return the angle size in degree.
 */
inline float RadianToDegree(const float &eSize) {
    return eSize * RadToDeg;
}

/**
 * @brief DegreeToRadian converts the size of an angle from degree to radian.
 * @param eSize is the angle size in degree.
 * @return the angle size in radian.
 */
inline float DegreeToRadian(const float &degree) {
    return degree * DegToRad;
}
} // namespace ktn::Math
#endif // KTNENGINE_MATH_KTNMATHCOMMON_HPP
