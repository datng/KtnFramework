#ifndef KTNAUDIO_HPP
#define KTNAUDIO_HPP
#include "ktnEngine/ktnCore"

#include <portaudio.h>
#include <vorbis/vorbisfile.h>

#include <atomic>
#include <string>

namespace ktn {
namespace audio {
class ktnAudio : public Core::ktnObject {
public:
    ktnAudio();
    ktnAudio(std::string path);
    ~ktnAudio();
    void Play();
    void Play(std::string path);
    void Loop();

private:
    void Setup();
    std::string m_Path;
    std::atomic<float> m_CurrentVolume;
    float m_TargetVolume;

    // for loading the file into memory
    OggVorbis_File m_VorbisFile;

    // for streaming the audio
    PaStreamParameters m_OutputStreamParameters;
    PaStream *m_Stream = nullptr;

    // is a static constant due to the output array initialization
    static const int m_FramesPerBuffer = 256;
}; // class ktnAudio
} // namespace audio
} // namespace ktn
#endif // KTNAUDIO_HPP
